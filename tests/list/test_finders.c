#include <criterion/criterion.h>

#include "list.h"
#include "test_int_functions.h"

Test(list, get)
{
    int a = 42;
    struct list *list = list_init_int();
    list_push_front(list, &a);

    int *ret_a = list_get(list, 0);

    cr_assert_eq(a, *ret_a);

    list_clear(list);
    list_destroy(list);
}

Test(list, find)
{
    int a = 42;
    struct list *list = list_init_int();
    list_push_front(list, &a);

    size_t a_i = list_find(list, &a);

    cr_assert_eq(0, a_i);

    list_clear(list);
    list_destroy(list);
}
