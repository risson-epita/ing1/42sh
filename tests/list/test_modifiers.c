#include <criterion/criterion.h>

#include "list.h"
#include "test_int_functions.h"

Test(list, push_front_one_element)
{
    int a = 1;
    struct list *list = list_init_int();

    int rc = list_push_front(list, &a);

    cr_assert_eq(1, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rc);

    list_clear(list);
    list_destroy(list);
}

Test(list, push_front_several_elements)
{
    int a = -1;
    int b = 0;
    int c = 2;
    struct list *list = list_init_int();

    int rca = list_push_front(list, &a);
    int rcb = list_push_front(list, &b);
    int rcc = list_push_front(list, &c);

    cr_assert_eq(3, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rca);
    cr_assert_eq(LIST_SUCCESS, rcb);
    cr_assert_eq(LIST_SUCCESS, rcc);

    list_clear(list);
    list_destroy(list);
}

Test(list, push_back_one_element)
{
    int a = 1;
    struct list *list = list_init_int();

    int rc = list_push_back(list, &a);

    cr_assert_eq(1, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rc);

    list_clear(list);
    list_destroy(list);
}

Test(list, push_back_several_elements)
{
    int a = -1;
    int b = 0;
    int c = 2;
    struct list *list = list_init_int();

    int rca = list_push_back(list, &a);
    int rcb = list_push_back(list, &b);
    int rcc = list_push_back(list, &c);

    cr_assert_eq(3, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rca);
    cr_assert_eq(LIST_SUCCESS, rcb);
    cr_assert_eq(LIST_SUCCESS, rcc);

    list_clear(list);
    list_destroy(list);
}

Test(list, push_front_back_front)
{
    int a = -1;
    int b = 0;
    int c = 2;
    struct list *list = list_init_int();

    int rca = list_push_front(list, &a);
    int rcb = list_push_back(list, &b);
    int rcc = list_push_front(list, &c);

    cr_assert_eq(3, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rca);
    cr_assert_eq(LIST_SUCCESS, rcb);
    cr_assert_eq(LIST_SUCCESS, rcc);

    list_clear(list);
    list_destroy(list);
}

Test(list, push_back_front_back)
{
    int a = -1;
    int b = 0;
    int c = 2;
    struct list *list = list_init_int();

    int rca = list_push_back(list, &a);
    int rcb = list_push_front(list, &b);
    int rcc = list_push_back(list, &c);

    cr_assert_eq(3, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rca);
    cr_assert_eq(LIST_SUCCESS, rcb);
    cr_assert_eq(LIST_SUCCESS, rcc);

    list_clear(list);
    list_destroy(list);
}

Test(list, insert_at_zero)
{
    int a = -1;
    int b = 0;
    struct list *list = list_init_int();
    list_push_front(list, &a);

    int rcb = list_insert_at(list, &b, 0);

    cr_assert_eq(2, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rcb);

    list_clear(list);
    list_destroy(list);
}

Test(list, insert_at_size)
{
    int a = -1;
    int b = 0;
    struct list *list = list_init_int();
    list_push_front(list, &a);

    int rcb = list_insert_at(list, &b, list_size(list));

    cr_assert_eq(2, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rcb);

    list_clear(list);
    list_destroy(list);
}

Test(list, insert_at_middle_one_element)
{
    int a = -1;
    int b = 0;
    int c = 2;
    struct list *list = list_init_int();
    list_push_front(list, &a);
    list_push_front(list, &b);

    int rcc = list_insert_at(list, &c, 1);

    cr_assert_eq(3, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rcc);

    list_clear(list);
    list_destroy(list);
}

Test(list, insert_at_middle_several_elements)
{
    int a = -1;
    int b = 0;
    int c = 2;
    int d = 21;
    int e = 42;
    int f = -512;
    struct list *list = list_init_int();
    list_push_front(list, &a);
    list_push_front(list, &b);

    int rcc = list_insert_at(list, &c, 1);
    int rcd = list_insert_at(list, &d, 1);
    int rce = list_insert_at(list, &e, 1);
    int rcf = list_insert_at(list, &f, 1);

    cr_assert_eq(6, list_size(list));
    cr_assert_eq(LIST_SUCCESS, rcc);
    cr_assert_eq(LIST_SUCCESS, rcd);
    cr_assert_eq(LIST_SUCCESS, rce);
    cr_assert_eq(LIST_SUCCESS, rcf);

    list_clear(list);
    list_destroy(list);
}

Test(list, remove_at_one_element)
{
    int a = -1;
    struct list *list = list_init_int();
    list_push_front(list, &a);

    int *ret_a = list_remove_at(list, 0);

    cr_assert_eq(0, list_size(list));
    cr_assert_eq(a, *ret_a);

    list_clear(list);
    list_destroy(list);
}

Test(list, remove_at_several_elements)
{
    int a = -1;
    int b = 0;
    struct list *list = list_init_int();
    list_push_front(list, &a);
    list_push_front(list, &b);

    int *ret_a = list_remove_at(list, 1);
    int *ret_b = list_remove_at(list, 0);

    cr_assert_eq(0, list_size(list));
    cr_assert_eq(a, *ret_a);
    cr_assert_eq(b, *ret_b);

    list_clear(list);
    list_destroy(list);
}

Test(list, remove_eq)
{
    int a = 12;
    struct list *list = list_init_int();
    list_push_front(list, &a);

    int *ret_a = list_remove_eq(list, &a);

    cr_assert_eq(0, list_size(list));
    cr_assert_eq(a, *ret_a);

    list_clear(list);
    list_destroy(list);
}
