#include <criterion/criterion.h>

#include "list.h"
#include "test_int_functions.h"

Test(list, init)
{
    struct list *list = list_init_int();
    cr_assert_eq(0, list_size(list));
    list_destroy(list);
}

Test(list, clear)
{
    int a = 1;
    int b = 2;
    int c = 3;
    struct list *list = list_init_int();
    list_push_front(list, &a);
    list_push_front(list, &b);
    list_push_front(list, &c);

    list_clear(list);
    cr_assert_eq(0, list_size(list));
    list_destroy(list);
}
