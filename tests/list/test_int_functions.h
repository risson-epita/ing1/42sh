#ifndef TEST_LIST_INT_FUNCTIONS_H
#define TEST_LIST_INT_FUNCTIONS_H

#include <stdio.h>

#include "list.h"

static inline int cmp_int(void *lhs, void *rhs)
{
    int *left = lhs;
    int *right = rhs;
    if (*left < *right)
        return LIST_LOWER;
    if (*left > *right)
        return LIST_BIGGER;
    return LIST_EQUAL;
}

static inline void clear_int(void *element)
{
    (void) element;
    return;
}

static inline struct list *list_init_int(void)
{
    return list_init(cmp_int, clear_int);
}

#endif /* ! TEST_LIST_INT_FUNCTIONS_H */
