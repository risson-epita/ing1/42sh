add_executable(test_list EXCLUDE_FROM_ALL
        test_finders.c
        test_helpers.c
        test_modifiers.c
        test_ops.c
)

target_include_directories(test_list PUBLIC
        .
)

target_link_libraries(test_list PRIVATE
        ${CRITERION_LIB}
        common_options
        list
        smalloc
)

add_test(NAME test_list
         COMMAND test_list --tap
)
add_dependencies(check test_list)
