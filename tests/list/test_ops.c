#include <criterion/criterion.h>

#include "list.h"
#include "test_int_functions.h"

Test(list, sort)
{
    int a = 21;
    int b = 0;
    int c = 2;
    int d = -1;
    int e = 42;
    int f = -512;
    struct list *list = list_init_int();
    list_push_back(list, &a);
    list_push_back(list, &b);
    list_push_back(list, &c);
    list_push_back(list, &d);
    list_push_back(list, &e);
    list_push_back(list, &f);

    list_sort(list);

    int *i0 = list_get(list, 0);
    int *i1 = list_get(list, 1);
    int *i2 = list_get(list, 2);
    int *i3 = list_get(list, 3);
    int *i4 = list_get(list, 4);
    int *i5 = list_get(list, 5);

    cr_assert_eq(-512, *i0, "expected: %d, got: %d", -512, *i0);
    cr_assert_eq(-1, *i1, "expected: %d, got: %d", -1, *i1);
    cr_assert_eq(0, *i2, "expected: %d, got: %d", 0, *i2);
    cr_assert_eq(2, *i3, "expected: %d, got: %d", 2, *i3);
    cr_assert_eq(21, *i4, "expected: %d, got: %d", 21, *i4);
    cr_assert_eq(42, *i5, "expected: %d, got: %d", 42, *i5);

    cr_assert_eq(6, list_size(list));

    list_clear(list);
    list_destroy(list);
}
