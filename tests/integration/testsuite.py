#! /usr/bin/env python3
from argparse import ArgumentParser
from pathlib import Path
from difflib import unified_diff
import subprocess as sp
from termcolor import colored
import yaml
import os
import signal
from contextlib import contextmanager
import tempfile

CHECKS = ["stdout", "stderr", "returncode", "has_stderr"]
DEFAULT_CHECKS = CHECKS[:3]
INPUT_TYPES = ["stdin", "file", "string_argument"]
DEFAULT_INPUT_TYPES = INPUT_TYPES[:1]
CURRENT_INPUT_TYPE = DEFAULT_INPUT_TYPES[0]
SANITY = False

def raise_timeout(signum, frame):
    raise TimeoutError

@contextmanager
def timeout(time):
    # Register a function to raise a TimeoutError on the signal
    signal.signal(signal.SIGALRM, raise_timeout)
    # Schedule the signal to be sent after `time`
    signal.alarm(time)

    try:
        yield
    except TimeoutError:
        raise TimeoutError
    finally:
        # Unregister the signal so it won't be triggered
        # if the timeout is not reached
        signal.signal(signal.SIGALRM, signal.SIG_IGN)

class TestCase:
    def __init__(self, content, timeout):
        self.name = content["name"]
        self.stdin = content["stdin"]
        self.timeout = timeout if "timeout" not in content else content["timeout"]
        self.checks = content.get("checks", DEFAULT_CHECKS)
        self.output_files = content.get("output_files", [])
        self.input_files = content.get("input_files", [])
        self.allow_failure = content.get("allow_failure", False)

    def diff(self, ref, student):
        """
            Function to format the diff output in case of a failed test.
        """
        ref = ref.splitlines(keepends=True)
        student = student.splitlines(keepends=True)
        return ''.join(unified_diff(ref, student, fromfile="ref", tofile="student"))

    def setup(self):
        for output_file in self.output_files:
            if os.path.exists(output_file):
                os.remove(output_file)
        for input_file in self.input_files:
            input_file_name = input_file["name"]
            input_file_content = input_file.get("content", "")
            with open(input_file_name, 'w') as f:
                f.write(input_file_content)

    def cleanup(self):
        for output_file in self.output_files:
            if os.path.exists(output_file):
                os.remove(output_file)
        for input_file in self.input_files:
            input_file_name = input_file["name"]
            if os.path.exists(input_file_name):
                os.remove(input_file_name)

    def run_cmd(self, args, stdin):
        global CURRENT_INPUT_TYPE

        if CURRENT_INPUT_TYPE == "stdin":
            return sp.run(args, capture_output=True, text=True, input=stdin)
        if CURRENT_INPUT_TYPE == "file":
            tmp_file = tempfile.NamedTemporaryFile(dir="/tmp", prefix="42sh-testsuite-integration-", mode='w', delete=False)
            tmp_file.write(stdin)
            tmp_file.close()
            args = args + [tmp_file.name]
            result = sp.run(args, capture_output=True, text=True)
            os.remove(tmp_file.name)
            return result
        if CURRENT_INPUT_TYPE == "string_argument":
            args = args + ["-c", stdin]
            return sp.run(args, capture_output=True, text=True)


    def _run(self, bash, executable, executable_valgrind):
        global SANITY

        ref = None
        student = None
        student_valgrind = None

        ref_output_files = [""] * len(self.output_files)
        student_output_files = [""] * len(self.output_files)

        if SANITY:
            self.setup()
            student_valgrind = self.run_cmd(executable_valgrind, self.stdin)
            self.cleanup()

        self.setup()
        ref = self.run_cmd(bash, self.stdin)
        if len(self.output_files) != 0:
            for i in range(len(self.output_files)):
                try:
                    with open(self.output_files[i], 'r') as file:
                        ref_output_files[i] = file.read()
                except FileNotFoundError:
                    raise AssertionError(f"File <{self.output_files[i]}> not found in ref output.")
        self.cleanup()

        self.setup()
        student = self.run_cmd(executable, self.stdin)
        if len(self.output_files) != 0:
            for i in range(len(self.output_files)):
                try:
                    with open(self.output_files[i], 'r') as file:
                        student_output_files[i] = file.read()
                except FileNotFoundError:
                    raise AssertionError(f"File <{self.output_files[i]}> not found in student output.")
        self.cleanup()

        return ref, student, student_valgrind, ref_output_files, student_output_files

    def run(self, executable):
        global SANITY

        valgrind_exit_code = 42
        valgrind = ["valgrind", "--leak-check=full", f"--error-exitcode={valgrind_exit_code}"]
        bash = ["bash", "--posix"]
        executable = [executable]
        executable_valgrind = valgrind
        executable_valgrind.append(executable[0])

        if self.timeout is not None:
            with timeout(self.timeout):
                ref, student, student_valgrind, ref_output_files, student_output_files = self._run(bash, executable, executable_valgrind)
        else:
            ref, student, student_valgrind, ref_output_files, student_output_files = self._run(bash, executable, executable_valgrind)

        if SANITY and student_valgrind.returncode == valgrind_exit_code:
            raise MemoryError(student_valgrind.stderr)

        for check in self.checks:
            if check == "stdout":
                assert ref.stdout == student.stdout, \
                f"difference in stdout:\n{self.diff(ref.stdout, student.stdout)}"

            elif check == "stderr":
                assert ref.stderr == student.stderr, \
                f"difference in stderr:\n{self.diff(ref.stderr, student.stderr)}"

            elif check == "returncode":
                assert ref.returncode == student.returncode, \
                    f"return code: expected: {ref.returncode}, got: {student.returncode}"

            elif check == "has_stderr":
                assert student.stderr != "", "something was expected on stderr"

            elif check == "output_files":
                for i in range(len(self.output_files)):
                    assert ref_output_files[i] == student_output_files[i], \
                        f"difference in output file <{self.output_files[i]}>\n{self.diff(ref_output_files[i], ref_output_files[i])}"

class TestSuite(list):
    def __init__(self, filename, timeout):
        super().__init__()
        self.name = Path(filename).stem
        self.passed_tests = 0
        self.failed_tests = 0
        self.failed_allowed_tests = 0
        with open(filename) as test_file:
            content = yaml.safe_load(test_file)
        if not content:
            return
        for testcase in content:
            self.append(TestCase(testcase, timeout))

    def run(self, executable):
        print(colored(f"[=====] Running TestSuite <{self.name}>", 'magenta'))
        for testcase in self:
            try:
                testcase.run(executable)
            except AssertionError as err:
                if testcase.allow_failure:
                    self.failed_allowed_tests += 1
                    print(f"{colored('[=====][IGNR]', 'yellow')} {testcase.name}")
                else:
                    self.failed_tests += 1
                    print(f"{colored('[=====][FAIL]', 'red')} {testcase.name}")
                print(err)
            except TimeoutError:
                if testcase.allow_failure:
                    self.failed_allowed_tests += 1
                    print(f"{colored('[=====][IGNR]', 'yellow')} {testcase.name}")
                else:
                    self.failed_tests += 1
                    print(f"{colored('[=====][TIME]', 'red')} {testcase.name}")
            except MemoryError as err:
                if testcase.allow_failure:
                    self.failed_allowed_tests += 1
                    print(f"{colored('[=====][IGNR]', 'yellow')} {testcase.name}")
                else:
                    self.failed_tests += 1
                    print(f"{colored('[=====][LEAK]', 'red')} {testcase.name}")
                print(err)
            except KeyboardInterrupt:
                print()
                exit(130)
            except Exception as err:
                if testcase.allow_failure:
                    self.failed_allowed_tests += 1
                    print(f"{colored('[=====][IGNR]', 'yellow')} {testcase.name}")
                else:
                    self.failed_tests += 1
                    print(colored("[=====][FAIL] An error occured. Here's the backtrace:", 'red'))
                print(err)
            else:
                print(f"{colored('[=====][PASS]', 'green')} {testcase.name}")
                self.passed_tests += 1
        print(colored(f"[=====] Results for TestSuite <{self.name}>: Tested: {len(self)} ", 'magenta') +
              f"| Passed: {colored(str(self.passed_tests), 'green')} "
              f"| Failed: {colored(str(self.failed_tests), 'red')}")

class Category(list):
    def __init__(self, name, path, timeout):
        super().__init__()
        self.name = name
        for root, dirs, files in os.walk(path):
            for file in files:
                if file.endswith(".yml"):
                    self.append(TestSuite(os.path.join(root, file), timeout))
        self.sort(key=lambda x: x.name)
        self.passed_testsuites = 0
        self.failed_testsuites = 0
        self.sort(key=lambda x: x.name)

    def run(self, executable):
        print(colored(f"[===] Running category <{self.name}>\n", 'cyan', attrs=['bold']))
        for testsuite in self:
            testsuite.run(executable)
            if testsuite.failed_tests == 0:
                self.passed_testsuites += 1
            else:
                self.failed_testsuites += 1
        print("\n" + colored(f"[===] Results for category <{self.name}>: Tested: {len(self)} ", 'cyan', attrs=['bold']) +
              f"| Passed: {colored(str(self.passed_testsuites), 'green')} "
              f"| Failed: {colored(str(self.failed_testsuites), 'red')}")

class Categories(list):
    def __init__(self, path, timeout):
        super().__init__()
        for category in os.listdir(path):
            self.append(Category(category, os.path.join(path, category), timeout))
        self.sort(key=lambda x: x.name)
        self.passed_categories = 0
        self.failed_categories = 0
        self.sort(key=lambda x: x.name)

    def list_available(self):
        print("Available categories:")
        for category in self:
            print(category.name)

    def run(self, executable, name):
        for category in self:
            if category.name == name:
                category.run(executable)
                return
        print(colored(f"Category {name} not found.", 'red'))

    def run_all(self, executable):
        print(colored(f"Running all categories with executable {executable}\n", 'blue'))
        for category in self:
            category.run(executable)
            if category.failed_testsuites == 0:
                self.passed_categories += 1
            else:
                self.failed_categories += 1
            print("\n")
        print(colored(f"[===] Global result: Categories tested: {len(self)} ", 'magenta') +
              f"| Categories passed: {colored(str(self.passed_categories), 'green')} "
              f"| Categories failed: {colored(str(self.failed_categories), 'red')}")

def start(categories, executable, args):
    global CURRENT_INPUT_TYPE
    if args.category is None:
        categories.run_all(executable)
    else:
        for category in args.category:
            categories.run(executable, category)

def everything(categories, executable):
    global SANITY
    global INPUT_TYPES
    global CURRENT_INPUT_TYPE
    SANITY = True
    for input_type in INPUT_TYPES:
        CURRENT_INPUT_TYPE = input_type
        print(colored("Running with input type " + CURRENT_INPUT_TYPE, 'blue'))
        categories.run_all(executable)
    return categories.failed_categories

def main():
    global SANITY
    global INPUT_TYPES
    global CURRENT_INPUT_TYPE
    parser = ArgumentParser(description="42sh test suite")

    parser.add_argument("executable", help="the 42sh executable to run the test suite against")
    parser.add_argument("-a", "--all", action="store_true", help="run all tests in every mode available, overrides all other options, except timeout")
    parser.add_argument("-c", "--category", action="append", help="execute the test suite on the specified category, can be specified multiple times, runs all categories if not specified")
    parser.add_argument("-i", "--input", action="append", help="execute the test suite with specified input type, can be specified multiple times, runs with `stdin` if not specified")
    parser.add_argument("-l", "--list", action="store_true", help="display list of test categories and input types")
    parser.add_argument("-s", "--sanity", action="store_true", help="execute the test suite with sanity checks enabled, using valgrind")
    parser.add_argument("-t", "--timeout", action="store", type=int, help="timeout for a single test, in seconds")

    args = parser.parse_args()

    executable = Path(args.executable).absolute()
    categories = Categories(os.path.join(os.getcwd(), "categories/"), args.timeout)

    if args.all:
        return everything(categories, executable)

    if args.sanity:
        SANITY = True

    if args.list:
        categories.list_available()
        print("Available input types:")
        for input_type in INPUT_TYPES:
            print(input_type)
        return 0

    if not args.input:
        start(categories, executable, args)
    else:
        for input_type in args.input:
            if input_type not in INPUT_TYPES:
                raise ValueError(f"Input type {input_type} does not exists.")
            CURRENT_INPUT_TYPE = input_type
            print(colored("Running with input type " + CURRENT_INPUT_TYPE, 'blue'))
            start(categories, executable, args)

    return categories.failed_categories

if __name__ == "__main__":
    rc = main()
    exit(rc)
