# Integration test suite

## Requirements

To be able to run the integration test suite located in `tests/integration`,
you need to install the required modulea, listed in `requirements.txt`.

### Via virtual environment

Create the virtual environment and install the requirements:

```bash
python -m venv env/
source env/bin/activate
pip install -r requirements.txt
deactivate
```

To use the environment:

```bash
source env/bin/activate
# launch the test suite, ...
deactivate
```

## Architecture

The tests are located in multiple categories in `tests/integration/categories`,
e.g., `simple_commands`, and then in files, called test suites, e.g.,
`basic.yml` to test basic commands. Then each file contains a list of test
cases.


Example of architecture:

```
categories
├── a.simple_commands
│   ├── 1.basic.yml
│   ├── 2.with_arguments.yml
│   ├── 3.semicolon.yml
│   ├── 4.syntax_error.yml
│   └── 5.command_fail.yml
├── b.if
│   ├── 1.simple.yml
│   ├── 2.with_semicolons.yml
│   ├── 3.with_test_cmd.yml
│   ├── 4.else.yml
│   ├── 5.elif.yml
│   ├── 6.nested.yml
│   ├── 7.syntax_error.yml
│   └── 8.command_fail.yml
├── c.while
│   ├── 1.simple.yml
│   ├── 2.nested
│   ├── 3.syntax_error.yml
│   └── 4.command_fail.yml
├── d.until
│   ├── 1.simple.yml
│   ├── 2.nested.yml
│   ├── 3.syntax_error.yml
│   └── 4.command_fail.yml
├── e.for
│   ├── 1.simple.yml
│   ├── 2.nested.yml
│   ├── 3.syntax_error.yml
│   └── 4.command_fail.yml
├── f.and_or
│   ├── 1.basic.yml
│   ├── 2.syntax_error.yml
│   └── 3.command_fail.yml
├── g.case
│   ├── 1.basic.yml
│   ├── 2.syntax_error.yml
│   └── 3.command_fail.yml
├── g.pipe
│   ├── 1.basic.yml
│   ├── 2.multiple.yml
│   └── 3.multiline.yml
└── h.history
    └── 1.command.yml
```


## Add tests

Example of a `tests.yml` file:

```yaml
- name: ls_simple
  stdin: ls

- name: ls_with_args
  stdin: ls -la
```

### Checks

**To specify what to check, use the `checks` field**.

It can take the following arguments: `stdout`, `stderr`, `has_stderr`, and
`returncode`.

Leave it blank to use the default values: `stdout`, `stderr`, `returncode`.

```yaml
- name: ls_simple
  stdin: ls
  checks:
	- stdout
	- returncode
```

### Output files

You can specify output files that will be created by `42sh` when the test is
run. It will be checked if they are the same as the ones `bash --posix`
produces.

```yaml
- name: ls_redir
  stdin: ls > output.test
  output_files:
    - output.test
```

### Input files

You can specify input files that will be created before running the tests.

```yaml
- name: cat_redir_input
  stdin: cat < input.test
  input_files:
    - name: input.test
      content: "Best test suite ever!"
```

## The test suite

The test suite will run the 42sh binary with the given stdin and will compare
stdout, stderr and returncode (depends on the field checks) with `bash
--posix`.


To run the test suite:

```bash
# In your virtual env
tests/integration/testsuite.py 42sh_binary
```

For a list of the supported options: `tests/integration/testsuite.py --help`.
