#include <criterion/criterion.h>

#include "parser.h"
#include "token.h"
#include "lexer.h"
#include "utils.h"

// Test(parser, ast_simple_command_two)
// {
//     char str[] = "momo alexia";
//     struct input *input = calloc(1, sizeof(struct input));
//     input->command_string = 1;
//     input->command = strdup(str);
//
//     struct lexer *lexer = lexer_create(input, get_input_command_string);
//
//     struct ast_node *ast = parse(lexer);
//
//     struct node_simple_command *simple_command = cast_void(ast);
//
//     cr_assert_eq(2, simple_command->nb_words);
//     cr_assert_eq(0, strcmp(simple_command->words[0], "momo"));
//     cr_assert_eq(0, strcmp(simple_command->words[1], "alexia"));
//
//     ast->free(ast);
//     free(lexer);
// }
//
// Test(parser, ast_simple_command_five)
// {
//     char str[] = "momo alexia schmittos robot sm14";
//     struct input *input = calloc(1, sizeof(struct input));
//     input->command_string = 1;
//     input->command = strdup(str);
//
//     struct lexer *lexer = lexer_create(input, get_input_command_string);
//
//     struct ast_node *ast = parse(lexer);
//
//     struct node_simple_command *simple_command = cast_void(ast);
//
//     cr_assert_eq(5, simple_command->nb_words);
//     cr_assert_eq(0, strcmp(simple_command->words[0], "momo"));
//     cr_assert_eq(0, strcmp(simple_command->words[1], "alexia"));
//     cr_assert_eq(0, strcmp(simple_command->words[2], "schmittos"));
//     cr_assert_eq(0, strcmp(simple_command->words[3], "robot"));
//     cr_assert_eq(0, strcmp(simple_command->words[4], "sm14"));
//
//     ast->free(ast);
//     free(lexer);
// }
//
// Test(parser, ast_simple_command_assignment_word)
// {
//     char str[] = "na_me2=test";
//     struct input *input = calloc(1, sizeof(struct input));
//     input->command_string = 1;
//     input->command = strdup(str);
//
//     struct lexer *lexer = lexer_create(input, get_input_command_string);
//
//     struct ast_node *ast = parse(lexer);
//
//     struct node_simple_command *simple_command = cast_void(ast);
//
//     cr_assert_eq(1, simple_command->nb_assignments);
//     // struct node_word left = simple_command->assignments[0]->left;
//     // struct node_word right = simple_command->assignments[0]->right;
//     // cr_assert_eq(0, strcmp(left.value, "na_me2"));
//     // cr_assert_eq(0, strcmp(right.value, "test"));
//
//     ast->free(ast);
//     free(lexer);
// }
