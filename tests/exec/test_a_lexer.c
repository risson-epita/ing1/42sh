#include <criterion/criterion.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <errno.h>

#include "list.h"
#include "arithmetic_expansion.h"

Test(a_lexer, a_token_set_type)
{
    struct a_token *t = calloc(1, sizeof(struct a_token));
    t->value = strdup("**");

    a_token_set_type(t);

    cr_assert_eq(A_POW, t->type);

    a_token_free(t);
}

Test(a_lexer, a_token_set_type_number)
{
    struct a_token *t = calloc(1, sizeof(struct a_token));
    t->value = strdup("118218");

    a_token_set_type(t);

    cr_assert_eq(A_NUMBER, t->type);

    a_token_free(t);
}

Test(a_lexer, a_lexer_start)
{
    char value[] = "1+2";
    struct a_lexer *lexer = a_lexer_start(value);

    struct a_token *t = list_get(lexer->token_list, 0);
    cr_assert_eq(t->type, A_NUMBER);
    cr_assert_str_eq(t->value, "1");

    t = list_get(lexer->token_list, 1);
    cr_assert_eq(t->type, A_PLUS);
    cr_assert_str_eq(t->value, "+");

    t = list_get(lexer->token_list, 2);
    cr_assert_eq(t->type, A_NUMBER);
    cr_assert_str_eq(t->value, "2");

    //a_lexer_free(lexer);
}

Test(a_lexer, a_lexer_start_spaces)
{
    char value[] = "    1 + 2  ";
    struct a_lexer *lexer = a_lexer_start(value);

    struct a_token *t = list_get(lexer->token_list, 0);
    cr_assert_eq(t->type, A_NUMBER);
    cr_assert_str_eq(t->value, "1");

    t = list_get(lexer->token_list, 1);
    cr_assert_eq(t->type, A_PLUS);
    cr_assert_str_eq(t->value, "+");

    t = list_get(lexer->token_list, 2);
    cr_assert_eq(t->type, A_NUMBER);
    cr_assert_str_eq(t->value, "2");

    //a_lexer_free(lexer);
}

Test(a_lexer, a_lexer_start_tilde)
{
    char value[] = "    1 ~ 2  ";
    struct a_lexer *lexer = a_lexer_start(value);

    struct a_token *t = list_get(lexer->token_list, 0);
    cr_assert_eq(t->type, A_NUMBER);
    cr_assert_str_eq(t->value, "1");

    t = list_get(lexer->token_list, 1);
    cr_assert_eq(t->type, A_TILDE);
    cr_assert_str_eq(t->value, "~");

    t = list_get(lexer->token_list, 2);
    cr_assert_eq(t->type, A_NUMBER);
    cr_assert_str_eq(t->value, "2");

    //a_lexer_free(lexer);
}

Test(a_lexer, a_lexer_start_double_stars)
{
    char value[] = "    1 ** 2  ";
    struct a_lexer *lexer = a_lexer_start(value);

    struct a_token *t = list_get(lexer->token_list, 0);
    cr_assert_eq(t->type, A_NUMBER);
    cr_assert_str_eq(t->value, "1");

    t = list_get(lexer->token_list, 1);
    cr_assert_eq(t->type, A_POW);
    cr_assert_str_eq(t->value, "**");

    t = list_get(lexer->token_list, 2);
    cr_assert_eq(t->type, A_NUMBER);
    cr_assert_str_eq(t->value, "2");

    //a_lexer_free(lexer);
}
