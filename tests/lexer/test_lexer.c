#include <criterion/criterion.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <errno.h>

#include "lexer.h"
#include "list.h"
#include "token.h"


struct lexer *prepare_lexer(char *cmd)
{
    struct input *input = calloc(1, sizeof(struct input));
    input->mode = M_CMDSTR;
    input->fin = fmemopen(cmd, strlen(cmd), "r");
    if (!input->fin)
        err(1, NULL);
    return lexer_create(input, get_input_from_stream);
}

Test(lexer, basic_word_1)
{
    char cmd[] = "ls";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t->type);
    cr_assert_str_eq(t->value, "ls");

    token_free(t);
    lexer_free(lexer);
}

Test(lexer, basic_word_2)
{
    char cmd[] = "pwd";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t->type);
    cr_assert_str_eq(t->value, "pwd");

    token_free(t);
    lexer_free(lexer);
}

Test(lexer, basic_word_3)
{
    char cmd[] = "find";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t->type);
    cr_assert_str_eq(t->value, "find");

    token_free(t);
    lexer_free(lexer);
}

Test(lexer, basic_eol)
{
    char cmd[] = "pwd";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t = lexer_pop(lexer);
    token_free(t);
    t = lexer_pop(lexer);

    cr_assert_eq(TOK_EOL, t->type);

    token_free(t);
    lexer_free(lexer);
}

Test(lexer, basic_eof)
{
    char cmd[] = "pwd";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t = lexer_pop(lexer);
    token_free(t);
    token_free(lexer_pop(lexer));
    t = lexer_pop(lexer);

    cr_assert_eq(TOK_EOF, t->type);

    token_free(t);
    lexer_free(lexer);
}

Test(lexer, several_words_1)
{
    char cmd[] = "ls -la";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t_ls = lexer_pop(lexer);
    struct token *t_la = lexer_pop(lexer);
    struct token *t_eol = lexer_pop(lexer);
    struct token *t_eof = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t_ls->type);
    cr_assert_str_eq(t_ls->value, "ls");

    cr_assert_eq(TOK_WORD, t_la->type);
    cr_assert_str_eq(t_la->value, "-la");

    cr_assert_eq(TOK_EOL, t_eol->type);

    cr_assert_eq(TOK_EOF, t_eof->type);

    token_free(t_ls);
    token_free(t_la);
    token_free(t_eol);
    token_free(t_eof);
    lexer_free(lexer);
}

Test(lexer, several_words_2)
{
    char cmd[] = "ls -l -a -z pouet";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t_ls = lexer_pop(lexer);
    struct token *t_l = lexer_pop(lexer);
    struct token *t_a = lexer_pop(lexer);
    struct token *t_z = lexer_pop(lexer);
    struct token *t_pouet = lexer_pop(lexer);
    struct token *t_eol = lexer_pop(lexer);
    struct token *t_eof = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t_ls->type);
    cr_assert_str_eq(t_ls->value, "ls");

    cr_assert_eq(TOK_WORD, t_l->type);
    cr_assert_str_eq(t_l->value, "-l");

    cr_assert_eq(TOK_WORD, t_a->type);
    cr_assert_str_eq(t_a->value, "-a");

    cr_assert_eq(TOK_WORD, t_z->type);
    cr_assert_str_eq(t_z->value, "-z");

    cr_assert_eq(TOK_WORD, t_pouet->type);
    cr_assert_str_eq(t_pouet->value, "pouet");

    cr_assert_eq(TOK_EOL, t_eol->type);

    cr_assert_eq(TOK_EOF, t_eof->type);

    token_free(t_ls);
    token_free(t_l);
    token_free(t_a);
    token_free(t_z);
    token_free(t_pouet);
    token_free(t_eol);
    token_free(t_eof);
    lexer_free(lexer);
}

Test(lexer, with_newline_1)
{
    char cmd[] = "ls\n-la";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t_ls = lexer_pop(lexer);
    struct token *t_eol1 = lexer_pop(lexer);
    struct token *t_la = lexer_pop(lexer);
    struct token *t_eol2 = lexer_pop(lexer);
    struct token *t_eof = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t_ls->type);
    cr_assert_str_eq(t_ls->value, "ls");

    cr_assert_eq(TOK_EOL, t_eol1->type);

    cr_assert_eq(TOK_WORD, t_la->type);
    cr_assert_str_eq(t_la->value, "-la");

    cr_assert_eq(TOK_EOL, t_eol2->type);

    cr_assert_eq(TOK_EOF, t_eof->type);

    token_free(t_ls);
    token_free(t_la);
    token_free(t_eol1);
    token_free(t_eol2);
    token_free(t_eof);
    lexer_free(lexer);
}

Test(lexer, basic_comment_1)
{
    char cmd[] = "ls #lolmdr xd ptdr ta mère # lol jean ; ''";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t_ls = lexer_pop(lexer);
    struct token *t_eol = lexer_pop(lexer);
    struct token *t_eof = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t_ls->type);
    cr_assert_str_eq(t_ls->value, "ls");

    cr_assert_eq(TOK_EOL, t_eol->type, "expected: %d, got: %d",
                                                        TOK_EOL, t_eol->type);
    cr_assert_eq(TOK_EOF, t_eof->type, "expected: %d, got: %d",
                                                        TOK_EOF, t_eof->type);

    token_free(t_ls);
    token_free(t_eol);
    token_free(t_eof);
    lexer_free(lexer);
}

Test(lexer, basic_operator_1)
{
    char cmd[] = "ls &";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t_ls = lexer_pop(lexer);
    struct token *t_and = lexer_pop(lexer);
    struct token *t_eol = lexer_pop(lexer);
    struct token *t_eof = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t_ls->type);
    cr_assert_str_eq(t_ls->value, "ls");

    cr_assert_eq(TOK_AMPERSAND, t_and->type, "expected: %d, got: %d",
                                                    TOK_AMPERSAND, t_and->type);
    cr_assert_str_eq(t_and->value, "&");

    cr_assert_eq(TOK_EOL, t_eol->type, "expected: %d, got: %d",
                                                        TOK_EOL, t_eol->type);
    cr_assert_eq(TOK_EOF, t_eof->type, "expected: %d, got: %d",
                                                        TOK_EOF, t_eof->type);

    token_free(t_ls);
    token_free(t_and);
    token_free(t_eol);
    token_free(t_eof);
    lexer_free(lexer);
}

Test(lexer, basic_operator_2)
{
    char cmd[] = "ls >>";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t_ls = lexer_pop(lexer);
    struct token *t_redir = lexer_pop(lexer);
    struct token *t_eol = lexer_pop(lexer);
    struct token *t_eof = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t_ls->type);
    cr_assert_str_eq(t_ls->value, "ls");

    cr_assert_eq(TOK_DMORE, t_redir->type, "expected: %d, got: %d",
                                                    TOK_DMORE, t_redir->type);
    cr_assert_str_eq(t_redir->value, ">>");

    cr_assert_eq(TOK_EOL, t_eol->type, "expected: %d, got: %d",
                                                        TOK_EOL, t_eol->type);
    cr_assert_eq(TOK_EOF, t_eof->type, "expected: %d, got: %d",
                                                        TOK_EOF, t_eof->type);

    token_free(t_ls);
    token_free(t_redir);
    token_free(t_eol);
    token_free(t_eof);
    lexer_free(lexer);
}

Test(lexer, basic_operator_3)
{
    char cmd[] = "ls >>m";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t_ls = lexer_pop(lexer);
    struct token *t_redir = lexer_pop(lexer);
    struct token *t_m = lexer_pop(lexer);
    struct token *t_eol = lexer_pop(lexer);
    struct token *t_eof = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t_ls->type);
    cr_assert_str_eq(t_ls->value, "ls");

    cr_assert_eq(TOK_DMORE, t_redir->type, "expected: %d, got: %d",
                                                    TOK_DMORE, t_redir->type);
    cr_assert_str_eq(t_redir->value, ">>");

    cr_assert_eq(TOK_WORD, t_m->type);
    cr_assert_str_eq(t_m->value, "m");

    cr_assert_eq(TOK_EOL, t_eol->type, "expected: %d, got: %d",
                                                        TOK_EOL, t_eol->type);
    cr_assert_eq(TOK_EOF, t_eof->type, "expected: %d, got: %d",
                                                        TOK_EOF, t_eof->type);

    token_free(t_ls);
    token_free(t_m);
    token_free(t_redir);
    token_free(t_eol);
    token_free(t_eof);
    lexer_free(lexer);
}

Test(lexer, double_quotes_same_line)
{
    char cmd[] = "abc\"lol\"def";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t_word = lexer_pop(lexer);
    struct token *t_eol = lexer_pop(lexer);
    struct token *t_eof = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t_word->type);
    cr_assert_str_eq(t_word->value, "abc\"lol\"def");

    cr_assert_eq(TOK_EOL, t_eol->type, "expected: %d, got: %d",
                                                        TOK_EOL, t_eol->type);
    cr_assert_eq(TOK_EOF, t_eof->type, "expected: %d, got: %d",
                                                        TOK_EOF, t_eof->type);

    token_free(t_word);
    token_free(t_eol);
    token_free(t_eof);
    lexer_free(lexer);
}

Test(lexer, double_quotes_multiline)
{
    char cmd[] = "abc\"lol\ndef\"";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t_word = lexer_pop(lexer);
    struct token *t_eol = lexer_pop(lexer);
    struct token *t_eof = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t_word->type);
    cr_assert_str_eq(t_word->value, "abc\"lol\ndef\"");

    cr_assert_eq(TOK_EOL, t_eol->type, "expected: %d, got: %d",
                                                        TOK_EOL, t_eol->type);
    cr_assert_eq(TOK_EOF, t_eof->type, "expected: %d, got: %d",
                                                        TOK_EOF, t_eof->type);

    token_free(t_word);
    token_free(t_eol);
    token_free(t_eof);
    lexer_free(lexer);
}


Test(lexer, variable_multiline)
{
    char cmd[] = "echo ${\npamplemousse}";
    struct lexer *lexer = prepare_lexer(cmd);
    struct token *t_word = lexer_pop(lexer);
    struct token *t_var = lexer_pop(lexer);
    struct token *t_eol = lexer_pop(lexer);
    struct token *t_eof = lexer_pop(lexer);

    cr_assert_eq(TOK_WORD, t_word->type);
    cr_assert_str_eq(t_word->value, "echo");

    cr_assert_eq(TOK_WORD, t_var->type);
    cr_assert_str_eq(t_var->value, "${\npamplemousse}");

    cr_assert_eq(TOK_EOL, t_eol->type, "expected: %d, got: %d",
                                                        TOK_EOL, t_eol->type);
    cr_assert_eq(TOK_EOF, t_eof->type, "expected: %d, got: %d",
                                                        TOK_EOF, t_eof->type);

    token_free(t_word);
    token_free(t_eol);
    token_free(t_eof);
    lexer_free(lexer);
}
