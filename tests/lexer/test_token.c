#include <criterion/criterion.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lexer.h"
#include "list.h"
#include "token.h"
//
// Test(token, get_token_eol)
// {
    // char str[] = "";
    // struct token *t = get_token(str);
//
    // cr_assert_eq(t->type, TOK_EOL, "expected %d, got %d", TOK_EOL, t->type);
//
    // free(t);
// }
//
// Test(token, get_token_word)
// {
    // char str[] = "pwd";
    // struct token *t = get_token(strdup(str));
//
    // cr_assert_eq(t->type, TOK_WORD, "expected %d, got %d", TOK_WORD, t->type);
    // cr_assert_eq(strcmp(str, t->value), 0, "expected %s, got %s", str, t->value);
//
    // token_free(t);
// }
//
// Test(token, get_token_assignment_word)
// {
    // char str[] = "na_m8e=test";
    // struct token *t = get_token(strdup(str));
//
    // cr_assert_eq(t->type, TOK_ASSIGNMENT, "expected %d, got %d", TOK_ASSIGNMENT, t->type);
    // cr_assert_eq(strcmp(str, t->value), 0, "expected %s, got %s", str, t->value);
//
    // token_free(t);
// }
//
// Test(token, assignment_word_bad_syntax)
// {
    // char str[] = "1na_me=test";
    // struct token *t = get_token(strdup(str));
//
    // cr_assert_eq(t->type, TOK_WORD, "expected %d, got %d", TOK_WORD, t->type);
    // cr_assert_eq(strcmp(str, t->value), 0, "expected %s, got %s", str, t->value);
//
    // token_free(t);
// }
//
// Test(token, assignment_word_bad_syntax_2)
// {
    // char str[] = "=test";
    // struct token *t = get_token(strdup(str));
//
    // cr_assert_eq(t->type, TOK_WORD, "expected %d, got %d", TOK_WORD, t->type);
    // cr_assert_eq(strcmp(str, t->value), 0, "expected %s, got %s", str, t->value);
//
    // token_free(t);
// }
