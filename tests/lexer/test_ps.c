#include <criterion/criterion.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>

#include "lexer.h"
#include "list.h"
#include "token.h"

char *format_ps(char *ps);

Test(lexer, ps_basic)
{
    char *ps = format_ps("test");
    cr_assert_str_eq(ps, "test");
}

Test(lexer, ps_a)
{
    char *ps = format_ps("momo\\a");
    cr_assert_str_eq(ps, "momo\a");
}

/*Test(lexer, ps_username)
{
    char *ps = format_ps("\\u");
    char *login = getlogin();
    cr_assert_str_eq(ps, login);
}*/

Test(lexer, ps_hostnamefull)
{
    char buff[128] = { 0 };
    char *ps = format_ps("\\H");
    gethostname(buff, 128);
    cr_assert_str_eq(ps, buff);
}

Test(lexer, ps_n)
{
    char *ps = format_ps("momo\\n");
    cr_assert_str_eq(ps, "momo\n");
}

Test(lexer, ps_r)
{
    char *ps = format_ps("momo\\r");
    cr_assert_str_eq(ps, "momo\r");
}
