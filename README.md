# 42sh

### Compilation

In order to build and run the project, do the following:

```sh
mkdir build
cd build
cmake .. # By default the build type is Release
make
./42sh
# Enjoy!
```

To build the documentation:

```sh
mkdir build
cd build
cmake ..
make doc
browser ./doc/html/index.html
```

In order to compiles with `-g` and `-D_DEBUG` and with ASan, which will print a
lot of debugging informations, do:

```sh
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug
make
```

To run the tests:

```sh
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug
make check
```
