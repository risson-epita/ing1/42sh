**IMPORTANT: Please do not create a Merge Request without creating an issue
first.** Create your issue, and the use the GitLab Web Interface to create an MR
from this issue.

> Any changes needs to be discussed before proceeding.

## Checklist before submitting MR

- [ ] Your MR is not a duplicate
- [ ] Your changes have been made in a separate branch
- [ ] Your commits have a descriptive commit title AND a descriptive commit
      description.
- [ ] If not, make sure it's not a problem if we squash all the commits in the
      MR.

## Description

> A few sentences describing the overall goals of this MR.

## List of components affected

* First
* Second

## Status

- [ ] To be squashed
- [ ] CI passed
- [ ] Ready for review

## Types of changes

- [ ] Bug fix (non-breaking change which fixes an issue)
- [ ] New feature (non-breaking change which adds functionality)
- [ ] Breaking change (fix or feature that would cause existing functionality to change)

## Non Functional Requirements

- [ ] Follows the coding-style (either checked by you or by the CI)
- [ ] Tests Coverage for the new code
- [ ] All new and existing tests passed
- [ ] Documentation

## Extended explanation (if needed)

> Explain in detail what changed, add informations useful for the team if needed
> Also, explain, if appropriate, the reasons that made you effect this change

