/**
** \file src/smalloc/sstrdup.c
**
** \brief Memory management to avoid having to check for failed allocations.
**
** \author 42Schmittos
*/

#include <stdlib.h>
#include <string.h>

/**
** Wrapper around `strdup(3)` that uses our own malloc.
**
** \param s the string tu duplicate.
**
** \return a copy of the string.
*/
char *__wrap_strdup(const char *s)
{
    size_t len = strlen(s) + 1;
    char *new = malloc(len);
    new = memcpy(new, s, len);
    return new;
}

/**
** Wrapper around `strndup(3)` that uses our own malloc.
**
** \param s the string to duplicate.
** \param n the number of characters to duplicate.
**
** \return a copy of the string.
*/
char *__wrap_strndup(const char *s, size_t n)
{
    size_t len = strnlen(s, n);
    char *new = malloc(len + 1);
    new[len] = '\0';
    new = memcpy(new, s, len);
    return new;
}
