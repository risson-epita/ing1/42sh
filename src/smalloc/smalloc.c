/**
** \file src/smalloc/smalloc.c
**
** \brief Memory management to avoid having to check for failed allocations.
**
** \author 42Schmittos
*/

#include <err.h>
#include <stdio.h>

#include "utils.h"

/** Store an allocated pointer. */
struct allocation
{
    /** The allocated pointer. */
    void *data;
    /** The next allocation. */
    struct allocation *next;
    /** The previous allocation. */
    struct allocation *prev;
};

/** The head of the list of allocations. */
struct allocation *allocations_head;

void *__real_malloc(size_t size);
void __real_free(void *ptr);
void *__real_calloc(size_t nmemb, size_t size);
void *__real_realloc(void *ptr, size_t size);

/**
** Add a pointer to the global list of allocations `allocations_head`.
**
** \param ptr the pointer to add to said list.
*/
void __add_alloc(void *ptr)
{
    struct allocation *new = __real_malloc(sizeof(struct allocation));
    if (!new)
        err(ERROR_CODE, "Out of memory, malloc failed.");

    new->data = ptr;
    if (!allocations_head)
    {
        allocations_head = new;
        new->next = NULL;
    }
    else
    {
        new->next = allocations_head;
        allocations_head->prev = new;
        allocations_head = new;
    }
    new->prev = NULL;
}

/**
** Wrapper around `malloc(3)` that exits with 127 if case `malloc(3)` fails.
**
** \param size the `size` parameter for `malloc(3)`.
**
** \return the return value of `malloc(3)` if it succeeds.
*/
void *__wrap_malloc(size_t size)
{
    void *ptr = __real_malloc(size);
    if (!ptr && size != 0)
        err(ERROR_CODE, "Out of memory, malloc failed.");

    __add_alloc(ptr);

    return ptr;
}

/**
** Wrapper around `free(3)`.
**
** \param ptr the `ptr` parameter for `free(3)`.
*/
void __wrap_free(void *ptr)
{
    __real_free(ptr);

    if (!allocations_head)
        return;

    struct allocation *current = allocations_head;
    while (current)
    {
        if (current->data == ptr)
        {
            if (!current->prev && !current->next) // Only element left
            {
                allocations_head = NULL;
            }
            else if (!current->prev && current->next) // First element
            {
                allocations_head = current->next;
                allocations_head->prev = NULL;
            }
            else if (current->prev && !current->next) // Last element
            {
                current->prev->next = NULL;
            }
            else
            {
                current->prev->next = current->next;
                current->next->prev = current->prev;
            }

            __real_free(current);
            return;
        }
        current = current->next;
    }
}

/**
** Wrapper around `calloc(3)`.
**
** \param nmemb the `nmemb` parameter for `calloc(3)`.
** \param size the `size` param for `calloc(3)`.
**
** \return the return value of `calloc(3)` if it succeeds.
*/
void *__wrap_calloc(size_t nmemb, size_t size)
{
    void *ptr = __real_calloc(nmemb, size);
    if (!ptr && nmemb != 0 && size != 0)
        err(ERROR_CODE, "Out of memory, calloc failed");

    __add_alloc(ptr);

    return ptr;
}

/**
** Wrapper around `realloc(3)`.
**
** \param ptr the `ptr` parameter for `realloc(3)`.
** \param size the `size` parameter for `realloc(3)`.
**
** \return the return value of `realloc(3)` if it succeeds.
*/
void *__wrap_realloc(void *ptr, size_t size)
{
    void *new_ptr = __real_realloc(ptr, size);
    if (!new_ptr && size != 0)
        err(ERROR_CODE, "Out of memory, realloc failed");

    if (!allocations_head)
    {
        __add_alloc(new_ptr);
        return new_ptr;
    }

    struct allocation *current = allocations_head;
    while (current)
    {
        if (current->data == ptr)
        {
            current->data = new_ptr;
            return new_ptr;
        }
        current = current->next;
    }
    __add_alloc(new_ptr);
    return new_ptr;
}

/**
** Free all allocations that haven't been freed.
**
** \return the number of freed pointers.
*/
size_t __free_all(void)
{
    size_t count = 0;
    if (!allocations_head)
        return count;

    struct allocation *current = allocations_head;
    while (current)
    {
        struct allocation *tmp = current;
        current = current->next;
        __real_free(tmp->data);
        __real_free(tmp);
        ++count;
    }
    allocations_head = NULL;
    return count;
}
