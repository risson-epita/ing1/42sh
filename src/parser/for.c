/**
** \file src/parser/for.c
**
** \brief Basic grammar parser for a for, can be used on basic for commands.
**
** \author 42Schmittos.
*/
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "token.h"
#include "utils.h"

/**
** Free a node of type for.
**
** \param node the `ast_node` structure we want to free.
*/
void for_free(struct ast_node *node)
{
    struct node_for *node_for = cast_void(node);

    free(node_for->word);
    node_for->do_group->base.free(&node_for->do_group->base);
    list_destroy(node_for->word_list);

    free(node_for);
}

/**
** Printing function for node of type for.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *for_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_for *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    char *word = n->word;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", cast_void(word), word);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(word));

    for (size_t i = 0; i < list_size(n->word_list); i++)
    {
        char *word = list_get(n->word_list, i);
        fprintf(file, "    \"%p\" [label=\"%s\"];\n", cast_void(word), word);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(word));
    }

    struct node_compound_list *do_group = n->do_group;
    do_group->base.print(&do_group->base, file);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(do_group));

    return ptr;
}

/**
** Initialize a node of type for.
**
** \param word the 'char *' after the for.
** \param word_list the list in which we do the `do`.
** \param do_group the 'node_compound_list' structure after the do.
**
** \return a node_for structure.
*/
struct node_for *for_init(char *word, struct list *word_list,
                                            struct node_compound_list *do_group)
{
    struct node_for *node_for = calloc(1, sizeof(struct node_for));

    node_for->base.free = for_free;
    node_for->base.exec = exec_for;
    node_for->base.print = for_print;

    node_for->base.name = "for";

    node_for->word = word;
    node_for->word_list = word_list;
    node_for->do_group = do_group;

    return node_for;
}

/**
** Parsing sub function for the semicolon case.
**
** \param lexer the `lexer` we use.
** \param word the word we found before on the rule.
** \param token the token we need to continue the parsing.
** \param error the error tracker we want to change.
** \return a node for;
*/
struct node_for *for_create_semicolon(struct lexer *lexer, char *word,
                                    struct token *token, int *error)
{
    consume(lexer, error);
    token = lexer_peek(lexer, error);
    if (token->type != TOK_DO)
    {
        *error = ERROR_CODE;
        return NULL;
    }
    lexer_pop(lexer);

    struct node_compound_list *do_group = compound_list_create(lexer, error);
    token = lexer_peek(lexer, error);
    if (token->type != TOK_DONE)
    {
        *error = ERROR_CODE;
        return NULL;
    }
    return for_init(word, NULL, do_group);
}

/**
** Create the `in` part in a for.
**
** \param lexer the `lexer` we use.
** \param word the word we found before on the rule.
** \param token the token we need to continue the parsing.
** \param error the error tracker we want to change.
** \return a node for;
*/
struct node_for *for_create_in(struct lexer *lexer, char *word,
                                                struct token *token, int *error)
{
    token = lexer_peek(lexer, error);
    struct list *word_list = list_init(NULL, free);
    while (token->type == TOK_WORD)
    {
        token = lexer_pop(lexer);
        list_push_back(word_list, strdup(token->value));
        token_free(token);
        token = lexer_peek(lexer, error);
    }

    consume(lexer, error);
    token = lexer_peek(lexer, error);
    if (token->type != TOK_SEMICOLON)
    {
        *error = ERROR_CODE;
        return NULL;
    }

    lexer_pop(lexer);
    token = lexer_peek(lexer, error);

    if (token->type != TOK_DO)
    {
        *error = ERROR_CODE;
        return NULL;
    }
    lexer_pop(lexer);

    struct node_compound_list *do_group = compound_list_create(lexer, error);
    token = lexer_peek(lexer, error);
    if (token->type != TOK_DONE)
    {
        *error = ERROR_CODE;
        return NULL;
    }
    return for_init(word, word_list, do_group);

}

/* Cf. documentation in `parser.h`. */
struct node_for *for_create(struct lexer *lexer, int *error)
{
    struct token *token = lexer_peek(lexer, error);

    if (token->type != TOK_FOR)
    {
        *error = ERROR_CODE;
        return NULL;
    }
    lexer_pop(lexer);
    token = lexer_peek(lexer, error);
    if (token->type != TOK_WORD)
    {
        *error = ERROR_CODE;
        return NULL;
    }
    lexer_pop(lexer);
    char *word = token->value;
    token = lexer_peek(lexer, error);
    if (token->type != TOK_SEMICOLON)
    {
        consume(lexer, error);
        token = lexer_peek(lexer, error);
        if (token->type != TOK_IN)
        {
            *error = ERROR_CODE;
            return NULL;
        }
    }

    lexer_pop(lexer);
    if (token->type == TOK_SEMICOLON)
    {
        return for_create_semicolon(lexer, word, token, error);
    }

    else if (token->type == TOK_IN)
    {
        return for_create_in(lexer, word, token, error);
    }

    *error = ERROR_CODE;
    return NULL;
}
