/**
** \file src/parser/simple_command.c
**
** \brief Handle grammar rules for a simple_command and differenciate words
**  from assignments in the ast.
**
** \author 42Schmittos
*/

#include <stdlib.h>
#include <string.h>

#include "exec.h"
#include "list.h"
#include "parser.h"
#include "token.h"
#include "utils.h"

/**
** Free a node of type "simple_command".
**
** \param node the `ast_node` we want to free.
*/
void simple_command_free(struct ast_node *node)
{
    struct node_simple_command *simple_command = cast_void(node);

    list_destroy(simple_command->words);

    for (size_t i = 0; i < simple_command->nb_assignments; i++)
    {
        struct node_assignment *assignment = simple_command->assignments[i];
        assignment->base.free(&assignment->base);
    }

    free(simple_command->assignments);
    free(simple_command);
}

/**
** Printing function for node of type simple_command.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *simple_command_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_simple_command *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);
    for (size_t i = 0; i < list_size(n->words); i++)
    {
        fprintf(file, "    \"%p\" [label=\"%s\"];\n",
            cast_void(list_get(n->words, i)), cast_char(list_get(n->words, i)));
        fprintf(file, "    \"%p\" -> \"%p\";\n",
                                        ptr, cast_void(list_get(n->words, i)));
    }

    for (size_t i = 0; i < n->nb_assignments; i++)
    {
        void *child = n->assignments[i]->base.print(&n->assignments[i]->base,
                                                                        file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, child);
    }

    for (size_t i = 0; i < n->nb_redirections; i++)
    {
        void *child = n->redirections[i]->base.print(&n->redirections[i]->base,
                file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, child);
    }

    return ptr;
}

/* Cf. documentation in `parser.h`. */
struct node_simple_command *simple_command_init(void)
{

    struct node_simple_command *simple_cmd = calloc(1, sizeof(*simple_cmd));

    simple_cmd->base.free = simple_command_free;
    simple_cmd->base.exec = exec_simple_command;
    simple_cmd->base.print = simple_command_print;

    simple_cmd->base.name = "simple command";

    simple_cmd->words = list_init(NULL, free);

    return simple_cmd;
}

/* Cf. documentation in `parser.h`. */
struct node_simple_command *simple_command_add_word(
                                            struct node_simple_command *node,
                                            struct token *token)
{
    char *word = strdup(token->value);
    token_free(token);

    list_push_back(node->words, word);

    return node;
}

/* Cf. documentation in `parser.h` */
struct node_simple_command *simple_command_add_assignment(
                                            struct node_simple_command *node,
                                            struct token *token)
{
    struct node_assignment *child = assignment_init(token);

    node->nb_assignments += 1;
    node->assignments = realloc(node->assignments,
                    node->nb_assignments * sizeof(struct node_assignment *));
    node->assignments[node->nb_assignments - 1] = child;

    return node;
}

/* Cf. documentation in `parser.h` */
struct node_simple_command *simple_command_create(struct lexer *lexer,
                                                                    int *error)
{
    struct node_simple_command *simple_cmd = NULL;

    simple_cmd = prefix(simple_cmd, lexer, error);
    simple_cmd = element(simple_cmd, lexer, error);

    return simple_cmd;
}
