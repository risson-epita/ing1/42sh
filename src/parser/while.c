/**
** \file src/parser/while.c
**
** \brief Basic grammar parser for a while, can be used on basic while commands.
**
** \author 42Schmittos
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "token.h"
#include "utils.h"

/**
** Free a node of type while.
**
** \param node the `ast_node` structure we want to free.
*/
void while_free(struct ast_node *node)
{
    struct node_while *node_while = cast_void(node);

    node_while->condition->base.free(&node_while->condition->base);
    node_while->do_group->base.free(&node_while->do_group->base);

    free(node_while);
}

/**
** Printing function for node of type while.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *while_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_while *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    struct node_compound_list *condition = n->condition;
    condition->base.print(&condition->base, file);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(condition));

    struct node_compound_list *do_group = n->do_group;
    do_group->base.print(&do_group->base, file);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(do_group));

    return ptr;
}

/**
** Initialize a node of type while.
**
** \param condition the 'node_compound_list' structure after the while.
** \param do_group 'node_compound_list' structure after the do.
**
** \return a node_while structure.
*/
struct node_while *while_init(struct node_compound_list *condition,
                                            struct node_compound_list *do_group)
{
    struct node_while *node_while = calloc(1, sizeof(struct node_while));

    node_while->base.free = while_free;
    node_while->base.exec = exec_while;
    node_while->base.print = while_print;

    node_while->base.name = "while";

    node_while->condition = condition;
    node_while->do_group = do_group;

    return node_while;
}

/* Cf. documentation in `parser.h`. */
struct node_while *while_create(struct lexer *lexer, int *error)
{
    struct token *token = lexer_peek(lexer, error);

    if (token->type != TOK_WHILE)
    {
        *error = 2;
        return NULL;
    }
    lexer_pop(lexer);
    struct node_compound_list *condition = compound_list_create(lexer, error);

    token = lexer_peek(lexer, error);
    if (token->type != TOK_DO)
    {
        *error = 2;
        if (condition)
            condition->base.free(&condition->base);
        return NULL;
    }
    lexer_pop(lexer);
    struct node_compound_list *do_group = compound_list_create(lexer, error);

    token = lexer_peek(lexer, error);
    if (token->type != TOK_DONE)
    {
        *error = 2;
        if (do_group)
            do_group->base.free(&do_group->base);
        return NULL;
    }

    return while_init(condition, do_group);
}
