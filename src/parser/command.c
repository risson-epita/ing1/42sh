/**
** \file src/parser/command.c
**
** \brief handle the initialisation and the creation of a command node and free
**  it.
**
** \author 42Schmittos
*/

#include <string.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "utils.h"

/**
** Free a command node.
**
** \param node the 'node' structure we want to free.
*/
void command_free(struct ast_node *node)
{
    struct node_command *command = cast_void(node);

    if (command->type == SIMPLE_COMMAND)
    {
        struct node_simple_command *child = cast_void(command->child);
        child->base.free(&child->base);
    }
    else if (command->type == SHELL_COMMAND)
    {
        struct node_shell_command *child = cast_void(command->child);
        child->base.free(&child->base);
    }

    free(command);
}

/**
** Printing function for node of type command.
**
** \param node the `struct ast_node` structure to print.
**
** \param file the `FILE` where to print the ast_node.
*/
void *command_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_command *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    if (n->type == SIMPLE_COMMAND)
    {
        struct node_simple_command *child = cast_void(n->child);
        child->base.print(&child->base, file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(child));
    }
    else if (n->type == SHELL_COMMAND)
    {
        struct node_shell_command *child = cast_void(n->child);
        child->base.print(&child->base, file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(child));
    }

    else if (n->type == FUNCTION)
    {
        struct node_function *child = cast_void(n->child);
        child->base.print(&child->base, file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(child));
    }

    return ptr;
}

/**
** Initialize a node of type command and search if we are a shell_command or a
** simple_command.
**
** \param type the 'type' enum we use to know on which command we are.
** \param child the 'child' structure we use to init our union.
**
** \return a command node.
*/
struct node_command *command_init(enum command_child_type type,
                                       struct ast_node *child)
{
    struct node_command *command = calloc(1, sizeof(struct node_command));

    command->base.free = command_free;
    command->base.exec = exec_command;
    command->base.print = command_print;

    command->base.name = "command";

    command->child = child;
    command->type = type;

    return command;
}

/* Cf. documentation in `parser.h`. */
struct node_command *command_create(struct lexer *lexer, int *error)
{
    struct node_command *node_command = NULL;
    struct node_shell_command *node_shell_command
                                        = shell_command_create(lexer, error);
    struct node_simple_command *node_simple_command = NULL;
    struct node_function *node_function = NULL;

    if (node_shell_command != NULL)
        node_command = command_init(SHELL_COMMAND, &node_shell_command->base);

    else if (!*error && (node_function = function_create(lexer, error)) != NULL)
        node_command = command_init(FUNCTION, &node_function->base);

    else if (!*error && (node_simple_command =
                                simple_command_create(lexer, error)) != NULL)
        node_command = command_init(SIMPLE_COMMAND, &node_simple_command->base);

    return node_command;
}
