/**
** \file src/parser/parser.h
**
** \brief Header for all the parser.
**
** \author 42Schmittos
*/

#ifndef PARSER_H
#define PARSER_H

#include <stdio.h>
#include <stdlib.h>

#include "lexer.h"
#include "token.h"

struct ast_node;

/** Exec function for the node */
typedef int (*node_exec)(struct ast_node *node);
/** Free function for the node */
typedef void (*node_free)(struct ast_node *node);
/** Print function for the node */
typedef void *(*node_print)(struct ast_node *node, FILE *file);

/** Represent an AST */
struct ast_node
{
    /** Represent a string */
    char *name;
    /** Free function */
    node_free free;
    /** Exec function */
    node_exec exec;
    /** Exec print */
    node_print print;
};

/** Represent an EOL node in the AST*/
struct node_eol
{
    /** Represent the AST */
    struct ast_node base;
};

/** Represent a node_list in the AST */
struct node_list
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent a list of and_or node in an array */
    struct node_and_or **and_or;
    /** Number of and_or */
    size_t nb_and_or;
};

/** Represent a node_pipeline node in the AST */
struct node_and_or
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the list of pipeline in a node_and_or */
    struct node_pipeline **pipelines;
    /** Represent the number of pipeline in a node_and_or */
    size_t nb_pipelines;
    /** Represent the list of unary operators */
    char **operators;
    /** Represent the number of operators in a node_and_or */
    size_t nb_operators;
};

/** Represent a node_pipeline node in the AST */
struct node_pipeline
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the negation in a node_pipeline */
    struct node_command **commands;
    /** Represent the number of commands in a node_pipeline */
    size_t nb_commands;
    /** Represent the negation in a node_pipeline */
    int negation;
};

/** Represent the command type enum */
enum command_child_type
{
    /** Represent the type simple command */
    SIMPLE_COMMAND,
    /** Represent the type shel command */
    SHELL_COMMAND,
    /** Represent the type functions */
    FUNCTION
};

/** Represent a node_command node in the AST */
struct node_command
{
    /** Represent an AST */
    struct ast_node base;
    /** Represent  */
    struct ast_node *child;
    /** Represent a command enum */
    enum command_child_type type;
};

/** Represent a node_simple_command int the AST */
struct node_simple_command
{
    /** Represent the AST */
    struct ast_node base;
    /** Allows to keep all children words */
    struct list *words;
    /** Allows to keep all children assignments */
    struct node_assignment **assignments;
    /** Allows to keep all children redirections */
    struct node_redirection **redirections;
    /** Count number of children assignments */
    size_t nb_assignments;
    /** Count number of children redirections */
    size_t nb_redirections;
};

/** Represent the shell_command enum */
enum shell_command_child_type
{
    /** Rule_for */
    FOR,
    /** Rule_while */
    WHILE,
    /** Rule_until */
    UNTIL,
    /** Rule_case */
    CASE,
    /** Rule_if */
    IF,
    /** Compound list */
    COMPOUND_LIST
};

/** Represent a shell_command node in the AST */
struct node_shell_command
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the shell_command enum */
    enum shell_command_child_type type;
    /** Represent children node of the AST */
    struct ast_node *child;
    /** The list of redirecctions associated to the shell_command */
    struct node_redirection **redirections;
    /** The number of redirections in the list */
    size_t nb_redirections;
};

/** Represent a node_function in the AST */
struct node_function
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the word in the function_node */
    char *word;
    /** Represent the shell_command in the function_node */
    struct node_shell_command *shell_command;
};

/** Represent a redirection node in the AST */
struct node_redirection
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the IO number */
    int io;
    /** The type of the redirection */
    enum token_type type;
    /** Represent the redirection */
    char *value;
};

/** Represent an assignment node in the AST */
struct node_assignment
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the left body of an assignment  */
    char *name;
    /** Represent the right body of an assignment */
    char *value;
};

/** Represent a node_compound_list node in the AST */
struct node_compound_list
{
    /** Represent the AST */
    struct ast_node base;
    /** Allows to keep all children commands */
    struct node_and_or **and_ors;
    /** Count number of children commands */
    size_t nb_and_or;
};

/** Represent a node_for node in the AST */
struct node_for
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the for index */
    char *word;
    /** Represent the list of words */
    struct list *word_list;
    /** Represent the number of words */
    size_t nb_word;
    /** Represent the for body */
    struct node_compound_list *do_group;
};

/** Represent a node_while node in the AST */
struct node_while
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the while condition in a node_while */
    struct node_compound_list *condition;
    /** Represent the while condition in a node_while */
    struct node_compound_list *do_group;
};

/** Represent a node_until node in the AST */
struct node_until
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the until condition in a node_until */
    struct node_compound_list *condition;
    /** Represent the until condition in a node_until */
    struct node_compound_list *do_group;
};

/** Represent a node_case in the AST */
struct node_case
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the case_clause of the node_case */
    struct node_case_clause *node_case_clause;
    /** Represent the word of the grammar rule */
    char *word;
};

/** Represent a rule to know if we are a if or an elif */
enum expected_if_type
{
    /** Represnt the if case. */
    PARSE_IF,
    /** Represent the elif.case */
    PARSE_ELIF
};

/** Represent a node_if node in the AST */
struct node_if
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the then condition in a node_if */
    struct node_compound_list *condition;
    /** Represent the if condition in a node_if */
    struct node_compound_list *if_body;
    /** Represent the else_clause in a node_if */
    struct node_else_clause *else_clause;
};

/** Represent the else_clause enum */
enum else_clause_child_type
{
    /** Token else */
    ELSE,
    /** Token elif */
    ELIF,
};

/** Represent an else_clause in the AST */
struct node_else_clause
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the children of an else_clause */
    struct ast_node *child;
    /** Represent the else_clause enum */
    enum else_clause_child_type type;
};

/** Represent a node_else_clause in the AST */
struct node_case_clause
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the list of case_items */
    struct node_case_item **case_items;
    /** Represent the size of the previous list */
    size_t nb_case_item;
};

/** Represent a node_case_item in the AST */
struct node_case_item
{
    /** Represent the AST */
    struct ast_node base;
    /** Represent the list of words */
    char **words;
    /** Represent the size of the previous list */
    size_t nb_word;
    /** Represent the compound_list of the struct */
    struct node_compound_list *compound_list;
};

/**
** Create a node_list node.
**
** \param lexer the 'lexer' structure.
** \param error pointer used to keep track of parsing errors.
**
** \return a node_list node.
*/
struct node_list *nlist_create(struct lexer *lexer, int *error);

/**
** Initialize an eol node.
**
** \return an eol node.
*/
struct node_eol *eol_init(void);

/**
** Parse a node_redirection.
**
** \param lexer the 'lexer' structure.
**
** \param error the error tracker.
**
** \return a node_redirection node.
*/
struct node_redirection *redirection_create(struct lexer *lexer, int *error);
/**
**  Add a node_redirection as a child to a simple_command.
**
** \param node the node_simple_command node.
**
** \param child the node_redirection to add.
**
** \return a node_simple_command node
*/
struct node_simple_command *simple_command_add_redirection (
        struct node_simple_command *node, struct node_redirection *child);

/**
**  Add a node_redirection as a child to a shell_command
**
** \param node the node_shell_command node
**
** \param child the node_redirection to add.
**
** \return a node_shell_command node
*/
struct node_shell_command *shell_command_add_redirection (
        struct node_shell_command *node, struct node_redirection *child);

/**
** Initialize a node_simple_command node.
**
** \return a node_simple_command node.
*/
struct node_simple_command *simple_command_init(void);

/**
** Add a node_simple_command in the ast which parse the word.
**
** \param node the 'node_simple_command' structure we add to the ast.
** \param token the 'token' structure.
**
** \return a node_simple_command node.
*/
struct node_simple_command *simple_command_add_word(
                                            struct node_simple_command *node,
                                            struct token *token);
/**
** Create a simple command.
**
** \param lexer the 'lexer' structure.
** \param error the error tracker.
**
** \return a node_simple_command node.
*/
struct node_simple_command *simple_command_create(struct lexer *lexer,
                                                                    int *error);

/**
** Add a node_simple_command in the ast which parse the assignment word.
**
** \param node the 'node_simple_command' structure.
** \param token the 'token' structure.
**
** \return a node_simple_command node.
*/
struct node_simple_command *simple_command_add_assignment(
                                            struct node_simple_command *node,
                                            struct token *token);

/**
** Initialisation function for an assignment_word node.
**
** \param token the 'token' structure we want to init.
**
** \return the node_assignment we just created.
*/
struct node_assignment *assignment_init(struct token *token);

/**
** Create a node_compound_list node.
**
** \param lexer the 'lexer' structure.
** \param error pointer used to keep track of parsing errors
**
** \return a node_compound_list node.
*/
struct node_compound_list *compound_list_create(struct lexer *lexer,
                                                                    int *error);

/**
** Create a command
**
** \param lexer the 'lexer' structure we give to a command.
** \param error the parsing error tracker
**
** \return a command node.
*/
struct node_command *command_create(struct lexer *lexer, int *error);

/**
** Create a if node.
**
** \param lexer the 'lexer' structure.
** \param if_type is `if` or `elif`
** \param error pointer used to keep track of parsing errors
**
**\ return the function init_node_if.
*/
struct node_if *if_create(struct lexer *lexer, enum expected_if_type if_type,
                        int *error);

/**
** Create a node_pipeline.
**
** \param lexer the 'lexer' structure.
** \param error the return code error.
**
** \return the result of the function init_node_piepline on success.
** On failure just return NULL.
*/
struct node_pipeline *pipeline_create(struct lexer *lexer, int *error);

/**
** Function who consume all consecutive `\n`;
**
** \param lexer the lexer we want to consume
** \param error the error indicator.
*/
void consume(struct lexer *lexer, int *error);

/**
** Create a node_and_or.
**
** \param lexer the 'lexer' structure.
** \param error the return code error.
**
** \return the result of the function init_node_and_or on success.
** On failure just return NULL.
*/
struct node_and_or *and_or_create(struct lexer *lexer, int *error);

/**
** Create an else_clause.
**
** \param lexer the 'lexer' structure.
** \param error the return code error.
**
** \return the result of the function init_node_else_clause on success.
** On failure just return NULL.
*/
struct node_else_clause *else_clause_create(struct lexer *lexer, int *error);

/**
** Create a while node.
**
** \param lexer the 'lexer' structure.
** \param error pointer used to keep track of parsing errors
**
** \return the function init_node_while.
*/
struct node_while *while_create(struct lexer *lexer, int *error);

/*
** Create an until node.
**
** \param lexer the 'lexer' structure.
** \param error pointer used to keep track of parsing errors.
**
**\ return the function init_node_until.
*/
struct node_until *until_create(struct lexer *lexer, int *error);

/**
** Create a node_for node.
**
** \param lexer the 'lexer' structure.
** \param error the parsing error tracker
**
** \return a node_for node.
*/
struct node_for *for_create(struct lexer *lexer, int *error);

/**
** Create a node_case node.
**
** \param lexer the 'lexer' structure.
** \param error the parsing error tracker
**
** \return a node_for node.
*/
struct node_case *case_create(struct lexer *lexer, int *error);

/**
** Create a node_case_clause node.
**
** \param lexer the 'lexer' structure.
** \param error the parsing error tracker
**
** \return a node_for node.
*/
struct node_case_clause *case_clause_create(struct lexer *lexer, int *error);

/**
** Create a node_case_item.
**
** \param lexer the 'lexer' structure.
** \param error the parsing error tracker
**
** \return a node_case_item node.
*/
struct node_case_item *case_item_create(struct lexer *lexer, int *error);

/**
** Initialize a node of type shell_command.
**
** \param type the 'type' enum we use to know which shell command type we are.
** \param child the 'child' structure.
**
** \return a node_shell_command node.
*/
struct node_shell_command *shell_command_init(
                                            enum shell_command_child_type type,
                                            struct ast_node *child);

/**
** Create a shell_command node.
**
** \param lexer the 'lexer' structure.
** \param error the parsing error tracker
**
** \return a node_shell_command node.
*/
struct node_shell_command *shell_command_create(struct lexer *lexer,
                                                                    int *error);

/**
** The parsing function to parse functions.
**
** \param lexer the current lexer.
** \param error the error tracker.
** \return the parsed function or NULL if there is no function.
*/
struct node_function *function_create(struct lexer *lexer, int *error);

/**
** The ast printer, for debugging.
**
** \param ast the ast to print.
*/
void ast_print(struct ast_node *ast);

/**
** Create a prefix.
**
** \param simple_cmd the 'simple_cmd' structure we use to init or create a
**  prefix.
** \param lexer the 'lexer' structure.
** \param error the error tracker.
**
** \return a simple_command node.
*/
struct node_simple_command *prefix(struct node_simple_command *simple_cmd,
                                   struct lexer *lexer, int *error);
/**
** Create an element.
**
** \param simple_cmd the 'simple_cmd' structure we use to init or create an
**  element.
** \param lexer the 'lexer' structure.
** \param error the error tracker.
**
** \return a simple_command node.
*/
struct node_simple_command *element(struct node_simple_command *simple_cmd,
                                    struct lexer *lexer, int *error);
/**
** Entry function of the grammar parser, create the ast.
**
** \param lexer the `lexer` structure to parse.
** \param error the parsing error tracker
**
** \return the ast created.
*/
struct ast_node *parse(struct lexer *lexer, int *error);

#endif /* ! PARSER_H */
