/**
** \file src/parser/eol.c
**
** \brief handle the initialisation of an eol node and free it.
**
** \author 42Schmittos
*/

#include <stdlib.h>

#include "exec.h"
#include "parser.h"
#include "utils.h"

/**
** Free an eol node.
**
** \param node the 'node' we want to free.
*/
void eol_free(struct ast_node *node)
{
    struct node_eol *eol = cast_void(node);
    free(eol);
}

/**
** Printing function for node of type EOL.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *eol_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);

    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    return ptr;
}

/* Cf documentation in `parser.h`. */
struct node_eol *eol_init(void)
{
    struct node_eol *eol = calloc(1, sizeof(struct node_eol));

    eol->base.free = eol_free;
    eol->base.exec = exec_eol;
    eol->base.print = eol_print;

    eol->base.name = "EOL";

    return eol;
}
