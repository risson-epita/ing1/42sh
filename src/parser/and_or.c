/**
** \file src/parser/and_or.c
**
** \brief handle the initialisation and the creation of a and_or_node and free
**  it.
**
** \author 42Schmittos
*/

#include <err.h>
#include <string.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "utils.h"

/**
** Free a node of type and_or.
**
** \param node the `ast_node` structure we want to free.
*/
void and_or_free(struct ast_node *node)
{
    struct node_and_or *and_or = cast_void(node);

    for (size_t i = 0; i < and_or->nb_pipelines; i++)
    {
        struct node_pipeline *child = and_or->pipelines[i];
        child->base.free(&child->base);
    }

    for (size_t i = 0; i < and_or->nb_operators; i++)
    {
        free(and_or->operators[i]);
    }

    free(and_or->operators);
    free(and_or->pipelines);
    free(and_or);
}

/**
** Printing function for node of type and_or.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *and_or_print(struct ast_node *node, FILE *file)
{
    void *ptr= cast_void(node);
    struct node_and_or *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    for (size_t i = 0; i < n->nb_pipelines; i++)
    {
        n->pipelines[i]->base.print(&n->pipelines[i]->base, file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr,
                                                    cast_void(n->pipelines[i]));
    }

    for (size_t i = 0; i < n->nb_operators; i++)
    {
        char **words = n->operators;
        fprintf(file, "    \"%p\" [label=\"%s\"];\n", cast_void(ptr), words[i]);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(words[i]));
    }

    return ptr;
}

/**
** Initialize a node of type and_or.
** \return a node_and_or structure.
*/
struct node_and_or *and_or_init(void)
{
    struct node_and_or *and_or = calloc(1, sizeof(struct node_and_or));

    and_or->base.free = and_or_free;
    and_or->base.exec = exec_and_or;
    and_or->base.print = and_or_print;

    and_or->base.name = "and_or";

    return and_or;
}

/**
** add function for node of type and_or.
**
** \param node_and_or the `ast_node` structure where to add.
** \param token the `token` structure to add.
**
**\return the node_and_or with the new token inside.
*/
struct node_and_or *and_or_add_operator(struct node_and_or *node_and_or,
        struct token *token)
{
    node_and_or->operators = realloc(node_and_or->operators,
            sizeof(char *) * (node_and_or->nb_operators + 1));

    node_and_or->operators[node_and_or->nb_operators] = strdup(token->value);

    node_and_or->nb_operators++;

    token_free(token);

    return node_and_or;
}

/**
** add function for node of type and_or.
**
** \param node_and_or the `ast_node` structure where to add.
** \param node_pipeline the `node_pipeline` structure to add.
**
**\return the node_and_or with the new pipeline inside.
*/
struct node_and_or *and_or_add_pipeline(struct node_and_or *node_and_or,
        struct node_pipeline *node_pipeline)
{
    node_and_or->pipelines = realloc(node_and_or->pipelines,
            sizeof(struct node_pipeline *) * (node_and_or->nb_pipelines + 1));

    node_and_or->pipelines[node_and_or->nb_pipelines] = node_pipeline;

    node_and_or->nb_pipelines++;

    return node_and_or;
}

/* Cf. documentation in `parser.h`. */
struct node_and_or *and_or_create(struct lexer *lexer, int *error)
{
    struct node_pipeline *node_pipeline = pipeline_create(lexer, error);
    if (!node_pipeline)
    {
        return NULL;
    }

    struct node_and_or *node_and_or = and_or_init();
    node_and_or = and_or_add_pipeline(node_and_or, node_pipeline);

    struct token *token = lexer_peek(lexer, error);

    while ((token && token->type == TOK_AND)
            || (token && token->type == TOK_OR))
    {
        lexer_pop(lexer);
        node_and_or = and_or_add_operator(node_and_or, token);
        consume(lexer, error);

        struct node_pipeline *node_pipeline = pipeline_create(lexer, error);
        if (!node_pipeline)
        {
            *error = ERROR_CODE;
            node_and_or->base.free(&node_and_or->base);
            return NULL;
        }

        node_and_or = and_or_add_pipeline(node_and_or, node_pipeline);

        token = lexer_peek(lexer, error);
    }

    return node_and_or;
}
