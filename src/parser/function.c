/**
** \file src/parser/function.c
**
** \brief handle the initialisation and the creation of a function node
**  and free it.
**
** \author 42Schmittos
*/

#include <err.h>
#include <string.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "parser.h"
#include "utils.h"
#include "shell.h"

/**
** The comparaison function for the node_function.
**
** \param lhs the node_function to compare.
** \param rhs the node_function to compare.
**
** \return an integer less, equal or higher if rhs is smaller, equal or higher
**         than lhs.
*/
int function_cmp(void *lhs, void* rhs)
{
    struct node_function *function = lhs;
    char *word = rhs;
    int rc = strcmp(function->word, word);
    if (rc < 0)
        return LIST_LOWER;
    if (rc > 0)
        return LIST_BIGGER;
    return LIST_EQUAL;
}

/**
** The clear function for the node_function.
**
** \param p the function to clear.
*/
void function_clear(void *p)
{
    struct node_function *node_function = p;
    node_function->base.free(&node_function->base);
}

/**
** The free function for the node_function.
**
** \param node the ast_node in the node_function.
*/
void function_free(struct ast_node *node)
{
    struct node_function *node_function = cast_void(node);

    free(node_function->word);
    struct node_shell_command *child = node_function->shell_command;
    child->base.free(&child->base);
    free(node_function);
}

/**
** The print function for the node_function.
**
** \param node the ast_node in the node_function.
** \param file the dot file to write to.
*/
void *function_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_function *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    char *word = n->word;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", cast_void(word), word);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(word));

    struct node_shell_command *shell_command = n->shell_command;
    shell_command->base.print(&shell_command->base, file);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(shell_command));

    return ptr;
}

/**
** The function who init the node_function.
**
** \return the initialized node_function.
*/
struct node_function *function_init(void)
{
    struct node_function *node_function = calloc(1,
                                                sizeof(struct node_function));

    node_function->base.free = function_free;
    node_function->base.exec = exec_function;
    node_function->base.print = function_print;

    node_function->base.name = "function";
    return node_function;
}

/**
** Declaration part of the parsing of a function.
** \param token the current token where we are in the string.
** \param lexer the lexer we use to parse.
** \param error the error tracker of the parser.
** \return a `node_function` structure just created.
*/
struct node_function *function_parse_declaration(struct token *token,
                                                struct lexer *lexer, int *error)
{
    struct node_function *node_function = function_init();
    if (token->type != TOK_WORD)
    {
        *error = ERROR_CODE;
        return NULL;
    }

    node_function->word = strdup(token->value);
    lexer_pop(lexer);

    token = lexer_peek(lexer, error);

    if (token->type != TOK_DPAREN)
    {
        if (token->type != TOK_LPAREN)
        {
            *error = ERROR_CODE;
            return NULL;
        }

        token_free(lexer_pop(lexer));
        token = lexer_peek(lexer, error);

        if (token->type != TOK_RPAREN)
        {
            *error = ERROR_CODE;
            return NULL;
        }
    }

    return node_function;
}

/* Cf. documentation in `parser.h`. */
struct node_function *function_create(struct lexer *lexer, int *error)
{
    struct token *token = lexer_peek(lexer, error);
    if (token->type == TOK_FUNCTION)
    {
        token_free(lexer_pop(lexer));
        token = lexer_peek(lexer, error);
    }
    else
    {
        struct token *next = lexer_peek_follow(lexer);
        if (!next || (next->type != TOK_DPAREN && next->type != TOK_LPAREN))
            return NULL;
    }

    struct node_function *node_function = function_parse_declaration(token,
                                                                lexer, error);

    if (node_function == NULL)
    {
        *error = ERROR_CODE;
        return NULL;
    }

    lexer_pop(lexer);
    consume(lexer, error);

    node_function->shell_command = shell_command_create(lexer, error);

    if (!node_function->shell_command)
    {
        *error = ERROR_CODE;
        return NULL;
    }

    struct node_function *existing_function = list_get(g_shell->functions,
                        list_find(g_shell->functions, node_function->word));

    if (existing_function != NULL)
        list_remove_eq(g_shell->functions, node_function->word);

    list_push_back(g_shell->functions, node_function);

    return node_function;
}
