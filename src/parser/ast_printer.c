/**
** \file src/parser/ast_printer.c
**
** \brief File for the debugging ast printer.
**
** \author 42Schmittos
*/

#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>

#include "parser.h"
#include "shell.h"

/**
** Create a dot file.
**
** \return the file created.
*/
static FILE *create_dot_file(void)
{
    time_t now;
    time(&now);
    char buf[sizeof "ast-2019-12-08T10:42:00Z.dot"];
    strftime(buf, sizeof buf, "ast-%FT%TZ.dot", gmtime(&now));
    FILE *file = fopen(buf, "w");
    if (!file)
        err(1, "failed to create ast dot file");

    char dot_base[] = "digraph G {\n";
    int rv = fprintf(file, "%s", dot_base);
    if (rv < 0)
        err(1, "failed to write to ast dot file");

    return file;
}

/**
** Close the file opened previously.
**
** \param file the `FILE` to close.
*/
static void close_dot_file(FILE *file)
{
    int rv = fprintf(file, "}\n");
    if (rv < 0)
        err(1, "failed to write to ast dot file");

    rv = fclose(file);
    if (rv != 0)
        err(1, "failed to close ast dot file");
}

/* Cf. documentation in `ast_printer.h`. */
void ast_print(struct ast_node *ast)
{
    FILE *file = create_dot_file();
    ast->print(ast, file);
    close_dot_file(file);
    shopt_unset("ast_print");
}
