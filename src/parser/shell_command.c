/**
** \file src/parser/shell_command.c
**
** \brief handle the initialisation and creation of a shell command node and
**  free it.
**
** \author 42Schmittos
*/

#include <err.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "utils.h"

/**
** The sub function to parse a if in a shell command.
**
** \param lexer the current lexer.
** \param node_shell_command the shell command being parsed.
** \param error the error tracker
** \return the shell command parsed or NULL if there is a parsing error
*/
struct node_shell_command *if_func(struct lexer *lexer,
        struct node_shell_command *node_shell_command, int *error)
{
    struct node_if *node_if = if_create(lexer, PARSE_IF, error);
    node_shell_command = shell_command_init(IF, &node_if->base);

    if (!lexer_peek(lexer, error) || lexer_peek(lexer, error)->type != TOK_FI)
    {
        *error = ERROR_CODE;
        if (node_if)
            node_if->base.free(&node_if->base);
        return NULL;
    }
    token_free(lexer_pop(lexer));
    return node_shell_command;
}

/**
** The sub function to parse a for in a shell command.
**
** \param lexer the current lexer.
** \param node_shell_command the shell command being parsed.
** \param error the error tracker
** \return the shell command parsed or NULL if there is a parsing error
*/
struct node_shell_command *for_func(struct lexer *lexer,
        struct node_shell_command *node_shell_command, int *error)
{
    struct node_for *node_for = for_create(lexer, error);
    node_shell_command = shell_command_init(FOR, &node_for->base);

    if (!lexer_peek(lexer, error) || lexer_peek(lexer, error)->type != TOK_DONE)
    {
        *error = ERROR_CODE;
        if (node_for)
            node_for->base.free(&node_for->base);
        return NULL;
    }
    token_free(lexer_pop(lexer));
    return node_shell_command;
}

/**
** The sub function to parse a case in a shell command.
**
** \param lexer the current lexer.
** \param node_shell_command the shell command being parsed.
** \param error the error tracker
** \return the shell command parsed or NULL if there is a parsing error
*/
struct node_shell_command *case_func(struct lexer *lexer,
        struct node_shell_command *node_shell_command, int *error)
{
    struct node_case *node_case = case_create(lexer, error);
    node_shell_command = shell_command_init(CASE, &node_case->base);

    if (!lexer_peek(lexer, error) || lexer_peek(lexer, error)->type != TOK_ESAC)
    {
        *error = ERROR_CODE;
        if (node_case)
            node_case->base.free(&node_case->base);
        return NULL;
    }
    token_free(lexer_pop(lexer));
    return node_shell_command;
}

/**
** The sub function to parse a while in a shell command.
**
** \param lexer the current lexer.
** \param node_shell_command the shell command being parsed.
** \param error the error tracker
** \return the shell command parsed or NULL if there is a parsing error
*/
struct node_shell_command *while_func(struct lexer *lexer,
        struct node_shell_command *node_shell_command, int *error)
{
    struct node_while *node_while = while_create(lexer, error);
    node_shell_command = shell_command_init(WHILE, &node_while->base);

    if (!lexer_peek(lexer, error) || lexer_peek(lexer, error)->type != TOK_DONE)
    {
        *error = ERROR_CODE;
        if (node_while)
            node_while->base.free(&node_while->base);
        return NULL;
    }
    token_free(lexer_pop(lexer));
    return node_shell_command;
}

/**
** The sub function to parse a until in a shell command.
**
** \param lexer the current lexer.
** \param node_shell_command the shell command being parsed.
** \param error the error tracker
** \return the shell command parsed or NULL if there is a parsing error
*/
struct node_shell_command *until_func(struct lexer *lexer,
        struct node_shell_command *node_shell_command, int *error)
{
    struct node_until *node_until = until_create(lexer, error);
    node_shell_command = shell_command_init(UNTIL, &node_until->base);

    if (!lexer_peek(lexer, error) || lexer_peek(lexer, error)->type != TOK_DONE)
    {
        *error = ERROR_CODE;
        if (node_until)
            node_until->base.free(&node_until->base);
        return NULL;
    }
    token_free(lexer_pop(lexer));
    return node_shell_command;
}

/**
** The sub function to parse a compound_list in a shell command,
** between '{' '}' and '(' ')'.
**
** \param lexer the current lexer.
** \param node_shell_command the shell command being parsed.
** \param error the error tracker
** \return the shell command parsed or NULL if there is a parsing error
*/
struct node_shell_command *brackets_parenthesis_func(struct lexer *lexer,
        struct node_shell_command *node_shell_command, int *error)
{
    struct token *stk = lexer_pop(lexer);
    enum token_type closing_type = stk->type ==
        TOK_LPAREN ? TOK_RPAREN : TOK_RBRACK;
    struct node_compound_list *node_compound_list = compound_list_create(lexer,
                                                                        error);
    struct token *token = lexer_peek(lexer, error);
    if (*error || token->type != closing_type)
    {
        *error = ERROR_CODE;
        return NULL;
    }
    token_free(stk);
    token_free(lexer_pop(lexer));
    node_shell_command = shell_command_init(COMPOUND_LIST,
            &node_compound_list->base);

    return node_shell_command;
}

/**
** Free a shell_command node.
**
** \param node the 'node' structure we want to free.
*/
void shell_command_free(struct ast_node *node)
{
    struct node_shell_command *shell_command = cast_void(node);
    shell_command->child->free(shell_command->child);

    free(shell_command);
}

/**
** Printing function for node of type shell_command.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *shell_command_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_shell_command *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    struct ast_node *child = n->child;
    child->print(child, file);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(child));

    for (size_t i = 0; i < n->nb_redirections; i++)
    {
        void *child = n->redirections[i]->base.print(&n->redirections[i]->base,
                file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, child);
    }

    return ptr;
}

/* Cf. documentation in `parser.h`. */
struct node_shell_command *shell_command_init(
                                            enum shell_command_child_type type,
                                            struct ast_node *child)
{
    struct node_shell_command *shell_command =
        calloc(1, sizeof(struct node_shell_command));

    shell_command->base.free = shell_command_free;
    shell_command->base.exec = exec_shell_command;
    shell_command->base.print = shell_command_print;

    shell_command->base.name = "shell command";

    shell_command->type = type;
    shell_command->child = child;

    return shell_command;
}

/* Cf. documentation in `parser.h`. */
struct node_shell_command *shell_command_create(struct lexer *lexer, int *error)
{
    struct token *token = lexer_peek(lexer, error);
    struct node_shell_command *node_shell_command = NULL;

    if (token->type == TOK_IF)
        node_shell_command = if_func(lexer, node_shell_command, error);

    else if (token->type == TOK_WHILE)
        node_shell_command = while_func(lexer, node_shell_command, error);

    else if (token->type == TOK_UNTIL)
        node_shell_command = until_func(lexer, node_shell_command, error);

    else if (token->type == TOK_FOR)
        node_shell_command = for_func(lexer, node_shell_command, error);

    else if (token->type == TOK_CASE)
        node_shell_command = case_func(lexer, node_shell_command, error);

    else if (token->type == TOK_LBRACK || token->type == TOK_LPAREN)
        node_shell_command =  brackets_parenthesis_func(lexer,
                node_shell_command, error);

    if (node_shell_command)
    {
        struct node_redirection *node_redirection = redirection_create(lexer,
                                                                        error);
        if (node_redirection)
            shell_command_add_redirection(node_shell_command, node_redirection);
    }

    if (*error)
        return NULL;

    return node_shell_command;
}
