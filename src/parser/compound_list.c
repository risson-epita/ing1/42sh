/**
** \file src/parser/compound_list.c
**
** \brief handle the initialisation and the creation of a compound_list node
**  and free it.
**
** \author 42Schmittos
*/

#include <err.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "utils.h"

/**
** Free a coumpound_list node.
**
** \param node the 'ast_node' structure.
*/
void compound_list_free(struct ast_node *node)
{
    struct node_compound_list *compound_list = cast_void(node);

    for (size_t i = 0; i < compound_list->nb_and_or; i++)
    {
        struct node_and_or *child = compound_list->and_ors[i];
        child->base.free(&child->base);
    }
    free(compound_list->and_ors);
    free(compound_list);
}

/**
** Printing function for node of type compound_list.
**
** \param node the `ast_node` structure to print.
** \param file the `FILE` where to print the ast.
*/
void *compound_list_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_compound_list *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    for (size_t i = 0; i < n->nb_and_or; i++)
    {
        struct node_and_or *child = n->and_ors[i];
        child->base.print(&child->base, file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(child));
    }

    return ptr;
}

/**
** Initialize a node_compound_list node.
**
** \return a node_compound_list node.
*/
struct node_compound_list *compound_list_init(void)
{
    struct node_compound_list *compound_list =
        calloc(1, sizeof(struct node_compound_list));

    compound_list->base.free = compound_list_free;
    compound_list->base.exec = exec_compound_list;
    compound_list->base.print = compound_list_print;

    compound_list->base.name = "compound list break";

    return compound_list;
}

struct node_compound_list *compound_list_add_and_or(
                                                struct node_compound_list *node,
                                                struct node_and_or *node_and_or)
{
    node->nb_and_or += 1;
    node->and_ors = realloc(node->and_ors,
                            node->nb_and_or * sizeof(struct node_and_or *));
    node->and_ors[node->nb_and_or - 1] = node_and_or;

    return node;
}

/**
** Check if the current token is a newline or a semicolon, if it is we consume
** it.
**
** \param lexer the 'lexer' structure.
** \param error the error indicator.
**
*/
static void consume_newlines(struct lexer *lexer, int *error)
{
    while (lexer_peek(lexer, error)->type == TOK_EOL)
    {
        token_free(lexer_pop(lexer));
    }

    if (lexer_peek(lexer, error)->type == TOK_EOF)
    {
        if (lexer->input->mode == M_CMDSTR)
            errx(1, "syntax error: unexpected end of file");
        errx(ERROR_CODE, "syntax error: unexpected end of file");
    }
}

/* Cf. documentation in `parser.h`. */
struct node_compound_list *compound_list_create(struct lexer *lexer, int *error)
{
    if (*error)
        return NULL;

    struct node_compound_list *node_compound_list = NULL;
    struct node_and_or *node_and_or = NULL;

    consume_newlines(lexer, error);

    while ((node_and_or = and_or_create(lexer, error)) != NULL)
    {
        if (!node_compound_list)
            node_compound_list = compound_list_init();

        node_compound_list = compound_list_add_and_or(node_compound_list,
                                                       node_and_or);

        if (lexer_peek(lexer, error)->type == TOK_SEMICOLON
                || lexer_peek(lexer, error)->type == TOK_AMPERSAND
                || lexer_peek(lexer, error)->type == TOK_EOL)
            token_free(lexer_pop(lexer));

        consume_newlines(lexer, error);
    }

    if (!node_compound_list)
        *error = ERROR_CODE;
    return node_compound_list;
}
