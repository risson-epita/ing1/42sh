/**
** \file src/parser/if.c
**
** \brief Basic grammar parser for a if, can be used on basic if commands.
**
** \author 42Schmittos
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "token.h"
#include "utils.h"

/**
** Free a node of type if.
**
** \param node the `ast_node` structure we want to free.
*/
void if_free(struct ast_node *node)
{
    struct node_if *node_if = cast_void(node);

    node_if->condition->base.free(&node_if->condition->base);
    node_if->if_body->base.free(&node_if->if_body->base);

    if (node_if->else_clause)
        node_if->else_clause->base.free(&node_if->else_clause->base);

    free(node_if);
}

/**
** Printing function for node of type if.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *if_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_if *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    struct node_compound_list *condition = n->condition;
    condition->base.print(&condition->base, file);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(condition));

    struct node_compound_list *if_body = n->if_body;
    if_body->base.print(&if_body->base, file);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(if_body));

    struct node_else_clause *else_clause = n->else_clause;
    if (else_clause)
    {
        else_clause->base.print(&else_clause->base, file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(else_clause));
    }

    return ptr;
}

/**
** Initialize a node of type if.
**
** \param condition the 'node_compound_list' structure after the if.
** \param if_body the 'node_compound_list' structure after the then.
** \param else_clause 'node_compound_list' structure after the else.
**
** \return a node_if structure.
*/
struct node_if *if_init(struct node_compound_list *condition,
                             struct node_compound_list *if_body,
                             struct node_else_clause *else_clause)
{
    struct node_if *node_if = calloc(1, sizeof(struct node_if));

    node_if->base.free = if_free;
    node_if->base.exec = exec_if;
    node_if->base.print = if_print;

    node_if->base.name = "if";

    node_if->condition = condition;
    node_if->if_body = if_body;
    node_if->else_clause = else_clause;

    return node_if;
}

/* Cf. documentation in `parser.h`. */
struct node_if *if_create(struct lexer *lexer, enum expected_if_type if_type,
                                                                    int *error)
{
    struct token *token = lexer_peek(lexer, error);
    if ((if_type == PARSE_IF && token->type != TOK_IF) ||
            (if_type == PARSE_ELIF && token->type != TOK_ELIF))
    {
        *error = ERROR_CODE;
        return NULL;
    }
    lexer_pop(lexer);
    struct node_compound_list *condition = compound_list_create(lexer, error);

    token = lexer_peek(lexer, error);
    if (token->type != TOK_THEN)
    {
        *error = ERROR_CODE;
        if (condition)
            condition->base.free(&condition->base);
        return NULL;
    }
    lexer_pop(lexer);
    struct node_compound_list *if_body = compound_list_create(lexer, error);

    struct node_else_clause *else_clause = else_clause_create(lexer, error);

    if (*error)
        return NULL;

    return if_init(condition, if_body, else_clause);
}
