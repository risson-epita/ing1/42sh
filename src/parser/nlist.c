/**
** \file src/parser/nlist.c
**
** \brief handle the initialisation and the creation of a list node
**  and free it.
**
** \author 42Schmittos
*/

#include <err.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "utils.h"

/**
** Free a node_list node.
**
** \param node the 'node' we want to free.
*/
void nlist_free(struct ast_node *node)
{
    struct node_list *node_list = cast_void(node);

    for (size_t i = 0; i < node_list->nb_and_or; i++)
    {
        struct node_and_or *child = node_list->and_or[i];
        child->base.free(&child->base);
    }
    free(node_list->and_or);
    free(node_list);
}

/**
** Printing function for node of type list.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *nlist_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_list *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    for (size_t i = 0; i < n->nb_and_or; i++)
    {
        struct node_and_or *child = n->and_or[i];
        child->base.print(&child->base, file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(child));
    }

    return ptr;
}

/**
** Add a node_and_or node in an array of node_and_or.
**
** \param node the `node_list` structure.
**
** \param and_or the `node_and_or`structure.
**
** \return a node_list node.
*/
struct node_list *nlist_add_and_or(struct node_list *node,
                                    struct node_and_or *and_or)
{
    node->nb_and_or += 1;
    node->and_or = realloc(node->and_or,
                            node->nb_and_or * sizeof(struct node_and_or *));
    node->and_or[node->nb_and_or - 1] = and_or;

    return node;
}

/**
** Initialize a node_list node.
**
** \return a node_list node.
*/
struct node_list *nlist_init(void)
{
    struct node_list *node_list = calloc(1, sizeof(struct node_list));

    node_list->base.free = nlist_free;
    node_list->base.exec = exec_list;
    node_list->base.print = nlist_print;

    node_list->base.name = "list";
    return node_list;
}

/* Cf. documentation in `parser.h`. */
struct node_list *nlist_create(struct lexer *lexer, int *error)
{
    struct node_and_or *node_and_or = and_or_create(lexer, error);
    if (! node_and_or)
    {
        *error = ERROR_CODE;
        return NULL;
    }

    struct node_list *node_list = nlist_init();
    node_list = nlist_add_and_or(node_list, node_and_or);

    struct token *token = lexer_peek(lexer, error);

    while (token && (token->type == TOK_SEMICOLON ||
                token->type == TOK_AMPERSAND))
    {
        lexer_pop(lexer);

        node_and_or = and_or_create(lexer, error);
        if (!node_and_or)
            return node_list;

        node_list = nlist_add_and_or(node_list, node_and_or);
        token = lexer_peek(lexer, error);
    }

    return node_list;
}
