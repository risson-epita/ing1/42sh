/**
** \file src/parser/until.c
**
** \brief Basic grammar parser for a until, can be used on basic until commands.
**
** \author 42Schmittos
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "token.h"
#include "utils.h"

/**
** Free a node of type until.
**
** \param node the `ast_node` structure we want to free.
*/
void until_free(struct ast_node *node)
{
    struct node_until *node_until = cast_void(node);

    node_until->condition->base.free(&node_until->condition->base);
    node_until->do_group->base.free(&node_until->do_group->base);

    free(node_until);
}

/**
** Printing function for node of type until.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *until_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_until *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    struct node_compound_list *condition = n->condition;
    condition->base.print(&condition->base, file);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(condition));

    struct node_compound_list *do_group = n->do_group;
    do_group->base.print(&do_group->base, file);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(do_group));

    return ptr;
}

/**
** Initialize a node of type until.
**
** \param condition the 'node_compound_list' structure after the until.
** \param do_group 'node_compound_list' structure after the do.
**
** \return a node_until structure.
*/
struct node_until *until_init(struct node_compound_list *condition,
                                            struct node_compound_list *do_group)
{
    struct node_until *node_until = calloc(1, sizeof(struct node_until));

    node_until->base.free = until_free;
    node_until->base.exec = exec_until;
    node_until->base.print = until_print;

    node_until->base.name = "until";

    node_until->condition = condition;
    node_until->do_group = do_group;

    return node_until;
}

/* Cf. documentation in `parser.h`. */
struct node_until *until_create(struct lexer *lexer, int *error)
{
    struct token *token = lexer_peek(lexer, error);

    if (token->type != TOK_UNTIL)
    {
        *error = 2;
        return NULL;
    }
    lexer_pop(lexer);
    struct node_compound_list *condition = compound_list_create(lexer, error);

    token = lexer_peek(lexer, error);
    if (token->type != TOK_DO)
    {
        *error = 2;
        if (condition)
            condition->base.free(&condition->base);
        return NULL;
    }
    lexer_pop(lexer);
    struct node_compound_list *do_group = compound_list_create(lexer, error);

    token = lexer_peek(lexer, error);
    if (token->type != TOK_DONE)
    {
        *error = 2;
        if (do_group)
            do_group->base.free(&do_group->base);
        return NULL;
    }

    return until_init(condition, do_group);
}
