/**
** \file src/parser/case_clause.c
**
** \brief Basic grammar parser for a case, can be used on basic case commands.
**
** \author 42Schmittos
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "token.h"
#include "utils.h"

/**
** Free a node of type case_clause.
**
** \param node the `ast_node` structure we want to free.
*/
void case_clause_free(struct ast_node *node)
{
    struct node_case_clause *node_case_clause = cast_void(node);

    for (size_t i = 0; i < node_case_clause->nb_case_item; i++)
    {
        struct node_case_item *child = node_case_clause->case_items[i];
        child->base.free(&child->base);
    }
    free(node_case_clause->case_items);
    free(node_case_clause);
}

/**
** Printing function for node of type case_clause.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *case_clause_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_case_clause *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    for (size_t i = 0; i < n->nb_case_item; i++)
    {
        struct node_case_item *case_item = n->case_items[i];
        case_item->base.print(&case_item->base, file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(case_item));
    }

    return ptr;
}

/**
** Initialize a node of type case_clause.
**
** \return a node_case structure.
*/
struct node_case_clause *case_clause_init(void)
{
    struct node_case_clause *node_case_clause = calloc(1,
                                            sizeof(struct node_case_clause));

    node_case_clause->base.free = case_clause_free;
    node_case_clause->base.exec = NULL;
    node_case_clause->base.print = case_clause_print;

    node_case_clause->base.name = "case_clause";
    return node_case_clause;
}

/* Cf. documentation in `parser.h`. */
struct node_case_clause *case_clause_add_case_item(
                                                 struct node_case_clause *node,
                                                 struct node_case_item *item)
{
    node->nb_case_item += 1;
    node->case_items = realloc(node->case_items, node->nb_case_item
                                            * sizeof(struct node_case_item *));
    node->case_items[node->nb_case_item - 1] = item;

    return node;
}

/* Cf. dcoumentation in `parser.h`. */
struct node_case_clause *case_clause_create(struct lexer *lexer, int *error)
{
    struct node_case_clause *node_case_clause = case_clause_init();
    struct token *token = lexer_peek(lexer, error);
    struct node_case_item *node_case_item = case_item_create(lexer, error);
    if (!node_case_item)
    {
        node_case_clause->base.free(&node_case_clause->base);
        *error = ERROR_CODE;
        return NULL;
    }

    node_case_clause = case_clause_add_case_item(node_case_clause,
        node_case_item);

    token = lexer_peek(lexer, error);
    while (token->type == TOK_DSEMICOLON)
    {
        lexer_pop(lexer);
        consume(lexer, error);
        node_case_item = case_item_create(lexer, error);
        if (!node_case_item)
            break;
        node_case_clause = case_clause_add_case_item(node_case_clause,
            node_case_item);

        token = lexer_peek(lexer, error);
    }

    consume(lexer, error);

    return node_case_clause;
}
