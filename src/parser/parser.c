/**
** \file src/parser/parser.c
**
** \brief Basic parser, can be used on basic shell commands.
**
** \author 42Schmittos
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "token.h"
#include "utils.h"

/* Cf. documentation in `parser.h`. */
struct node_simple_command *prefix(struct node_simple_command *simple_cmd,
                                   struct lexer *lexer, int *error)
{
    struct node_redirection *node_redirection
                                            = redirection_create(lexer, error);
    if (*error)
        return NULL;

    struct token *token = lexer_peek(lexer, error);
    if ((token && token->type == TOK_ASSIGNMENT) || node_redirection)
    {
        simple_cmd = simple_command_init();
    }

    while ((token && token->type == TOK_ASSIGNMENT) || node_redirection)
    {
        if (node_redirection)
        {
            simple_cmd = simple_command_add_redirection(simple_cmd,
                    node_redirection);
        }
        else
        {
            token = lexer_pop(lexer);
            simple_cmd = simple_command_add_assignment(simple_cmd, token);
        }

        node_redirection = redirection_create(lexer, error);
        if (*error)
        {
            return NULL;
            simple_cmd->base.free(&simple_cmd->base);
        }
        token = lexer_peek(lexer, error);
    }

    return simple_cmd;
}

/* Cf. documentation in `parser.h`. */
struct node_simple_command *element(struct node_simple_command *simple_cmd,
        struct lexer *lexer, int *error)
{
    struct node_redirection *node_redirection =
                                            redirection_create(lexer, error);
    struct token *token = lexer_peek(lexer, error);

    if (*error)
        return NULL;

    if (!simple_cmd && ((token && token->type == TOK_WORD) || node_redirection))
    {
        simple_cmd = simple_command_init();
    }

    while ((token && token->type == TOK_WORD) || node_redirection)
    {
        if (node_redirection)
        {
            simple_cmd = simple_command_add_redirection(simple_cmd,
                    node_redirection);
        }
        else
        {
            token = lexer_pop_command(lexer);
            simple_cmd = simple_command_add_word(simple_cmd, token);
        }

        node_redirection = redirection_create(lexer, error);
        if (*error)
        {
            return NULL;
            simple_cmd->base.free(&simple_cmd->base);
        }

        token = lexer_peek_command(lexer);
    }

    return simple_cmd;
}

/* Cf. documentation in `parser.h`. */
struct ast_node *parse(struct lexer *lexer, int *error)
{
    struct token *token = lexer_peek(lexer, error);

    if (!token || token->type == TOK_EOF)
    {
        return NULL;
    }

    if (token->type == TOK_EOL)
    {
        struct node_eol *eol = eol_init();
        return &eol->base;
    }

    struct node_list *node_list = nlist_create(lexer, error);
    if (!node_list)
    {
        warnx("syntax error near unexpected token `%s'",
                lexer_peek(lexer, error) ? lexer_peek(lexer, error)->value
                                            : "EOL");
        return &eol_init()->base;
    }

    return &node_list->base;
}
