/**
** \file src/parser/case_item.c
**
** \brief Basic grammar parser for a case, can be used on basic case commands.
**
** \author 42Schmittos
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "token.h"
#include "utils.h"

/**
** Free a node of type case_item.
**
** \param node the `ast_node` structure we want to free.
*/
void case_item_free(struct ast_node *node)
{
    struct node_case_item *node_case_item = cast_void(node);
    for (size_t i = 0; i < node_case_item->nb_word; i++)
    {
        char *child = node_case_item->words[i];
        free(child);
    }
    free(node_case_item->words);
    free(node_case_item);
}

/**
** Printing function for node of type case_item.
**
** \param node the `ast_node` structure to print.
** \param file the `FILE` where to print the ast.
*/
void *case_item_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_case_item *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    for (size_t i = 0; i < n->nb_word; i++)
    {
        char **words = n->words;
        fprintf(file, "    \"%p\" [label=\"%s\"];\n", cast_void(words[i]),
                                                                    words[i]);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(words[i]));
    }

    struct node_compound_list *compound_list = n->compound_list;
    compound_list->base.print(&compound_list->base, file);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(compound_list));

    return ptr;
}

/**
** Initialize a node of type case_item.
**
** \return a node_case structure.
*/
struct node_case_item *case_item_init(void)
{
    struct node_case_item *node_case_item = calloc(1,
        sizeof(struct node_case_item));

    node_case_item->base.free = case_item_free;
    node_case_item->base.exec = exec_case_item;
    node_case_item->base.print = case_item_print;

    node_case_item->base.name = "case_item";
    return node_case_item;
}

/**
** Add a word to a case_item struct.
**
** \param node the node where we want to add a word.

** \param word the word we want to add.
**
** \return a node_case structure.
*/
struct node_case_item *case_item_add_word(struct node_case_item *node,
                                                                    char *word)
{
    node->nb_word += 1;
    node->words = realloc(node->words, node->nb_word * sizeof(char *));
    node->words[node->nb_word - 1] = word;

    return node;
}

/**
** Find the next word and add it to the case_item struct.
** \param token the token where we are in the lexer.
** \param node_case_item the case_item where we will add the word.
** \param word the word we want to add.
** \param lexer the lexer where the word is.
*/
int node_case_pipe(struct token *token, struct node_case_item *node_case_item,
    char *word, struct lexer *lexer)
{
    int error = 0;
    lexer_pop(lexer);
    token = lexer_peek(lexer, &error);
    word = strdup(token->value);
    node_case_item = case_item_add_word(node_case_item, word);
    lexer_pop(lexer);
    return 0;
}

/* Cf. documentation in `parser.h`. */
struct node_case_item *case_item_create(struct lexer *lexer, int *error)
{
    struct token *token = lexer_peek(lexer,error);
    if (token->type == TOK_LPAREN)
    {
        token_free(lexer_pop(lexer));
        token = lexer_peek(lexer, error);
    }

    if (token->type != TOK_WORD)
        return NULL;

    struct node_case_item *node_case_item = case_item_init();

    char *word = strdup(token->value);
    node_case_item = case_item_add_word(node_case_item, word);

    token_free(lexer_pop(lexer));
    token = lexer_peek(lexer, error);
    while (token && token->type == TOK_PIPE)
    {
        *error = node_case_pipe(token, node_case_item, word, lexer);
        token = lexer_peek(lexer, error);
    }

    token = lexer_peek(lexer, error);
    if (!token || token->type != TOK_RPAREN)
    {
        node_case_item->base.free(&node_case_item->base);
        *error = ERROR_CODE;
        return NULL;
    }

    token_free(lexer_pop(lexer));
    consume(lexer, error);

    token = lexer_peek(lexer, error);

    node_case_item->compound_list = compound_list_create(lexer, error);

    return node_case_item;
}
