/**
** \file src/parser/assignment_word.c
**
** \brief handle the initialisation of an assignment_word node and free it.
**
** \author 42Schmittos
*/

#include <stdlib.h>
#include <string.h>

#include "exec.h"
#include "parser.h"
#include "token.h"
#include "utils.h"

/**
** Free an assignment_word node.
**
** \param node the 'node' we want to free.
*/
void assignment_free(struct ast_node *node)
{
    struct node_assignment *node_assignment = cast_void(node);
    free(node_assignment->name);
    free(node_assignment->value);
    free(node_assignment);
}

/**
** Printing function for a node of type assignement word.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print.
*/
void *assignment_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_assignment *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    fprintf(file, "    \"%p\" [label=\"%s\"];\n", cast_void(n->name), n->name);
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", cast_void(n->value),
                                                                    n->value);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(n->name));
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(n->value));

    return ptr;
}

/* Cf. documentation in `parser.h`. */
struct node_assignment *assignment_init(struct token *token)
{
    struct node_assignment *node_assignment = calloc(1, sizeof
        (struct node_assignment));

    node_assignment->base.free = assignment_free;
    node_assignment->base.print = assignment_print;

    node_assignment->base.name = "assignment";

    char *ptr = strchr(token->value, '=');
    node_assignment->name = strndup(token->value, ptr - token->value);
    node_assignment->value = strdup(ptr + 1);

    token_free(token);

    return node_assignment;
}
