/**
** \file src/parser/else_clause.c
**
** \brief grammar parser for a else clause, can be used on else_clause commands.
**
** \author 42Schmittos
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lexer.h"
#include "parser.h"
#include "token.h"
#include "utils.h"
#include "exec.h"

/**
** Free a node of type else_clause.
**
** \param node the `ast_node` structure we want to free.
*/
void else_clause_free(struct ast_node *node)
{
    struct node_else_clause *node_else_clause = cast_void(node);

    if (node_else_clause->type == ELSE)
    {
        struct node_compound_list *child = cast_void(node_else_clause->child);
        child->base.free(&child->base);
    }
    else
    {
        struct node_if *child = cast_void(node_else_clause->child);
        child->base.free(&child->base);
    }

    free(node_else_clause);
}

/**
** Printing function for node of type else_clause.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *else_clause_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_else_clause *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    if (n->type == ELSE)
    {
        struct node_compound_list *child = cast_void(n->child);
        child->base.print(&child->base, file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(child));
    }
    else
    {
        struct node_if *child = cast_void(n->child);
        child->base.print(&child->base, file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(child));
    }

    return ptr;
}

/**
** Initialize a node of type else_clause.
**
** \param type the 'else_clause_child_type' enum we use to know if we are an
**  else or an elif.
** \param child the 'ast_node' structure.
**
** \return a node_else_clause node.
*/
struct node_else_clause *else_clause_init(enum else_clause_child_type type,
        struct ast_node *child)
{
    struct node_else_clause *node_else_clause =
        calloc(1, sizeof(struct node_else_clause));

    node_else_clause->base.free = else_clause_free;
    node_else_clause->base.exec = exec_else_clause;
    node_else_clause->base.print = else_clause_print;

    node_else_clause->base.name = "else clause";

    node_else_clause->type = type;

    if (type == ELSE)
        node_else_clause->child = cast_void(child);
    else if (type == ELIF)
        node_else_clause->child = cast_void(child);

    return node_else_clause;
}

/* Cf. documentation in `parser.h`. */
struct node_else_clause *else_clause_create(struct lexer *lexer, int *error)
{
    struct token *token = lexer_peek(lexer, error);
    if (token->type == TOK_ELSE)
    {
        lexer_pop(lexer);
        struct node_compound_list *else_body = compound_list_create(lexer,
                                                                        error);
        return else_clause_init(ELSE, &else_body->base);
    }
    else if (token->type == TOK_ELIF)
    {
        struct node_if *elif_body = if_create(lexer, PARSE_ELIF, error);
        return else_clause_init(ELIF, &elif_body->base);
    }
    return NULL;
}
