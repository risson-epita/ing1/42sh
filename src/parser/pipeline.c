/**
** \file src/parser/pipeline.c
**
** \brief handle the initialisation and the creation of a pipeline_node and free
**  it.
**
** \author 42Schmittos
*/

#include <err.h>
#include <string.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "utils.h"

/**
** Free a node of type pipeline.
**
** \param node the `ast_node` structure we want to free.
*/
void pipeline_free(struct ast_node *node)
{
    struct node_pipeline *node_pipeline = cast_void(node);

    for (size_t i = 0; i < node_pipeline->nb_commands; i++)
    {
        struct node_command *node_command = node_pipeline->commands[i];
        node_command->base.free(&node_command->base);
    }

    free(node_pipeline->commands);
    free(node_pipeline);
}

/**
** Printing function for node of type pipeline.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *pipeline_print(struct ast_node *node, FILE *file)
{
    void *ptr= cast_void(node);
    struct node_pipeline *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    for (size_t i = 0; i < n->nb_commands; i++)
    {
        n->commands[i]->base.print(&n->commands[i]->base, file);
        fprintf(file, "    \"%p\" -> \"%p\";\n", ptr,
                                                    cast_void(n->commands[i]));
    }
    return ptr;
}

/**
** Initialize a node of type pipeline.
**\param negation the int who check if we are in a negation or note;
** \return a node_pipeline structure.
*/
struct node_pipeline *pipeline_init(int negation)
{
    struct node_pipeline *pipeline = calloc(1, sizeof(struct node_pipeline));

    pipeline->base.free = pipeline_free;
    pipeline->base.exec = exec_pipeline;
    pipeline->base.print = pipeline_print;

    pipeline->base.name = "pipeline";
    pipeline->negation = negation;

    return pipeline;
}

void consume(struct lexer *lexer, int *error)
{
    while (lexer_peek(lexer, error)->type == TOK_EOL)
        token_free(lexer_pop(lexer));

    if (lexer_peek(lexer, error)->type == TOK_EOF)
    {
        if (lexer->input->mode == M_CMDSTR)
            errx(1, "syntax error: unexpected end of file");
        errx(ERROR_CODE, "syntax error: unexpected end of file");
    }
}

/**
** add function for node of type pipeline.
**
** \param node_pipeline the `node_pipeline` structure where to add.
** \param node_command the `node_command` structure to add.
**
**\return the pipeline with the new token inside.
*/
struct node_pipeline *pipeline_add_command(struct node_pipeline *node_pipeline,
        struct node_command *node_command)
{
    node_pipeline->commands = realloc(node_pipeline->commands,
            sizeof(struct node_command *) * (node_pipeline->nb_commands + 1));

    node_pipeline->commands[node_pipeline->nb_commands] = node_command;

    node_pipeline->nb_commands += 1;

    return node_pipeline;
}

/* Cf. documentation in `parser.h`. */
struct node_pipeline *pipeline_create(struct lexer *lexer, int *error)
{
    struct token *token = lexer_peek(lexer, error);

    int negation = 0;
    if (token->type == TOK_EXCLAMATION)
    {
        negation = 1;
        lexer_pop(lexer);
    }

    struct node_pipeline *node_pipeline = pipeline_init(negation);

    struct node_command *node_command = command_create(lexer, error);
    if (!node_command)
    {
        node_pipeline->base.free(&node_pipeline->base);
        return NULL;
    }

    node_pipeline = pipeline_add_command(node_pipeline, node_command);

    token = lexer_peek(lexer, error);
    while (token && token->type == TOK_PIPE)
    {
        lexer_pop(lexer);
        consume(lexer, error);

        node_command = command_create(lexer, error);
        if (!node_command)
        {
            *error = ERROR_CODE;
            node_pipeline->base.free(&node_pipeline->base);
            return NULL;
        }

        node_pipeline = pipeline_add_command(node_pipeline, node_command);

        token = lexer_peek(lexer, error);
    }

    return node_pipeline;
}
