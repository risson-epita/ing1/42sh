/**
** \file src/parser/redirection.c
**
** \brief Basic grammar parser for a redirection, can be used on basic
**        redirection commands.
**
** \author 42Schmittos.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "token.h"
#include "utils.h"

/**
** Free a node of type "redirection".
**
** \param node the `ast_node` we want to free.
*/
void redirection_free(struct ast_node *node)
{
    struct node_redirection *node_redirection = cast_void(node);

    free(node_redirection->value);
    free(node_redirection->base.name);

    free(node_redirection);
}

/**
** Printing function for node of type redirection.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *redirection_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_redirection *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%d%s\"];\n", ptr, n->io, node->name);
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", cast_void(n->value),
            n->value);

    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(n->value));

    return ptr;
}

/* Cf. documentation in `parser.h`. */
struct node_redirection *redirection_init(struct token *token_redirection,
        struct token *token_word, int io)
{
    struct node_redirection *node_redirection = calloc(1,
                                    sizeof(struct node_redirection));
    node_redirection->base.free = redirection_free;
    node_redirection->base.print = redirection_print;

    node_redirection->base.name = strdup(token_redirection->value);

    node_redirection->type = token_redirection->type;
    node_redirection->io = io;
    node_redirection->value = strdup(token_word->value);

    token_free(token_redirection);
    token_free(token_word);

    return node_redirection;
}

/* Cf. documentation in `parser.h`. */
static inline int is_input_redirection(struct token *token)
{
    enum token_type t = token->type;
    if (t == TOK_LESS ||  t == TOK_DLESS || t == TOK_LESSAND
            || t == TOK_LESSMORE ||  t == TOK_DLESSDASH)
    {
        return 1;
    }
    return 0;
}

/* Cf. documentation in `parser.h`. */
struct node_simple_command *simple_command_add_redirection (
        struct node_simple_command *node, struct node_redirection *child)
{
    node->nb_redirections += 1;

    node->redirections = realloc(node->redirections,
                    node->nb_redirections * sizeof(struct node_redirection *));
    node->redirections[node->nb_redirections - 1] = child;

    return node;
}
/* Cf. documentation in `parser.h`. */
struct node_shell_command *shell_command_add_redirection (
        struct node_shell_command *node, struct node_redirection *child)
{
    node->nb_redirections += 1;

    node->redirections = realloc(node->redirections,
                    node->nb_redirections * sizeof(struct node_redirection *));
    node->redirections[node->nb_redirections - 1] = child;

    return node;
}

/* Cf. documentation in `parser.h`. */
struct node_redirection *redirection_create(struct lexer *lexer, int *error)
{
    struct token *token = lexer_peek(lexer, error);
    int io = -1;
    if (token->type == TOK_IONUMBER)
    {
        io = atoi(token->value);
        lexer_pop(lexer);
        token = lexer_peek(lexer, error);
    }

    if (!is_redirection(token))
        return NULL;

    if (io == -1 && is_input_redirection(token))
        io = STDIN_FILENO;
    else if (io == -1 && is_redirection(token))
        io = STDOUT_FILENO;

    struct token *token_redirection = lexer_pop(lexer);
    token = lexer_peek_command(lexer);

    if (token->type != TOK_WORD)
    {
        *error = ERROR_CODE;
        token_free(token_redirection);
        return NULL;
    }

    lexer_pop(lexer);

    return redirection_init(token_redirection, token, io);

}
