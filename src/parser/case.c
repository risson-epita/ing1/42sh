/**
** \file src/parser/case.c
**
** \brief Basic grammar parser for a case, can be used on basic case commands.
**
** \author 42Schmittos
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "token.h"
#include "utils.h"

/**
** Free a node of type case.
**
** \param node the `ast_node` structure we want to free.
*/
void case_free(struct ast_node *node)
{
    struct node_case *node_case = cast_void(node);

    node_case->node_case_clause->base.free(&node_case->node_case_clause->base);
    free(node_case->word);
    free(node_case);
}

/**
** Printing function for node of type case.
**
** \param node the `ast_node` structure to print.
**
** \param file the `FILE` where to print the ast.
*/
void *case_print(struct ast_node *node, FILE *file)
{
    void *ptr = cast_void(node);
    struct node_case *n = ptr;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", ptr, node->name);

    char *word = n->word;
    fprintf(file, "    \"%p\" [label=\"%s\"];\n", cast_void(word), word);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(word));

    struct node_case_clause *case_clause = n->node_case_clause;
    case_clause->base.print(&case_clause->base, file);
    fprintf(file, "    \"%p\" -> \"%p\";\n", ptr, cast_void(case_clause));

    return ptr;
}

/**
** Initialize a node of type case.
**
** \return a node_case structure.
*/
struct node_case *case_init(void)
{
    struct node_case *node_case = calloc(1, sizeof(struct node_case));

    node_case->base.free = case_free;
    node_case->base.exec = exec_case;
    node_case->base.print = case_print;

    node_case->base.name = "rule_case";
    return node_case;
}

/**
** Pop a token from the lexer and check it is a `TOK_WORD`.
**
** \param lexer the lexer to use.
** \param error the error indicator.
**
** \return the popped token.
*/
struct token *case_get_word(struct lexer *lexer, int *error)
{
    token_free(lexer_pop(lexer));
    struct token *token = lexer_peek(lexer, error);
    if (token->type != TOK_WORD)
    {
        *error = ERROR_CODE;
        return NULL;
    }
    return token;
}

int handle_case_esac(struct lexer *lexer, struct node_case *node, int *error)
{
    struct token *token = lexer_peek(lexer, error);
    if (token->type != TOK_ESAC)
    {
        node->base.free(&node->base);
        *error = ERROR_CODE;
        return 0;
    }
    return 1;
}

/* Cf. documentation in `parser.h`. */
struct node_case *case_create(struct lexer *lexer, int *error)
{
    struct token *token = lexer_peek(lexer, error);

    if (token->type != TOK_CASE)
        return NULL;

    if ((token = case_get_word(lexer, error)) == NULL)
        return NULL;

    struct node_case *node_case = case_init();

    node_case->word = strdup(token->value);
    token_free(lexer_pop(lexer));
    consume(lexer, error);
    token = lexer_peek(lexer, error);
    if (token->type != TOK_IN)
    {
        node_case->base.free(&node_case->base);
        *error = ERROR_CODE;
        return NULL;
    }

    lexer_pop(lexer);
    consume(lexer, error);
    token = lexer_peek(lexer, error);

    node_case->node_case_clause = case_clause_create(lexer, error);
    if (*error)
    {
        node_case->base.free(&node_case->base);
        return NULL;
    }

    if (!handle_case_esac(lexer, node_case, error))
        return NULL;

    return node_case;
}
