/**
** \file src/exec/expand_command.c
**
** \brief Expand commands functions.
**
** \author 42Schmittos
*/
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "exec.h"
#include "shell.h"
#include "utils.h"

#define BUFFER_MAX_SIZE 512

struct lexer *prepare_from_command_string(char *command);
int execute(struct lexer *lexer);
struct lexer;

char *xread(int fd)
{
    char *line = calloc(1, 1);
    char buffer[BUFFER_MAX_SIZE];
    ssize_t result = 1;
    while (result != 0)
    {
        result = read(fd, buffer, BUFFER_MAX_SIZE - 1);
        if (result == -1)
            err(ERROR_CODE, "failed to read");
        buffer[result] = '\0';
        line = realloc(line, strlen(line) + result + 1);
        line = strcat(line, buffer);
    }
    return line;
}

/* Cf. documentation in `exec.h`. */
char *expand_command(char *command)
{
    if (strlen(command) == 0)
        return NULL;
    struct lexer *lexer = prepare_from_command_string(command);
    int fds[2];

    if (pipe(fds) == -1)
        err(ERROR_CODE, "failed to pipe");

    pid_t pid = fork();

    if (pid == -1)
        err(ERROR_CODE, "failed to fork");
    else if (pid == 0)
    {
        close(fds[STDIN_FILENO]);
        dup2(fds[STDOUT_FILENO], STDOUT_FILENO);
        exit(execute(lexer));
    }

    close(fds[STDOUT_FILENO]);

    char *line = xread(fds[STDIN_FILENO]);

    wait_for_child(pid);
    close(fds[STDIN_FILENO]);

    return line;
}
