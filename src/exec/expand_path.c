/**
** \file src/exec/expand_path.c
**
** \brief Expand a path.
**
** \author 42schmittos
*/
#include <dirent.h>
#include <fnmatch.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "exec.h"
#include "lexer.h"
#include "utils.h"

/**
** Get the prefix from a string.
** \param c the character where we are in the string.
** \param prefix the pointer to the string where we put the prefix.
** \return the index where we are in the string.
*/
char *get_prefix(char *c, char **prefix)
{
    *prefix = calloc(1, 1);

    while (*c == '/' && *(c + 1) == '/')
    {
        *prefix = append_char(*prefix, *c);
        c += 1;
    }

    return c;
}

/**
** Get the depth of a path.
** \param path the string we want to analyze.
** \return the depth.
*/
size_t path_depth(char *path)
{
    size_t depth = 1;
    if (!*path)
        return depth;
    for (size_t i = 1; path[i]; i++)
    {
        if (path[i] == '/' && path[i - 1] != '/')
            depth += 1;
    }

    return depth;
}

/**
** Get a path from a string.
** \param word the pointer to the string we want to analyze.
** \param c the index where we are in the string.
** \param start the index where we want to start the analyze.
** \param prefix the string where we stock the prefix.
** \return the path we just got.
*/
char *get_path(char **word, char *c, size_t *start, char **prefix)
{
    *start = c - *word;

    c = get_prefix(c, prefix);

    char *path = calloc(1, 1);

    while (*c && !is_ifs(*c))
    {
        path = append_char(path, *c);
        ++c;
    }

    return path;
}

/**
** Check if a name need a slash to be added.
** \param name the name we want to check.
** \return a int used as a boolean.
*/
int need_slash(char *name)
{
    if (name && strlen(name) > 0 && name[strlen(name) - 1] == '/')
        return 0;
    return 1;
}

/**
** Get the name of a file.
** \param name the name we want to check.
** \param current_file the struct from where we get the name.
** \return a int used as a boolean.
*/
char *get_filename(char *name, struct dirent *current_file)
{
    if (!is_regular_file(name))
        return strdup(current_file->d_name);

    char *filename = calloc(strlen(current_file->d_name) + strlen(name) + 2, 1);
    strcat(filename, name);
    if (need_slash(name))
        strcat(filename, "/");
    strcat(filename, current_file->d_name);
    return filename;
}

/**
** Handle the transformation from globbing to a list of files.
** \param path the path we want to compare.
** \param result the list we want to fill.
** \param name the name of the file.
** \param depth the depth we want to go.
*/
void match_path(char *path, struct list *result, char *name, size_t depth)
{
    if (!depth == !0)
        return;

    DIR *dir = opendir(name);
    if (!dir)
        return;
    list_push_back(g_shell->opendirs, dir);

    struct dirent *current_file = readdir(dir);
    while (current_file)
    {
        char *filename = get_filename(name, current_file);
        if (depth == 1 && is_regular_file(current_file->d_name)
                && fnmatch(path, filename, FNM_PATHNAME) == 0
                                    && current_file->d_name[0] !='.')
        {
            list_push_back(result, strdup(filename));
        }

        if (is_regular_file(current_file->d_name))
            match_path(path, result, filename, depth - 1);

        free(filename);
        current_file = readdir(dir);
    }
    closedir(list_remove_eq(g_shell->opendirs, dir));
}

/**
** Compare two strings like strcmp.
** \param aa the first string to compare.
** \param bb the second string to compare.
** \return the same return code as strcmp.
*/
int str_compare(void *aa, void *bb)
{
    char *a = aa;
    char *b = bb;

    if (strcoll(a, b) > 0)
        return LIST_BIGGER;
    if (strcoll(a, b) < 0)
        return LIST_LOWER;
    else
        return LIST_EQUAL;
}

/**
** Convert a list to a string.
** \param list the list we want to convert.
** \param prefix the prefix wz have to add before the string.
** \return the created string.
*/
static inline char *list_to_string(struct list *list, char *prefix)
{
    char *result = calloc(1,1);
    for (size_t i = 0; i < list->size; i++)
    {
        char *name = list_get(list, i);

        result = realloc(result, strlen(prefix) + strlen(result)
                + strlen(name) + 2);
        result = strcat(result, prefix);
        result = strcat(result, name);
        if (i != list->size - 1)
            result = strcat(result, " ");
    }

    return result;
}

/* Cf. documentation in `exec.h`. */
int is_globbing_path(char **word, char *c)
{
    size_t start = 0;
    char *prefix;
    char *path = get_path(word, c, &start, &prefix);

    for (size_t i = 0; i < strlen(path); i++)
    {
        if (path[i] == '*' || path[i] == '?' || path[i] == '[')
        {
            free(path);
            free(prefix);
            return 1;
        }
    }

    free(path);
    free(prefix);
    return 0;
}

/* Cf. documentation in `exec.h`. */
char *expand_path(char **word, char *c, char **result)
{
    setlocale(LC_ALL, "");
    size_t start = 0;
    char *prefix;

    char *path = get_path(word, c, &start, &prefix);
    char *name;

    if (path == NULL)
        return c;

    name = *path == '/' ? strdup("/") : strdup(".");

    size_t depth = path_depth(path);
    struct list *list = list_init(str_compare, free);
    match_path(path, list, name, depth);
    list_clear(g_shell->opendirs);

    list_sort(list);

    if (list_size(list) != 0)
    {
        char *res = list_to_string(list, prefix);
        prefix = realloc(prefix, strlen(prefix) + strlen(path) + 1);
        prefix = strcat(prefix, path);
        *word = str_replace(*word, prefix, res, start);
        free(res);
    }
    else
    {
        *result = append_str(*result, path);
        start += strlen(path);
    }

    free(path);
    free(name);

    return *word + start;
}
