/**
** \file src/exec/exec.h
**
** \brief Header file for the execution functions.
**
** \author 42Schmittos
*/

#ifndef EXEC_H
#define EXEC_H

#include "list.h"
#include "parser.h"

/** Represent a context structure */
struct context
{
    /** Represent a list of assignments variables */
    struct list *variables;
};

/**
** Function to handle a EOL node from an AST.
**
** \param node the AST to handle.
**
** \return 0.
*/
int exec_eol(struct ast_node *node);

/**
** Function to execute a simple command, represented as an ast.
**
** \param node : ast to execute.
**
** \return the return code of the command executed.
*/
int exec_simple_command(struct ast_node *node);

/**
** Function to execute a command, represented as a list of char *.
**
** \param argc the number of arguments.
** \param argv the arguments.
** \param envp the environment to pass to the process.
** \param fd_links the list of the redirections.
**
** \return the return code of the command executed.
*/
int exec_words(int argc, char *argv[], char *envp[], struct list *fd_links);

/**
** Exec an assignment and set the variable name to the environment.
**
** \param simple_command the 'node_simple_command' structure.
**
** \return true on success.
*/
char **assignment_set(struct node_simple_command *simple_command);

/**
** Exec a command.
**
** \param node the 'ast_node' structure.
**
** \return 0 on succes.
*/
int exec_command(struct ast_node *node);

/**
** Exec shell_command.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/
int exec_shell_command(struct ast_node *node);

/**
** Exec if.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/
int exec_if(struct ast_node *node);

/**
** Exec else_clause.
**
** \param node the 'ast_node' structure.
**
** \return 0 on succes.
*/
int exec_else_clause(struct ast_node *node);

/**
** Exec while.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/
int exec_while(struct ast_node *node);

/**
** Exec until.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/
int exec_until(struct ast_node *node);

/**
** Exec for.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/
int exec_for(struct ast_node *node);

/**
** Exec a compound_list.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/
int exec_compound_list(struct ast_node *node);

/**
** Exec pipeline.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/
int exec_pipeline(struct ast_node *node);

/**
** Exec redirection.
**
** \param redirections a list of redirections.
** \param nb_redirections the size of the previous list.
**
** \return 0 on success.
*/
struct list *exec_redirection(struct node_redirection **redirections,
       size_t nb_redirections);

/**
** Function to execute from an AST.
**
** \param node the AST to execute.
**
** \return the return code of the command executed.
*/
int exec(struct ast_node *node);

/**
** Function to execute the content of the list of arguments.
**
** \param argv the list to execute.
** \param envp the environment to pass to the process.
*/
void child_process(char *argv[], char *envp[]);

/**
** Function to wait for the child who is executed.
** \param child_pid the pid of the child to wait for.
** \return a int.
*/
int wait_for_child(pid_t child_pid);

/**
** Exec and_or.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/
int exec_and_or(struct ast_node *node);

/**
** Exec list.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/

int exec_list(struct ast_node *node);

/**
** Exec case_item.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/

int exec_case_item(struct ast_node *node);

/**
** Exec case.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/
int exec_case(struct ast_node *node);

/**
** Exec function.
**
** \param node the 'ast_node' structure.
**
** \return 0 on success.
*/
int exec_function(struct ast_node *node);

/**
** Handle the expansion of a list of words.
**
** \param words the list of words.
*/
int expand_words(struct list *words);

/**
** Replace the old string by the new one.
**
** \param str the actual string.
** \param old the old string.
** \param new the new string which holds the new value of the string
** \param word_index an index.
**
** \return the `new` string.
*/
char *str_replace(char *str, char *old, char *new, size_t word_index);

/**
** Handle the expansion recursively.
**
** \param word the list of words.
** \param c a pointer on the current character.
** \param result the list of expanded words.
** \param quoting_char the current char.
**
** \return the pointer on the current character.
*/
char *expand_rec(char **word, char *c, char **result, char quoting_char);

/**
** Handle the expansion for a command.
**
** \param command the current command.
**
** \return the string after command substitution.
*/
char *expand_command(char *command);

/**
** Handle the expansion for an assignment word.
**
** \param assignments the list of assignments.
** \param len the number of assignments.
**
** \return the string after command substitution.
*/
void expand_assignments(struct node_assignment **assignments, size_t len);

/**
** Expand the current delimited path.
**
** \param word the word containing the path.
** \param c the pointer on the character at the begining of the path.
** \param result the current string resulting of the expansion.
**
** \return the pointer on the expanded path to be lexed.
*/
char *expand_path(char **word, char *c, char **result);

/**
** Check if the current word is a path waiting to be expanded
**
** \param word the current word
** \param c the pointer on the current character
**
** \return 1 if the word contain a path, else 0
*/
int is_globbing_path(char **word, char *c);

/**
** Remove the newlines at the end of the substituted command
**
** \param command the substituted command
*/
void remove_end_newlines(char *command);

/**
** Check if the string represent a regular filename
**
** \param s the filename to check
**
** \return 1 if the file is a regular filename, else 0
*/
int is_regular_file(char *s);

/** A link that has been made between fds for a redirection. */
struct fd_link
{
    /** The fd that has been replaced. */
    int process_fd;
    /** The fd replacing it. */
    int file_fd;
    /** The fd that has been saved to be later restored. */
    int save_process_fd;
};

/**
** Handle backslashes in command substitution
**
** \param result the current result of the expansion
** \param c a pointer on the current character
** \param quoting_char the current character of quoting_char
**
** \return the updated pointer c
*/
char *handle_backslash(char **result, char *c, char quoting_char);

/**
** Expand a word into a string.
**
** \param word the word to expand.
**
** \return the string resulting of the expansion.
*/
char *expand_word_to_str(char *word);

#endif /* ! EXEC_H */
