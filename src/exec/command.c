/**
** \file src/exec/command.c
**
** \brief Exec a command.
**
** \author 42Schmittos
*/

#include "parser.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_command(struct ast_node *node)
{
    if (g_shell->is_continue > 0)
    {
        return 0;
    }
    if (g_shell->is_break > 0)
        return 0;
    int rc = 0;
    struct node_command *command = cast_void(node);

    if (command->type == SIMPLE_COMMAND)
    {
        struct node_simple_command *child = cast_void(command->child);
        rc = child->base.exec(&child->base);
    }
    else if (command->type == SHELL_COMMAND)
    {
        struct node_shell_command *child = cast_void(command->child);
        rc = child->base.exec(&child->base);
    }
    return rc;
}
