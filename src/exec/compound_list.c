/**
** \file src/exec/compound_list.c
**
** \brief Exec a compound_list.
**
** \author 42Schmittos
*/

#include "parser.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_compound_list(struct ast_node *node)
{
    if (g_shell->is_continue > 0)
    {
        return 0;
    }
    if (g_shell->is_break > 0)
        return 0;
    int rc = 0;
    struct node_compound_list *compound_list = cast_void(node);

    for (size_t i = 0; i < compound_list->nb_and_or; i++)
    {
        struct node_and_or *and_or = compound_list->and_ors[i];
        rc = and_or->base.exec(&and_or->base);
        if (rc != 0)
            return rc;
    }

    return rc;
}
