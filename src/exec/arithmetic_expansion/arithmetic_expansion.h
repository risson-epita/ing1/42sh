/**
** \file src/exec/arithmetic_expansion/arithmetic_expansion.h
**
** \brief Header file for the arithmetic expansion.
**
** \author 42Schmittos
*/

#ifndef ARITHMETIC_EXPANSION_H
#define ARITHMETIC_EXPANSION_H

/** Type used for arithmetic expansion */
enum a_type
{
    /** token none */
    A_NONE,
    /** token `-` */
    A_MINUS,
    /** token `+` */
    A_PLUS,
    /** token `*` */
    A_MULT,
    /** token `/` */
    A_DIV,
    /** token `**` */
    A_POW,
    /** token `^` */
    A_XOR,
    /** token `&` */
    A_AMPERSAND,
    /** token `||` */
    A_OR,
    /** token `&&` */
    A_AND,
    /** token `!` */
    A_NOT,
    /** token `~` */
    A_TILDE,
    /** token `(` */
    A_RPAREN,
    /** token `)` */
    A_LPAREN,
    /** token `|` */
    A_PIPE,
    /** token number */
    A_NUMBER
};

/** Struct token for arithmetic expansion */
struct a_token
{
    /** type used for arithmetic expansion */
    enum a_type type;
    /** value of each token */
    char *value;
};

/** Struct lexer for arithmetic expansion */
struct a_lexer
{
    /** token_list for arithmetic expansion */
    struct list *token_list;
    /** value of each token in token_list */
    char *value;
};

/** Struct ast for arithmetic expansion */
struct ast
{
    /** typed used for arithmetic expansion */
    enum a_type type;
    /** union between struct children and value */
    union {
        /** to evaluate the AST */
        struct {
            /** left child */
            struct ast *left;
            /** right child */
            struct ast *right;
        } children;
        /** data */
        long value;
    } data;
};

/** Struct function for arithmetic expansion */
struct arithmetic_function
{
    /** type for arithmetic_function */
    enum a_type type;
    /** a pointer on an execution function */
    long (*fun)(struct ast *, struct ast *);
};

/**
** Function to set a token type.
**
** \param token the token to check.
*/
void a_token_set_type(struct a_token *token);

/**
** Function to free a token.
**
** \param p the token to free.
*/
void a_token_free(void *p);

/**
** Function to free an arithmetic lexer..
**
** \param lexer the lexer to check.
*/
void a_lexer_free(struct a_lexer *lexer);

/**
** Function to begin the lexing..
**
** \param value the value to check.
**
** \return a a_lexer structure.
*/
struct a_lexer *a_lexer_start(char *value);

/**
** Function to peek a character.
**
** \param lexer the a_lexer structure to check.
**
** \return a a_token structure.
*/
struct a_token *a_lexer_peek(struct a_lexer *lexer);

/**
** Function to consume a character..
**
** \param lexer the a_lexer structure to check.
**
** \return a a_token structure.
*/
struct a_token *a_lexer_pop(struct a_lexer *lexer);

/**
** Function to create an AST.
**
** \return an ast structure.
*/
struct ast *ast_create(void);

/**
** Function to parse an expression.
**
** \param lexer the a_lexer structure to check.
** \param ast the structure to check.
** \param error an integer which set an error.
**
** \return 0 on success.
*/
int parse_expression(struct a_lexer *lexer, struct ast **ast, int *error);

/**
** Evaluate the AST.
**
** \param ast the ast_structure to check.
**
** \return 0 on success.
*/
long evaluate(struct ast *ast);

/**
** Function to expand an arithmetic expansion.
**
** \param word the list of words to check.
** \param c the pointer on the current character.
**
** \return the result string expanded.
*/
char *expand_arithmetic(char **word, char *c);

/**
** Function to get a variable.
**
** \param value the value to check.
**
** \return a variable structure.
*/
struct variable *get_variable_rec(char *value);

/**
** Function that check if a token is unary.
**
** \param token the token to check.
**
** \return 1 if yes 0 otherwise
*/
static inline int token_is_unary(struct a_token *token)
{
    return token->type == A_NOT || token->type == A_TILDE;
}

/**
** Function who check if a token is power.
**
** \param token the token to check.
**
** \return 1 if yes 0 otherwise
*/
static inline int token_is_pow(struct a_token *token)
{
    return token->type == A_POW;
}

/**
** Function who check if a token is S op.
**
** \param token the token to check.
**
** \return 1 if yes 0 otherwise
*/
static inline int token_is_sop(struct a_token *token)
{
    return token->type == A_MULT || token->type == A_DIV;
}

/**
** Function who check if a token is E op.
**
** \param token the token to check.
**
** \return 1 if yes 0 otherwise
*/
static inline int token_is_eop(struct a_token *token)
{
    return token->type == A_PLUS || token->type == A_MINUS;
}

/**
** Function who check if a token is binary and.
**
** \param token the token to check.
**
** \return 1 if yes 0 otherwise
*/
static inline int token_is_biand(struct a_token *token)
{
    return token->type == A_AMPERSAND;
}

/**
** Function who check if a token is binary xor.
**
** \param token the token to check.
**
** \return 1 if yes 0 otherwise
*/
static inline int token_is_bi_xor(struct a_token *token)
{
    return token->type == A_XOR;
}

/**
** Function who check if a token is binary or.
**
** \param token the token to check.
**
** \return 1 if yes 0 otherwise
*/
static inline int token_is_bior(struct a_token *token)
{
    return token->type == A_PIPE;
}

/**
** Function who check if a token is and.
**
** \param token the token to check.
**
** \return 1 if yes 0 otherwise
*/
static inline int token_is_and(struct a_token *token)
{
    return token->type == A_AND;
}

/**
** Function who check if a token is or.
**
** \param token the token to check.
**
** \return 1 if yes 0 otherwise
*/
static inline int token_is_or(struct a_token *token)
{
    return token->type == A_OR;
}

/**
** Function to evaluate a not operator.
**
** \param lhs the left child of the AST to evaluate.
** \param rhs the right child of the AST to evaluate.
**
** \return the result of the operation.
*/
long fun_not(struct ast *lhs, struct ast *rhs);

/**
** Function to evaluate a binary not operator.
**
** \param lhs the left child of the AST to evaluate.
** \param rhs the right child of the AST to evaluate.
**
** \return the result of the operation.
*/
long fun_bnot(struct ast *lhs, struct ast *rhs);

/**
** Function to evaluate a power operator.
**
** \param lhs the first AST to evaluate.
**
** \param rhs the second AST to evaluate.
**
** \return the result of the operation.
*/
long fun_power(struct ast *lhs, struct ast *rhs);

/**
** Function to evaluate a addition operator.
**
** \param lhs the first ast to evaluate.
**
** \param rhs the second AST to evaluate.
**
** \return the result of the operation.
*/
long fun_addition(struct ast *lhs, struct ast *rhs);

/**
** Function to evaluate a substraction operator.
**
** \param lhs the first AST to evaluate.
**
** \param rhs the second AST to evaluate.
**
** \return the result of the operation.
*/
long fun_substraction(struct ast *lhs, struct ast *rhs);

/**
** Free the arithmetic expansion ast.
**
** \param ast the ast to free.
*/
void ast_free(struct ast *ast);

/**
** A function that create an ast.
**
** \return the created ast.
*/
struct ast *ast_create(void);

/**
** Function who add a string in an ast.
**
** \param value the string we want to put in the ast.
**
** \return the created ast.
*/
struct ast *ast_alloc_number(char *value);

/**
** Parsing function for parenthesis.
**
** \param lexer the lexer we use.
** \param ast the sat created while parsing.
** \param error the error tracker.
**
** \return 1 if we are in a `)` and 0 otherwise.
*/
int parse_parenthesis(struct a_lexer *lexer, struct ast **ast, int *error);

/**
** Parsing function for basic expressions.
**
** \param lexer the lexer we use.
** \param ast the sat created while parsing.
** \param error the error tracker.
**
** \return the result of the operation.
*/
int parse_exp(struct a_lexer *lexer, struct ast **ast, int *error);

#endif /* ! ARITHMETIC_EXPANSION_H */
