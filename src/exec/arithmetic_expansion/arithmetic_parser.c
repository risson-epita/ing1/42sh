/**
** \file src/exec/arithmetic_expansion/arithmetic_parser.c
**
** \brief Entry for the arithmetic parser.
**
** \author 42Schmittos
*/
#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "arithmetic_expansion.h"
#include "list.h"
#include "utils.h"

static int parse_or(struct a_lexer *lexer, struct ast **ast, int *error);

/**
** Parsing function for t expression.
**
** \param lexer the lexer we use.
** \param ast the sat created while parsing.
** \param error the error tracker.
**
** \return the result of the operation.
*/
static int parse_texp(struct a_lexer *lexer, struct ast **ast, int *error)
{
    int correct_syntax = 0;
    if (*error)
        return 0;

    struct a_token *token = a_lexer_peek(lexer);

    if (!token)
        return 0;

    int res = 1;

    if (token->type == A_LPAREN)
    {
        a_lexer_pop(lexer);
        return parse_parenthesis(lexer, ast, error);
    }

    while (token && (token->type == A_MINUS || token->type == A_PLUS
                || token->type == A_TILDE || token->type == A_NOT))
    {
        correct_syntax = 1;
        token = a_lexer_pop(lexer);
        struct ast *tmp = ast_create();
        tmp->type = token->type;

        res = parse_texp(lexer, &tmp->data.children.right, error);
        tmp->data.children.left = *ast;
        *ast = tmp;

        token = a_lexer_peek(lexer);
    }

    if (token && token->type == A_NUMBER)
    {
        *ast = ast_alloc_number(a_lexer_pop(lexer)->value);
    }
    else if (!correct_syntax)
    {
        *error = 1;
    }

    return res;
}

/**
** Parsing function for power expressions.
**
** \param lexer the lexer we use.
** \param ast the sat created while parsing.
** \param error the error tracker.
**
** \return the result of the operation.
*/
static int parse_power(struct a_lexer *lexer, struct ast **ast, int *error)
{

    if (*error)
        return 0;

    if (!parse_texp(lexer, ast, error))
        return 0;

    int res = 1;
    struct a_token *token = a_lexer_peek(lexer);
    while (res && token && token_is_pow(token))
    {
        token = a_lexer_pop(lexer);

        struct ast *tmp = ast_create();
        tmp->type = token->type;

        res = parse_texp(lexer, &tmp->data.children.right, error);
        tmp->data.children.left = *ast;
        *ast = tmp;

        token = a_lexer_peek(lexer);
    };

    return res;
}

/**
** Parsing function for s expressions.
**
** \param lexer the lexer we use.
** \param ast the sat created while parsing.
** \param error the error tracker.
**
** \return the result of the operation.
*/
static int parse_sexp(struct a_lexer *lexer, struct ast **ast, int *error)
{

    if (*error)
        return 0;

    if (!parse_power(lexer, ast, error))
        return 0;

    int res = 1;
    struct a_token *token = a_lexer_peek(lexer);
    while (res && token && token_is_sop(token))
    {
        token = a_lexer_pop(lexer);

        struct ast *tmp = ast_create();
        tmp->type = token->type;

        res = parse_power(lexer, &tmp->data.children.right, error);
        tmp->data.children.left = *ast;
        *ast = tmp;

        token = a_lexer_peek(lexer);
    };

    return res;
}

/* Cf. documentation in `arithmetic_expansion.h`. */
int parse_exp(struct a_lexer *lexer, struct ast **ast, int *error)
{

    if (*error)
        return 0;

    if (!parse_sexp(lexer, ast, error))
        return 0;

    int res = 1;
    struct a_token *token = a_lexer_peek(lexer);
    while (res && token && token_is_eop(token))
    {
        token = a_lexer_pop(lexer);

        struct ast *tmp = ast_create();
        tmp->type = token->type;

        res = parse_sexp(lexer, &tmp->data.children.right, error);
        tmp->data.children.left = *ast;
        *ast = tmp;

        token = a_lexer_peek(lexer);
    };

    return res;
}

/**
** Parsing function for binary and expressions.
**
** \param lexer the lexer we use.
** \param ast the sat created while parsing.
** \param error the error tracker.
**
** \return the result of the operation.
*/
static int parse_biand(struct a_lexer *lexer, struct ast **ast, int *error)
{

    if (*error)
        return 0;

    if (!parse_exp(lexer, ast, error))
        return 0;

    int res = 1;
    struct a_token *token = a_lexer_peek(lexer);
    while (res && token && token_is_biand(token))
    {
        token = a_lexer_pop(lexer);

        struct ast *tmp = ast_create();
        tmp->type = token->type;

        res = parse_exp(lexer, &tmp->data.children.right, error);
        tmp->data.children.left = *ast;
        *ast = tmp;

        token = a_lexer_peek(lexer);
    };

    return res;
}

/**
** Parsing function for binary xor expressions.
**
** \param lexer the lexer we use.
** \param ast the sat created while parsing.
** \param error the error tracker.
**
** \return the result of the operation.
*/
static int parse_bixor(struct a_lexer *lexer, struct ast **ast, int *error)
{

    if (*error)
        return 0;

    if (!parse_biand(lexer, ast, error))
        return 0;

    int res = 1;
    struct a_token *token = a_lexer_peek(lexer);
    while (res && token && token_is_bi_xor(token))
    {
        token = a_lexer_pop(lexer);

        struct ast *tmp = ast_create();
        tmp->type = token->type;

        res = parse_biand(lexer, &tmp->data.children.right, error);
        tmp->data.children.left = *ast;
        *ast = tmp;

        token = a_lexer_peek(lexer);
    };

    return res;
}

/**
** Parsing function for binary or expressions.
**
** \param lexer the lexer we use.
** \param ast the sat created while parsing.
** \param error the error tracker.
**
** \return the result of the operation.
*/
static int parse_bior(struct a_lexer *lexer, struct ast **ast, int *error)
{

    if (*error)
        return 0;

    if (!parse_bixor(lexer, ast, error))
        return 0;

    int res = 1;
    struct a_token *token = a_lexer_peek(lexer);
    while (res && token && token_is_bior(token))
    {
        token = a_lexer_pop(lexer);

        struct ast *tmp = ast_create();
        tmp->type = token->type;

        res = parse_bixor(lexer, &tmp->data.children.right, error);
        tmp->data.children.left = *ast;
        *ast = tmp;

        token = a_lexer_peek(lexer);
    };

    return res;
}

/**
** Parsing function for and expressions.
**
** \param lexer the lexer we use.
** \param ast the sat created while parsing.
** \param error the error tracker.
**
** \return the result of the operation.
*/
static int parse_and(struct a_lexer *lexer, struct ast **ast, int *error)
{

    if (*error)
        return 0;

    if (!parse_bior(lexer, ast, error))
    {
        return 0;
    }

    int res = 1;
    struct a_token *token = a_lexer_peek(lexer);
    while (res && token && token_is_and(token))
    {
        token = a_lexer_pop(lexer);

        struct ast *tmp = ast_create();
        tmp->type = token->type;

        res = parse_bior(lexer, &tmp->data.children.right, error);
        tmp->data.children.left = *ast;
        *ast = tmp;

        token = a_lexer_peek(lexer);
    };

    return res;
}

/**
** Parsing function for or expressions.
**
** \param lexer the lexer we use.
** \param ast the sat created while parsing.
** \param error the error tracker.
**
** \return the result of the operation.
*/
static int parse_or(struct a_lexer *lexer, struct ast **ast, int *error)
{

    if (*error)
        return 0;

    if (!parse_and(lexer, ast, error))
    {
        return 0;
    }

    int res = 1;
    struct a_token *token = a_lexer_peek(lexer);
    while (res && token && token_is_or(token))
    {
        token = a_lexer_pop(lexer);

        struct ast *tmp = ast_create();
        tmp->type = token->type;

        res = parse_and(lexer, &tmp->data.children.right, error);
        tmp->data.children.left = *ast;
        *ast = tmp;

        token = a_lexer_peek(lexer);
    };

    return res;
}

int parse_expression(struct a_lexer *lexer, struct ast **ast, int *error)
{
    struct a_token *token = a_lexer_peek(lexer);
    if (!token || token->type == A_NONE)
        return 0;

    int rc = parse_or(lexer, ast, error);

    if (*error)
    {
        warnx("%s: syntax error: operand expected", lexer->value);
        return 0;
    }

    if (!rc)
        return 0;

    return 0;
}
