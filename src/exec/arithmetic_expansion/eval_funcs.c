/**
** \file src/exec/arithmetic_expansion/eval_funcs.c
**
** \brief Functions for the evaluation functions.
**
** \author 42Schmittos
*/

#include <math.h>
#include "arithmetic_expansion.h"

/* Cf. documentation in `arithmetic_expansion.h`. */
long evaluate(struct ast *ast);

/* Cf. documentation in `arithmetic_expansion.h`. */
long fun_not(struct ast *lhs, struct ast *rhs)
{
    if (lhs)
        return !evaluate(rhs);
    return !evaluate(rhs);
}

/* Cf. documentation in `arithmetic_expansion.h`. */
long fun_bnot(struct ast *lhs, struct ast *rhs)
{
    if (lhs)
        return ~evaluate(rhs);
    return ~evaluate(rhs);
}

/* Cf. documentation in `arithmetic_expansion.h`. */
long fun_power(struct ast *lhs, struct ast *rhs)
{
    return pow(evaluate(lhs), evaluate(rhs));
}

/* Cf. documentation in `arithmetic_expansion.h`. */
long fun_addition(struct ast *lhs, struct ast *rhs)
{
    return evaluate(lhs) + evaluate(rhs);
}

/* Cf. documentation in `arithmetic_expansion.h`. */
long fun_substraction(struct ast *lhs, struct ast *rhs)
{
    return evaluate(lhs) - evaluate(rhs);
}
