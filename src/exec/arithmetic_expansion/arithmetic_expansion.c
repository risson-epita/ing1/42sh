/**
** \file src/exec/arithmetic_expansion/arithmetic_expansion.c
**
** \brief Functions for the expansion functions.
**
** \author 42Schmittos
*/

#include <ctype.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "arithmetic_expansion.h"
#include "list.h"
#include "utils.h"
#include "exec.h"

/**
** Function who get the start of the expression.
**
** \param c the pointer to the start of the string.
**
** \return a pointer to the start of the expression.
*/
char *get_arithmetic_expression(char *c)
{
    char *value = calloc(1, 1);
    int nested = 1;
    while (*c && nested)
    {
        if (*c == '(')
            nested += 1;
        else if (*c == ')')
            nested -= 1;
        if (nested == 0)
            break;
        value = append_char(value, *c);
        c += 1;
    }

    return value;
}

/* Cf. documentation in `arithmetic_expansion.h`. */
struct variable *get_variable_rec(char *value)
{
    int error = 0;
    struct variable *var = NULL;
    struct variable *next = NULL;
    char *original_value = value;

    while ((next = variable_get(value)))
    {
        var = next;
        if (strcmp(var->value, original_value) == 0)
        {
            error = 1;
            break;
        }
        value = var->value;
    }

    if (error)
    {
        g_shell->arithmetic_error = error;
        warnx("%s: expression recursion level exceeded", original_value);
    }

    return var;
}

char *expand_arithmetic(char **word, char *c)
{
    int error = 0;
    size_t start = c - *word;
    char *arithmetic_expression = get_arithmetic_expression(c);
    char *saved_expression = strdup(arithmetic_expression);

    char *expanded = expand_word_to_str(arithmetic_expression);

    struct a_lexer *lexer = a_lexer_start(expanded);
    struct ast *ast = ast_create();
    parse_expression(lexer, &ast, &error);

    if (error)
        g_shell->arithmetic_error = error;

    long result = evaluate(ast);
    char *str_result = itoa(result);

    ast_free(ast);

    saved_expression = append_char(saved_expression, ')');
    saved_expression = append_char(saved_expression, ')');

    *word = str_replace(*word, saved_expression, str_result, start);

    free(expanded);
    return *word + start;
}
