/**
** \file src/exec/arithmetic_expansion/arithmetic_evaluation.c
**
** \brief Functions for the evaluation functions.
**
** \author 42Schmittos
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <math.h>

#include "arithmetic_expansion.h"
#include "list.h"
#include "utils.h"

#define NB_FUNS sizeof(funs) / sizeof(funs[0])

long evaluate(struct ast *ast);

/**
** Function for the multiplication operator.
**
** \param lhs the left member of the operation.
**
** \param rhs the right member of the operation.
**
** \return the result of the operation
*/
long fun_multiplication(struct ast *lhs, struct ast *rhs)
{
    return evaluate(lhs) * evaluate(rhs);
}

/**
** Function for the division operator.
**
** \param lhs the left member of the operation.
**
** \param rhs the right member of the operation.
**
** \return the result of the operation
*/
long fun_division(struct ast *lhs, struct ast *rhs)
{
    long right = evaluate(rhs);
    if (right == 0)
    {
        warnx("division by 0");
        return 0;
    }
    return evaluate(lhs) / evaluate(rhs);
}

/**
** Function for the binary and operator.
**
** \param lhs the left member of the operation.
**
** \param rhs the right member of the operation.
**
** \return the result of the operation
*/
long fun_biand(struct ast *lhs, struct ast *rhs)
{
    return evaluate(lhs) & evaluate(rhs);
}

/**
** Function for the binary xor operator.
**
** \param lhs the left member of the operation.
**
** \param rhs the right member of the operation.
**
** \return the result of the operation
*/
long fun_bixor(struct ast *lhs, struct ast *rhs)
{
    return evaluate(lhs) ^ evaluate(rhs);
}

/**
** Function for the binary or operator.
**
** \param lhs the left member of the operation.
**
** \param rhs the right member of the operation.
**
** \return the result of the operation
*/
long fun_bior(struct ast *lhs, struct ast *rhs)
{
    return evaluate(lhs) | evaluate(rhs);
}

/**
** Function for the and operator.
**
** \param lhs the left member of the operation.
**
** \param rhs the right member of the operation.
**
** \return the result of the operation
*/
long fun_and(struct ast *lhs, struct ast *rhs)
{
    return evaluate(lhs) && evaluate(rhs);
}

/**
** Function for the or operator.
**
** \param lhs the left member of the operation.
**
** \param rhs the right member of the operation.
**
** \return the result of the operation
*/
long fun_or(struct ast *lhs, struct ast *rhs)
{
    return evaluate(lhs) || evaluate(rhs);
}

/** Table of all the operations functions */
struct arithmetic_function funs[] =
{
    { A_PLUS, fun_addition },
    { A_MINUS, fun_substraction },
    { A_MULT, fun_multiplication },
    { A_DIV, fun_division },
    { A_AND, fun_and },
    { A_OR, fun_or },
    { A_XOR, fun_bixor },
    { A_POW, fun_power },
    { A_PIPE, fun_bior },
    { A_AMPERSAND, fun_biand },
    { A_TILDE, fun_bnot },
    { A_NOT, fun_not }
};

long evaluate(struct ast *ast)
{
    if (!ast)
    {
        return 0;
    }
    else
    {
        if (ast->type == A_NUMBER)
            return ast->data.value;

        for (unsigned long i = 0; i < NB_FUNS; i++)
        {
            if (funs[i].type == ast->type)
            {
                struct ast *rhs = ast->data.children.right;
                struct ast *lhs = ast->data.children.left;

                return funs[i].fun(lhs, rhs);
            }
        }
    }
    return 0;
}
