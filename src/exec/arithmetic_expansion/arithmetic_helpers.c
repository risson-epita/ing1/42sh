/**
** \file src/exec/arithmetic_expansion/arithmetic_helpers.c
**
** \brief Helpers for the arithmetic parser.
**
** \author 42Schmittos
*/
#include <stdlib.h>
#include <string.h>

#include "arithmetic_expansion.h"

struct ast *ast_create(void)
{
    return calloc(1, sizeof(struct ast));
}

void ast_free(struct ast *ast)
{
    if (ast && ast->type != A_NUMBER)
    {
        ast_free(ast->data.children.right);
        ast_free(ast->data.children.left);
    }

    free(ast);
}

struct ast *ast_alloc_number(char *value)
{
    long int_value = atol(value);
    struct ast *res = calloc(1, sizeof(*res));
    res->type = A_NUMBER;
    res->data.value = int_value;
    return res;
}

int parse_parenthesis(struct a_lexer *lexer, struct ast **ast, int *error)
{

    if (*error)
        return 0;

    int res = parse_exp(lexer, ast, error);

    struct a_token *token = a_lexer_pop(lexer);
    res = res && token && token->type == A_RPAREN;

    a_token_free(token);
    return res;
}
