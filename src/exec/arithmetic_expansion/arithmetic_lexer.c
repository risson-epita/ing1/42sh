/**
** \file src/exec/arithmetic_expansion/arithmetic_lexer.c
**
** \brief Header file for the arithmetic lexing.
**
** \author 42Schmittos
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "arithmetic_expansion.h"
#include "list.h"
#include "utils.h"
#include "shell.h"

#define NB_TOKENS sizeof(types) / sizeof(types[0])

/** a list of all the tokens */
struct a_token types[] =
{
    //TODO
    { A_NONE, "" },
    //TODO
    { A_MINUS, "-"},
    //TODO
    { A_PLUS, "+"},
    //TODO
    { A_MULT, "*"},
    //TODO
    { A_DIV, "/"},
    //TODO
    { A_POW, "**"},
    //TODO
    { A_XOR, "^"},
    //TODO
    { A_AMPERSAND, "&"},
    //TODO
    { A_OR, "||"},
    //TODO
    { A_AND, "&&"},
    //TODO
    { A_NOT, "!"},
    //TODO
    { A_TILDE, "~"},
    //TODO
    { A_LPAREN, "("},
    //TODO
    { A_RPAREN, ")"},
    //TODO
    { A_PIPE, "|"}
};

/**
** Function who get the type of a token.
**
** \param value the string we want to know the type of.
**
** \return the token type
*/
enum a_type a_token_get_type(char *value)
{
    for (long unsigned int i = 0; i < NB_TOKENS; i++)
    {
        if (strcmp(value, types[i].value) == 0)
        {
            return types[i].type;
        }
    }

    return A_NUMBER;
}

void a_token_set_type(struct a_token *token)
{
    token->type = a_token_get_type(token->value);
    if (token->type == A_NUMBER)
    {
        for (size_t i = 0; token->value[i]; i++)
        {
            if (!isdigit(token->value[i]))
            {
                struct variable *var = get_variable_rec(token->value);
                if (var)
                {
                    free(token->value);
                    token->value = var->value;
                }
                break;
            }
        }
    }
}

void a_token_free(void *p)
{
    struct a_token *t = p;
    if (t && t->value)
        free(t->value);
    free(t);
}

/**
** Function who compare two token.
**
** \param lhs the first token to compare.
**
** \param rhs the second token to compare.
**
** \return the result of the comparison
*/
int a_token_cmp(void *lhs, void *rhs)
{
    struct a_token *left = lhs;
    struct a_token *right = rhs;

    if (left->type < right->type)
        return LIST_LOWER;
    if (left->type > right->type)
        return LIST_BIGGER;
    return  LIST_EQUAL;
}

/**
** Function who create a lexer from a string.
**
** \param value the string from who we want to create the lexer.
**
** \return the lexer.
*/
struct a_lexer *a_lexer_create(char *value)
{
    struct a_lexer *lexer = calloc(1, sizeof(struct a_lexer));
    lexer->value = value;

    lexer->token_list = list_init(a_token_cmp, a_token_free);

    return lexer;
}

void a_lexer_free(struct a_lexer *lexer)
{
    list_destroy(lexer->token_list);
    free(lexer->value);
    free(lexer);
}

/**
** Function who check if a character or two are a token.
**
** \param value the string from who we will check the character.
**
** \param c the character we want to check.
**
** \return 1 if yes 0 otherwise.
*/
int a_is_operator(char *value, char c)
{
    char *to_check;
    if (!value)
        to_check = calloc(1,1);
    else
        to_check = strdup(value);
    to_check = append_char(to_check, c);

    enum a_type to_check_type = a_token_get_type(to_check);

    free(to_check);
    return to_check_type != A_NUMBER && to_check_type != A_NONE;
}

/**
** Main function of the lexer.
**
** \param lexer the lexer we use.
**
** \param token the token we are on in the lexer.
**
** \param c the character where we are in the string.
**
** \return the result of the expansion.
*/
char *a_lexer_rec(struct a_lexer *lexer, struct a_token *token, char *c)
{
    if (*c && a_is_operator(token->value, *c))
    {
        token->value = append_char(token->value, *c);
        return a_lexer_rec(lexer, token, c + 1);
    }

    if (*c && a_is_operator(token->value, 0))
        return c;

    if (*c && a_is_operator(NULL, *c))
    {
        if (token->type != A_NONE)
            return c;
        token->value = append_char(token->value, *c);
        return a_lexer_rec(lexer, token, c + 1);
    }

    if (*c == '\0')
        return c;

    if (isblank(*c))
    {
        if (token->type != A_NONE)
            return c;

        while (isblank(*c))
            c += 1;
        return a_lexer_rec(lexer, token, c);
    }

    if (token->type == A_NUMBER)
    {
        token->value = append_char(token->value, *c);
        return a_lexer_rec(lexer, token, c + 1);
    }

    token->type = A_NUMBER;
    return a_lexer_rec(lexer, token, c);
}

struct a_lexer *a_lexer_start(char *value)
{
    struct a_lexer *lexer = a_lexer_create(value);
    char *start_of_next_token = lexer->value;

    do {
        struct a_token *token_new = calloc(1, sizeof(struct a_token));
        token_new->value = calloc(1, 1);

        start_of_next_token = a_lexer_rec(lexer, token_new,
                start_of_next_token);

        a_token_set_type(token_new);
        list_push_back(lexer->token_list, token_new);
    } while (*start_of_next_token);

    return lexer;
}

struct a_token *a_lexer_pop(struct a_lexer *lexer)
{
    return list_remove_at(lexer->token_list, 0);
}

struct a_token *a_lexer_peek(struct a_lexer *lexer)
{
    return list_get(lexer->token_list, 0);
}
