/**
** \file src/exec/list.c
**
** \brief Exec a list.
**
** \author 42Schmittos
*/

#include "parser.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_list(struct ast_node *node)
{
    if (g_shell->is_continue > 0)
    {
        return 0;
    }
    if (g_shell->is_break > 0)
        return 0;
    int rc = 0;
    struct node_list *node_list = cast_void(node);

    for (size_t i = 0; i < node_list->nb_and_or; i++)
    {
        struct node_and_or *node_and_or = node_list->and_or[i];
        rc = node_and_or->base.exec(&node_and_or->base);
        if (rc != 0)
            return rc;
    }

    return rc;
}
