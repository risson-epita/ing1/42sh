/**
** \file src/exec/shell_command.c
**
** \brief Exec shell_command.
**
** \author 42Schmittos
*/

#include "exec.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_shell_command(struct ast_node *node)
{
    if (g_shell->is_continue > 0)
    {
        return 0;
    }
    if (g_shell->is_break > 0)
        return 0;
    struct list *fd_links = NULL;
    int rc = 0;

    struct node_shell_command *shell_command = cast_void(node);

    fd_links = exec_redirection(shell_command->redirections,
                                                shell_command->nb_redirections);

    if (fd_links)
        rc = shell_command->child->exec(shell_command->child);

    if (fd_links)
    {
        list_destroy(fd_links);
        return rc;
    }
    return ERROR_CODE;
}
