/**
** \file src/exec/else_clause.c
**
** \brief Exec else_clause.
**
** \author 42Schmittos
*/

#include "exec.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_else_clause(struct ast_node *node)
{
    if (g_shell->is_continue > 0)
    {
        return 0;
    }
    if (g_shell->is_break > 0)
        return 0;
    int rc = 0;

    struct node_else_clause *node_else_clause = cast_void(node);

    if (node_else_clause->type == ELSE)
    {
        struct node_compound_list *else_body
                                        = cast_void(node_else_clause->child);
        rc = else_body->base.exec(&else_body->base);
    }
    else
    {
        struct node_if *elif_body = cast_void(node_else_clause->child);
        rc = elif_body->base.exec(&elif_body->base);
    }

    return rc;
}
