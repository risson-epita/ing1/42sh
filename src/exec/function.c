/**
** \file src/exec/function.c
**
** \brief Exec a function
**
** \author 42Schmittos
*/

#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "exec.h"
#include "list.h"
#include "parser.h"
#include "shell.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_function(struct ast_node *node)
{
    if (g_shell->is_continue > 0)
    {
        return 0;
    }
    if (g_shell->is_break > 0)
        return 0;
    struct node_function *node_function = cast_void(node);
    struct node_shell_command *child= node_function->shell_command;
    return child->base.exec(&node_function->shell_command->base);
}
