/**
** \file src/exec/assignment_word.c
**
** \brief Exec an assignment word
**
** \author 42Schmittos
*/

#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "exec.h"
#include "shell.h"
#include "utils.h"

/**
** Function to add a variable on our environment.
**
** \param data which stores a data variable.
** \param envp the array which stores each assignments words.
** \param i the index.
** \param variable an integer to know if we have a word or an assignment word
*/
void add_variable_to_env(void *data, char **envp, size_t *i, int variable)
{
    char *name;
    char *value;
    if (variable)
    {
        struct variable *var = data;
        name = var->name;
        value = var->value;
    }
    else
    {
        struct node_assignment *var = data;
        name = var->name;
        value = var->value;
    }

    char *var;
    if (value)
    {
        var = calloc(strlen(name)
                + strlen(value) + 2, sizeof(char));
        var = strcat(var, name);
        var = strcat(var, "=");
        var = strcat(var, value);
    }
    else
        var = strdup(name);
    envp[*i] = var;
    *i += 1;
}

/**
** Function to copy a variable on our environment.
**
** \param node a node_simple_command.
**
** \return a node_assignment node.
*/
struct node_assignment **copy_assignment_list(struct node_simple_command *node)
{
    struct node_assignment **saved_assignments = calloc(
            node->nb_assignments, sizeof(struct node_assignment *));

    for (size_t i = 0; i < node->nb_assignments; i++)
    {
        struct node_assignment *copy = calloc(1,
                sizeof(struct node_assignment));
        copy->value = strdup(node->assignments[i]->value);
        copy->name = strdup(node->assignments[i]->name);

        saved_assignments[i] = copy;
    }

    return saved_assignments;
}

/**
** Function to free a variable on our environment.
**
** \param assignments a node_assignments to access to variables.
** \param len the number of variables to free.
**
** \return a node_assignment node.
*/
void free_assignments_list(struct node_assignment **assignments, size_t len)
{
    for (size_t i = 0; i < len; i++)
    {
        free(assignments[i]->value);
        free(assignments[i]->name);
        free(assignments[i]);
    }
}

/* Cf. documentation in `exec.h`. */
char **assignment_set(struct node_simple_command *simple_command)
{
    struct node_assignment **saved_assignments =
        copy_assignment_list(simple_command);

    expand_assignments(saved_assignments, simple_command->nb_assignments);

    size_t count = simple_command->nb_assignments;
    for (size_t i = 0; i < list_size(g_shell->variables); ++i)
    {
        struct variable *var = list_get(g_shell->variables, i);
        if (var->is_exported)
            ++count;
    }

    char **envp = calloc(count + 1, sizeof(char *));

    size_t i = 0;

    for (size_t j = 0; j < simple_command->nb_assignments; ++j)
    {
        struct node_assignment *assignment = saved_assignments[j];
        if (list_size(simple_command->words) == 0)
            variable_set(assignment->name, assignment->value, 0);

        add_variable_to_env(assignment, envp, &i, 0);
    }

    for (size_t j = 0; j < list_size(g_shell->variables); ++j)
    {
        struct variable *variable = list_get(g_shell->variables, j);
        if (!variable->is_exported)
            continue;

        add_variable_to_env(variable, envp, &i, 1);
    }

    free_assignments_list(saved_assignments, simple_command->nb_assignments);

    return envp;
}
