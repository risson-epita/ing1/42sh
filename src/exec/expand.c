/**
** \file src/exec/expand.c
**
** \brief All types of expand.
**
** \author 42schmittos
*/

#include <ctype.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "arithmetic_expansion.h"
#include "exec.h"
#include "lexer.h"
#include "list.h"
#include "parser.h"
#include "shell.h"
#include "utils.h"

int error;

char *expand_rec(char **word, char *c, char **result, char quoting_char);

/**
** Function to get the value of the `~`.
**
** \param expand the value of the expanded string.
** \param result the string where we stock the new string.
** \param tilde_start the index where the tilde start.
** \param name the name of the variable where `~` is.
**
*/
void get_tilde_var(char **expand, char **result, size_t *tilde_start,
        char *name)
{
    struct variable *var = variable_get(name);
    if (var)
        *expand = strdup(var->value);
    else
    {
        *result = append_char(*result, '~');
        *tilde_start += 1;
    }
}

/**
** Expand the tidle in a string.
** \param word a pointer to the basic string.
** \param c the index where we are in the string.
** \param result the string where we stock the new string.
** \return the new string
*/
char *expand_tilde(char **word, char *c, char **result)
{
    size_t tilde_start = c - *word;
    char *tilde = calloc(1, sizeof(char));
    char *expand = NULL;

    while (*c && !is_ifs(*c))
        tilde = append_char(tilde, *c++);

    if (strcmp(tilde, "~-") == 0)
    {
        get_tilde_var(&expand, result, &tilde_start, "OLDPWD");
    }
    else if (strcmp(tilde, "~+") == 0)
    {
        get_tilde_var(&expand, result, &tilde_start, "PWD");
    }
    else if (strcmp(tilde, "~") == 0 || *(tilde + 1) == '/')
    {
        expand = get_user_home();
        sprintf(tilde, "~");
    }
    else
    {
        struct passwd *pw = getpwnam(tilde + 1);
        if (pw)
            expand = strdup(pw->pw_dir);
        else
        {
            *result = append_char(*result, '~');
            tilde_start += 1;
        }
    }

    if (expand)
        *word = str_replace(*word, tilde, expand, tilde_start);

    free(tilde);
    free(expand);
    return *word + tilde_start;
}

/**
** Expand a variable in a string.
** \param word a pointer to the basic string.
** \param c the index where we are in the string.
** \param quoting_char the type of quoting we are in.
** \param result the string where we stock the new string.
** \return the new string
*/
char *expand_var(char **word, char *c, char quoting_char, char **result)
{
    size_t var_start = c - *word;
    char *var_name = calloc(1, 1);

    while (*c && !is_ifs(*c))
    {
        var_name = append_char(var_name, *c);
        if (correct_variable_name_syntax(var_name))
            ++c;
        else
        {
            var_name[strlen(var_name) - 1] = 0;
            break;
        }
    }

    if (strlen(var_name) == 0)
    {
        *result = append_char(*result, '$');
        free(var_name);
        return c;
    }

    struct variable *var = variable_get(var_name);

    if (*c == quoting_char)
        var_name = append_char(var_name, quoting_char);

    if (var)
    {
        *word = str_replace(*word, var_name, var->value, var_start);
    }
    else
    {
        *word = str_replace(*word, var_name, "", var_start);
    }

    free(var_name);
    c = *word + var_start;
    return c;
}

/**
** Substitute a command.
** \param word a pointer to the basic string.
** \param c the index where we are in the string.
** \param result accumulator storing the result of the expansion.
** \param end_char the ending character we are looking for.
** \return the new string
*/
char *substitute_command(char **word, char *c, char **result, char end_char)
{
    if (*c == 0)
    {
        *result = append_char(*result, end_char);
        return c;
    }

    size_t command_start = c - *word;
    char *command_name = calloc(1, 1);

    ssize_t nested = 0;

    while (*c && (*c != end_char || nested > 0))
    {
        if (*c == '(')
            nested += 1;
        if (*c == ')')
            nested -= 1;
        command_name = append_char(command_name, *c);
        ++c;
    }

    char *command = expand_command(command_name);
    command_name = append_char(command_name, end_char);

    if (command)
    {
        remove_end_newlines(command);
        *word = str_replace(*word, command_name, command, command_start);
    }
    else
    {
        *word = str_replace(*word, command_name, "", command_start);
    }

    c = *word + command_start;
    return c;
}

/**
** The second part of the expand function.
** \param word a pointer to the basic string.
** \param c the index where we are in the string.
** \param result the string where we stock the new string.
** \param quoting_char the type of quoting we are in.
** \return the new string
*/
char *expand_rec_2(char **word, char *c, char **result, char quoting_char)
{
    if (quoting_char == '\'' || quoting_char == '"')
    {
        if (*c == quoting_char)
            return expand_rec(word, c + 1, result, 0);
        *result = append_char(*result, *c);
        return expand_rec(word, c + 1, result, quoting_char);
    }

    if (is_ifs(*c))
    {
        while (is_ifs(*c))
            ++c;
        return c;
    }

    if (*c == '\'' || *c == '"')
        return expand_rec(word, c + 1, result, *c);

    if (quoting_char != ')' && is_globbing_path(word, c))
    {
        c = expand_path(word, c, result);
        return expand_rec(word, c, result, 0);
    }

    *result = append_char(*result, *c);
    return expand_rec(word, c + 1, result, quoting_char);
}

/* Cf. documentation in `exec.h`. */
char *expand_rec(char **word, char *c, char **result, char quoting_char)
{
    if (*c == '\0')
        return c;

    if (*c == '\\' && quoting_char != '\'')
    {
        c = handle_backslash(result, c, quoting_char);
        return expand_rec(word, c, result, quoting_char);
    }

    if (*c == '`' && quoting_char != '\'')
    {
        c = substitute_command(word, c + 1, result, '`');
        return expand_rec(word, c, result, quoting_char);
    }

    if (*c == '~' && c - 1 < *word) // if the tilde is a prefix
    {
        c = expand_tilde(word, c, result);
        return expand_rec(word, c, result, quoting_char);
    }

    if (*c == '$' && quoting_char != '\'' && !is_ifs(*(c + 1)))
    {
        c += 1;
        if (*c == '{')
            c = expand_var(word, c + 1, '}', result);
        else if (*c == '(' && *(c + 1) == '(')
        {
            c = expand_arithmetic(word, c + 2);
        }
        else if (*c == '(')
            c = substitute_command(word, c + 1, result, ')');
        else
            c = expand_var(word, c, quoting_char, result);

        return expand_rec(word, c, result, quoting_char);
    }

    return expand_rec_2(word, c, result, quoting_char);

}

/* Cf. documentation in `exec.h`. */
void expand_assignments(struct node_assignment **assignments, size_t len)
{
    for (size_t i = 0; i < len; i++)
    {
        char *value = assignments[i]->value;
        char *result = calloc(1, 1);

        expand_rec(&value, value, &result, 0);

        assignments[i]->value = result;
        free(value);
    }
}

/**
** Expand function for words.
** \param words a list of words.
** \param i the index of the word in the word list.
*/
void expand_word(struct list *words, size_t i)
{
    char *word = list_remove_at(words, i);
    char *start_of_next_word = word;
    do {
        char *result = calloc(1, 1);
        start_of_next_word = expand_rec(&word, start_of_next_word, &result, 0);

        if (strlen(result) == 0)
            free(result);
        else
            list_insert_at(words, result, i++);
    } while (*start_of_next_word);
    free(word);
}

/* Cf. documentation in `exec.h`. */
int expand_words(struct list *words)
{
    g_shell->arithmetic_error = 0;
    for (size_t i = 0; i < list_size(words); ++i)
        expand_word(words, i);

    if (g_shell->arithmetic_error)
    {
        list_clear(words);
        return g_shell->arithmetic_error;
    }

    return 0;
}
