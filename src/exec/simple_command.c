/**
** \file src/exec/simple_command.c
**
** \brief Exec simple_command and handle signal.
**
** \author 42Schmittos
*/

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "builtins.h"
#include "exec.h"
#include "list.h"
#include "shell.h"
#include "utils.h"

/**
** Free the environment
**
** \param envp the env to free.
*/
void free_env(char **envp)
{
    for (size_t i = 0; envp[i]; ++i)
        free(envp[i]);
    free(envp);
}

/* Cf. documentation in `exec.h`. */
int exec_simple_command(struct ast_node *node)
{
    if (g_shell->is_break > 0)
        return 0;
    struct list *fd_links = NULL;
    int rc_words = 0;

    struct node_simple_command *command = cast_void(node);

    char **envp = assignment_set(command);

    fd_links = exec_redirection(command->redirections,
                                                    command->nb_redirections);

    if (fd_links && list_size(command->words) > 0)
    {
        struct list *saved_words = list_init(NULL, free);
        for (size_t i = 0; i < list_size(command->words); i++)
            list_push_back(saved_words, strdup(list_get(command->words, i)));

        rc_words = expand_words(saved_words);
        if (rc_words)
            return rc_words;

        char **argv = cast_double_char(list_to_array(saved_words));

        if (list_size(saved_words) > 0)
            rc_words = exec_words(list_size(saved_words), argv, envp, fd_links);

        free(argv);
        list_destroy(saved_words);
    }

    free_env(envp);

    if (fd_links)
    {
        list_destroy(fd_links);
        return rc_words;
    }
    return ERROR_CODE;
}

/* Cf. documentation in `exec.h`. */
void child_process(char *argv[], char *envp[])
{
    execvpe(argv[0], argv, envp);
    errx(127, "%s: command not found", argv[0]);
}

/* Cf. documentation in `exec.h`. */
int wait_for_child(pid_t child_pid)
{
    int wstatus;

    do {
        pid_t w = waitpid(child_pid, &wstatus, WUNTRACED | WCONTINUED);
        if (w == -1)
            exit(ERROR_CODE);
    } while (!WIFEXITED(wstatus) && !WIFSIGNALED(wstatus));

    return WEXITSTATUS(wstatus);
}

/* Cf. documentation in `exec.h`. */
int exec_words(int argc, char *argv[], char *envp[], struct list *fd_links)
{
    fflush(stdout);
    fflush(stderr);

    struct node_function *node_function = list_get(g_shell->functions,
                                        list_find(g_shell->functions, argv[0]));
    if (node_function)
        return node_function->base.exec(&node_function->base);
    struct builtin *builtin = get_builtin(argv[0]);
    if (builtin)
        return builtin->func(argc, argv);

    pid_t child_pid = fork();

    if (child_pid == -1)
        err(1, "failed to fork");
    else if (child_pid == 0) // Child
        child_process(argv, envp);
    else // Parent
    {
        list_clear(fd_links);
        struct process *child = calloc(1, sizeof(struct process));
        child->pid = child_pid;
        list_push_back(g_shell->processes, child);
        int rc = wait_for_child(child_pid);
        free(list_remove_eq(g_shell->processes, child));
        fflush(stdout);
        fflush(stderr);
        return rc;
    }

    return 0;
}
