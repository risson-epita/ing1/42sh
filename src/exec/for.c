/**
** \file src/exec/for.c
**
** \brief Exec for.
*/

#include "exec.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_for(struct ast_node *node)
{
    int rc = 0;
    struct node_for *node_for = cast_void(node);
    struct node_compound_list *do_group = node_for->do_group;

    size_t empty_list = -1;
    if (list_size(node_for->word_list) == empty_list)
        return rc;

    expand_words(node_for->word_list);

    g_shell->loops++;
    for (size_t i = 0; i < list_size(node_for->word_list); i++)
    {
        char *value = list_get(node_for->word_list, i);
        variable_set(node_for->word, value, 0);

        rc = do_group->base.exec(&do_group->base);

        variable_unset(node_for->word);

        if (g_shell->is_continue > 0)
        {
            g_shell->is_continue--;
            continue;
        }
        if (g_shell->is_break == -1 || g_shell->is_continue == -1)
        {
            return 1;
        }
        if (g_shell->is_break > 0)
        {
            g_shell->loops--;
            g_shell->is_break--;
            return 0;
        }
    }

    g_shell->loops--;

    return rc;
}
