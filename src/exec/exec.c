/**
** \file src/exec/exec.c
**
** \brief Functions to execute from an AST given by the parser.
**
** \author 42Schmittos
*/

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "exec.h"
#include "list.h"
#include "parser.h"
#include "shell.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec(struct ast_node *node)
{
    int rc = node->exec(node);
    return rc;
}

/* Cf. documentation in `exec.h`. */
int exec_eol(struct ast_node *node)
{
    node = cast_void(node);
    return 0;
}
