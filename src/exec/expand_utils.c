/**
** \file src/exec/expand_utils.c
**
** \brief Utilities to expand stuff.
*/

#include <ctype.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "exec.h"
#include "lexer.h"
#include "list.h"
#include "parser.h"
#include "shell.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
char *str_replace(char *str, char *old, char *new, size_t word_index)
{
    char *pos = str;

    size_t old_size = strlen(old);

    char *new_str = calloc((strlen(new) - old_size) + strlen(str) + 1, 1);

    strcpy(new_str, "");

    char *search = strstr(str + word_index, old);

    if (search != NULL)
    {
        strncpy(new_str + strlen(new_str), pos, search - pos);
        strcpy(new_str + strlen(new_str), new);

        pos = search + old_size;
        strcpy(new_str + strlen(new_str), pos);
    }
    else
    {
        free(new_str);
        return str;
    }

    free(str);

    return new_str;
}

/* Cf. documentation in `exec.h`. */
void remove_end_newlines(char *command)
{
    for (ssize_t i = strlen(command) - 1; i > 0 && command[i] == '\n'; i--)
        command[i] = 0;
}

/* Cf. documentation in `exec.h`. */
int is_regular_file(char *s)
{
    if (strcmp(s, ".") != 0 && strcmp(s, "..") != 0)
        return 1;
    return 0;
}

char *handle_backslash(char **result, char *c, char quoting_char)
{
    ++c;
    if (*c == 0)
    {
        *result = append_char(*result, '\\');
        return c;
    }

    if (quoting_char != 0
            && *c != '$' && *c != '`' && *c != '"' && *c != '\\' && *c != '\n')
        *result = append_char(*result, '\\');

    *result = append_char(*result, *c);
    return c + 1;
}

char *expand_word_to_str(char *word)
{
    char *result = calloc(1, 1);
    char *word_copy = strdup(word);
    char *start_of_next_word = word_copy;

    do {
        char *sub_result = calloc(1, 1);
        start_of_next_word = expand_rec(&word_copy,
                start_of_next_word, &sub_result, ')');

        if (strlen(sub_result) != 0)
            result = append_str(result, sub_result);

        free(sub_result);
    } while (*start_of_next_word);

    free(word_copy);

    return result;
}
