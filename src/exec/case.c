/**
** \file src/exec/case.c
**
** \brief Exec a case.
**
** \author 42Schmittos
*/

#include <fnmatch.h>

#include "exec.h"
#include "parser.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_case(struct ast_node *node)
{
    if (g_shell->is_continue > 0)
    {
        return 0;
    }
    if (g_shell->is_break > 0)
        return 0;

    int rc = 0;
    struct node_case *node_case = cast_void(node);
    struct node_case_clause *node_case_clause = node_case->node_case_clause;
    if (!node_case_clause)
    {
        return rc;
    }

    char *expanded_word = expand_word_to_str(node_case->word);
    free(node_case->word);
    node_case->word = expanded_word;

    struct node_case_item **case_items =
        node_case->node_case_clause->case_items;

    for (size_t i = 0; i < node_case_clause->nb_case_item; i++)
    {
        for (size_t j = 0; j < case_items[i]->nb_word; j++)
        {
            char *expanded_item = expand_word_to_str(case_items[i]->words[j]);
            free(case_items[i]->words[j]);
            case_items[i]->words[j] = expanded_item;

            if (!fnmatch(case_items[i]->words[j], node_case->word, 0))
            {
                return case_items[i]->base.exec(&case_items[i]->base);
            }
        }
    }

    return rc;
}
