/**
** \file src/exec/case_item.c
**
** \brief Exec a case.
**
** \author 42Schmittos
*/

#include "parser.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_case_item(struct ast_node *node)
{
    if (g_shell->is_continue > 0)
    {
        return 0;
    }
    if (g_shell->is_break > 0)
        return 0;
    int rc = 0;
    struct node_case_item *node_case_item = cast_void(node);

    struct node_compound_list *case_item_list
                                            = node_case_item->compound_list;
    rc = case_item_list->base.exec(&case_item_list->base);
    return rc;
}
