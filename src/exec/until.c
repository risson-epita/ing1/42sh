/**
** \file src/exec/until.c
**
** \brief Exec until.
**
** \author 42Schmittos
*/

#include "exec.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_until(struct ast_node *node)
{
    struct node_until *node_until = cast_void(node);

    struct node_compound_list *condition = node_until->condition;
    struct node_compound_list *do_group = node_until->do_group;

    int rc = 0;

    g_shell->loops++;

    while (condition->base.exec(&condition->base) != 0)
    {
        rc = do_group->base.exec(&do_group->base);

        if (g_shell->is_continue > 0)
        {
            g_shell->is_continue--;
            continue;
        }

        if (g_shell->is_break == -1 || g_shell->is_continue == -1)
        {
            return 1;
        }
        if (g_shell->is_break > 0)
        {
            g_shell->loops--;
            g_shell->is_break--;
            return 0;
        }
    }

    g_shell->loops--;
    return rc;
}
