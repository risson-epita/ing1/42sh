/**
** \file src/exec/if.c
**
** \brief Exec if.
**
** \author 42Schmittos
*/

#include "exec.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_if(struct ast_node *node)
{
    if (g_shell->is_continue > 0)
    {
        return 0;
    }
    if (g_shell->is_break > 0)
        return 0;
    struct node_if *node_if = cast_void(node);

    struct node_compound_list *condition = node_if->condition;
    struct node_compound_list *if_body = node_if->if_body;
    struct node_else_clause *else_clause = node_if->else_clause;

    int rc_condition = condition->base.exec(&condition->base);


    int rc = 0;

    if (rc_condition == 0)
        rc = if_body->base.exec(&if_body->base);
    else if (else_clause != NULL)
        rc = else_clause->base.exec(&else_clause->base);

    return rc;
}
