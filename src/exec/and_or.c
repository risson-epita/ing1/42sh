/**
** \file src/exec/and_or.c
**
** \brief Exec And_or.
**
** \author 42Schmittos
*/

#include <err.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "exec.h"
#include "utils.h"

/* Cf. documentation in `exec.h`. */
int exec_and_or(struct ast_node *node)
{
    if (g_shell->is_continue > 0 || g_shell->is_break > 0)
        return 0;

    struct node_and_or *node_and_or = cast_void(node);
    int rc = 0;

    if (node_and_or->nb_pipelines == 1)
    {
        struct node_pipeline *node_pipeline = node_and_or->pipelines[0];
        return node_pipeline->base.exec(&node_pipeline->base);
    }

    size_t i = 0;
    struct node_pipeline *node_pipeline = node_and_or->pipelines[i];
    int command_rc = node_pipeline->base.exec(&node_pipeline->base);
    rc = !command_rc;
    i++;
    while (i < node_and_or->nb_pipelines)
    {
        if (strcmp(node_and_or->operators[i - 1], "&&") == 0)
        {
            node_pipeline = node_and_or->pipelines[i];
            rc = rc && !(command_rc =
                    node_pipeline->base.exec(&node_pipeline->base));
        }

        if (strcmp(node_and_or->operators[i - 1], "||") == 0)
        {
            node_pipeline = node_and_or->pipelines[i];
            rc = rc || !(command_rc =
                    node_pipeline->base.exec(&node_pipeline->base));
        }

        i++;
    }
    return command_rc;
}
