/**
** \file src/exec/redirection.c
**
** \brief Exec redirections.
**
** \author 42Schmittos
*/

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "builtins.h"
#include "exec.h"
#include "list.h"
#include "utils.h"
#include "shell.h"

/** The number of redirections available. */
#define NB_REDIRECTIONS sizeof(functions) / sizeof(functions[0])

/**
** Comparison function for a fd_link list.
**
** \param lhs first element to compare.
** \param rhs second element to compare.
**
** \return an int superior, inferior or equal to the comparison of the
** two elements.
*/
int fd_link_cmp(void *lhs, void *rhs)
{
    struct fd_link *left = lhs;
    struct fd_link *right = rhs;
    if (left->process_fd < right->process_fd)
        return LIST_LOWER;
    if (left->process_fd > right->process_fd)
        return LIST_LOWER;
    return LIST_EQUAL;

}

/**
** Free the comparison function for a fd_link list.
**
** \param element the element to free.
*/
void fd_link_free(void *element)
{
    struct fd_link *fd_link = element;
    close(fd_link->file_fd);
    dup2(fd_link->save_process_fd, fd_link->process_fd);
    close(fd_link->save_process_fd);
    free(element);
}

/**
** Open a file with certain flags.
**
** \param filename the name of the file to open.
** \param flags the flags for `open`.
**
** \return the fd of the opened file.
*/
int redirect_open(char *filename, int flags)
{
    int new_fd = open(filename, flags, 0644);
    if (new_fd == -1)
    {
        warn("%s", filename);
        return -1;
    }

    return new_fd;
}

/**
** Same as `redirect_open`, but fail if `noclobber` option is set and file
** already exists.
**
** \param filename the name of the file to open.
** \param flags the flags for `open`.
**
** \return the fd of the opened file.
*/
int redirect_open_output_noclobber(char *filename, int flags)
{
    if (set_is_set("noclobber") && access(filename, F_OK) == 0)
    {
        warnx("%s: cannot overwrite existing file", filename);
        return -1;
    }
    int new_fd = open(filename, flags, 0644);

    if (new_fd == -1)
    {
        warn("%s", filename);
        return -1;
    }

    return new_fd;
}

/**
** Create a temporary file and fill it with text.
**
** \param text the text to put in the temporary file.
** \param flags the flags for `open`.
**
** \return the fd of the opened file.
*/
int redirect_open_heredoc(char *text, int flags)
{
    int new_fd = open(P_tmpdir, flags, 0664);

    if (new_fd == -1)
    {
        warn("Opening temporary file failed");
        return -1;
    }

    char *start_of_next_word = text;
    do {
        char *result = calloc(1, 1);
        start_of_next_word = expand_rec(&text, start_of_next_word, &result, 0);

        ssize_t len = strlen(result);
        ssize_t count = 0;
        while (count != len)
        {
            ssize_t rc = write(new_fd, result + count, len - count);
            if (rc == -1)
            {
                warn(NULL);
                return -1;
            }
            count += rc;
        }
    } while(*start_of_next_word);


    lseek(new_fd, 0, SEEK_SET);

    return new_fd;
}

/**
** Handle duplicate redirections file opening.
**
** \param text the word on the right of the redirection.
** \param flags the flags for `fcntl`.
**
** \return the fd to duplicate.
*/
int redirect_duplicate(char *text, int flags)
{
    if (!strcmp("-", text))
        return -2;
    int new_fd = atoi(text);
    if (fcntl(new_fd, flags) != -1 || errno != EBADF)
        return new_fd;
    return -1;
}

/**
** Association of a redireciton type and a function to open the right file,
** with the right flags.
*/
struct redirection_fun
{
    /** The type of redirection. */
    enum token_type type;
    /** The function to execute to open the file. */
    int (*fun)(char *filename, int flags);
    /** The flags for opening the file. */
    int flags;
};

/** List of supported redirecitons. */
struct redirection_fun functions[] =
{
    { TOK_LESS, redirect_open, O_RDONLY },
    { TOK_MORE, redirect_open_output_noclobber, O_CREAT | O_WRONLY | O_TRUNC },
    { TOK_DMORE, redirect_open, O_CREAT | O_WRONLY | O_APPEND },
    { TOK_MOREPIPE, redirect_open, O_CREAT | O_WRONLY | O_TRUNC },
    { TOK_LESSMORE, redirect_open, O_CREAT | O_RDWR },
    { TOK_DLESS, redirect_open_heredoc, O_TMPFILE | O_RDWR },
    { TOK_DLESSDASH, redirect_open_heredoc, O_CREAT | O_RDWR | O_TRUNC },
    { TOK_LESSAND, redirect_duplicate, F_GETFD },
    { TOK_MOREAND, redirect_duplicate, F_GETFD },
};

/* Cf. documentation in `exec.h`. */
struct list *exec_redirection(struct node_redirection **redirections,
                                                        size_t nb_redirections)
{
    struct list *fd_links = list_init(fd_link_cmp, fd_link_free);
    for (size_t i = 0; i < nb_redirections; i++)
    {
        struct node_redirection *node_redirection = redirections[i];

        struct fd_link *current_fd_link = calloc(1, sizeof(struct fd_link));
        current_fd_link->process_fd = node_redirection->io;

        struct fd_link *exist_fd_link = list_get(fd_links,
                                        list_find(fd_links, current_fd_link));

        if (exist_fd_link)
            free(list_remove_eq(fd_links, exist_fd_link));

        for (size_t j = 0; j < NB_REDIRECTIONS; j++)
        {
            if (functions[j].type == node_redirection->type)
            {
                current_fd_link->file_fd
                                    = functions[j].fun(node_redirection->value,
                                                            functions[j].flags);
                if (current_fd_link->file_fd == -1)
                {
                    list_destroy(fd_links);
                    return NULL;
                }
            }
        }

        list_push_back(fd_links, current_fd_link);
    }

    for (size_t i = 0; i < list_size(fd_links); ++i)
    {
        struct fd_link *current_fd_link = list_get(fd_links, i);
        current_fd_link->save_process_fd = dup(current_fd_link->process_fd);
        if (current_fd_link->file_fd == -2)
            close(current_fd_link->process_fd);
        else
            dup2(current_fd_link->file_fd, current_fd_link->process_fd);
    }

    return fd_links;
}
