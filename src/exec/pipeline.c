/**
** \file src/exec/pipeline.c
**
** \brief Exec Pipeline.
**
** \author 42Schmittos
*/

#include <err.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "exec.h"
#include "utils.h"

int save_stdin;

/**
** Function to fork in a pipe.
**
** \param fd_in the filedescriptor where we read.
** \param fd_out the filedescriptor where we write.
** \param node the `struct node_command` ast.
** \param to_close file descriptor to close after fork.
**
** \return the pid of the child.
*/
pid_t fork_pipe(int fd_in, int fd_out, struct node_command *node, int to_close)
{
    pid_t pid = fork();
    if (pid == -1)
    {
        err(2, "failed to fork");
    }
    else if (pid == 0)
    {
        if (g_shell->mode == M_INTER)
            sigaction(SIGINT, &(g_shell->default_sigaction), NULL);
        g_shell->mode = M_STDIN;
        if (fd_in != STDIN_FILENO)
        {
            dup2(fd_in, STDIN_FILENO);
            close(fd_in);
        }

        if (fd_out != STDOUT_FILENO)
        {
            dup2(fd_out, STDOUT_FILENO);
            close(fd_out);
        }

        int rc = node->base.exec(&node->base);
        close(to_close);
        close(save_stdin);
        close(fd_in);
        close(fd_out);
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        node->base.free(&node->base);
        exit(rc);
    }
    return pid;
}

/**
** Sub_function used to execute the last command of pipeline.
**
** \param node the `struct node_command` ast.
** \param fd_in the last_command input fd.
** \param fds the fds of the current pipe.
**
** \return 0 on success.
*/
int pipe_last_command(struct node_pipeline *node, int fd_in, int *fds)
{
    pid_t child_pid = fork_pipe(fd_in, STDOUT_FILENO,
                                node->commands[node->nb_commands - 1], fds[0]);
    struct process *child = calloc(1, sizeof(struct process));
    child->pid = child_pid;
    list_push_back(g_shell->processes, child);

    close(fd_in);
    dup2(save_stdin, STDIN_FILENO);
    close(save_stdin);

    int rc = 0;
    for (size_t i = 0; i < list_size(g_shell->processes); ++i)
    {
        struct process *process = list_get(g_shell->processes, i);
        rc = wait_for_child(process->pid);
        free(list_remove_at(g_shell->processes, i));
    }

    return rc;
}

/**
** Sub_function used for execute a pipeline.
**
** \param node the `struct node_command` ast.
**
** \return 0 on success.
*/
int pipe_commands(struct node_pipeline *node)
{
    int fd_in = STDIN_FILENO;
    save_stdin = dup(STDIN_FILENO);

    if (save_stdin == -1)
        err(2, "failed to dup file descriptor");

    int fds[2];

    for (size_t i = 0; i < node->nb_commands; i++)
    {
        if (pipe(fds) == -1)
            err(2, "failed to pipe");

        pid_t child_pid = fork_pipe(fd_in, fds[1], node->commands[i], fds[0]);

        struct process *child = calloc(1, sizeof(struct process));
        child->pid = child_pid;
        list_push_back(g_shell->processes, child);

        close(fd_in);
        close(fds[1]);
        fd_in = fds[0];
    }

    if (fd_in != STDIN_FILENO)
        dup2(fd_in, STDIN_FILENO);

    return pipe_last_command(node, fd_in, fds);
}

/* Cf. documentation in `exec.h`. */
int exec_pipeline(struct ast_node *node)
{
    struct node_pipeline *node_pipeline = cast_void(node);

    if (node_pipeline->nb_commands == 1)
    {
        struct node_command *node_command = node_pipeline->commands[0];
        if (node_pipeline->negation)
            return !node_command->base.exec(&node_command->base);
        return node_command->base.exec(&node_command->base);
    }

    if (node_pipeline->negation)
        return !pipe_commands(node_pipeline);
    return pipe_commands(node_pipeline);
}
