/**
** \file src/shell/home.c
**
** \brief Manage the history of the current shell in interactive mode.
**
** \author 42Schmittos.
*/

#include <err.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "shell.h"

/* Cf. documentation in `shell.h`. */
char *get_user_home(void)
{
    char *home = NULL;
    struct variable *var = variable_get("HOME");
    if (var)
        home = strdup(var->value);
    else
    {
        struct passwd *pw = getpwuid(getuid());
        if (pw && pw->pw_dir)
            home = strdup(pw->pw_dir);
        else
        {
            home = calloc(2,1);
            home[0] = '/';
        }
    }
    return home;
}
