#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "shell.h"

/* Cf. documentation in `parser.h`. */
struct alias *alias_find(char *name)
{
    struct alias to_find = { name, NULL };
    return list_get(g_shell->aliases, list_find(g_shell->aliases, &to_find));
}

/* Cf. documentation in `parser.h`. */
void alias_add(char *name, char *value)
{
    struct alias *new = malloc(sizeof(struct alias));
    new->name = strdup(name);
    new->value = strdup(value);
    aliases_free(list_remove_eq(g_shell->aliases, new));
    list_push_back(g_shell->aliases, new);
}

/* Cf. documentation in `parser.h`. */
int aliases_cmp(void *lhs, void *rhs)
{
    struct alias *left = lhs;
    struct alias *right = rhs;
    int rc = strcmp(left->name, right->name);
    if (rc < 0)
        return LIST_LOWER;
    if (rc > 0)
        return LIST_BIGGER;
    return LIST_EQUAL;
}

/* Cf. documentation in `parser.h`. */
void aliases_free(void *element)
{
    if (!element)
        return;
    struct alias *el = element;
    free(el->name);
    free(el->value);
    free(el);
}
