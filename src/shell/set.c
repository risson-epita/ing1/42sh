/**
** \file src/shell/set.c
**
** \brief Functions to manage shell options with builtin `set`.
**
** \author 42Schmittos
*/

#include <stdio.h>
#include <stddef.h>
#include <string.h>

/** The number of supported options. */
#define NB_OPTIONS sizeof(set_list) / sizeof(set_list[0])

/** Struct who stock the name and the value of an element. */
struct set
{
    /** The name of the element. */
    char *name;
    /** The value of the element. */
    int value;
};

/** set options list  */
struct set set_list[] =
{
    { "noclobber", 0 },
};

/* Cf. documentation in `shell.h`. */
int set_unset(char *name)
{
    for (size_t i = 0; i < NB_OPTIONS; ++i)
    {
        if (!strcmp(name, set_list[i].name))
        {
            set_list[i].value = 0;
            return 0;
        }
    }
    return 1;
}

/* Cf. documentation in `shell.h`. */
int set_set(char *name)
{
    for (size_t i = 0; i < NB_OPTIONS; ++i)
    {
        if (!strcmp(name, set_list[i].name))
        {
            set_list[i].value = 1;
            return 0;
        }
    }
    return 1;
}

/* Cf. documentation in `shell.h`. */
int set_is_set(char *name)
{
    for (size_t i = 0; i < NB_OPTIONS; ++i)
    {
        if (!strcmp(name, set_list[i].name))
        {
            return set_list[i].value;
        }
    }
    return -1;
}

/* Cf. documentation in `shell.h`. */
int set_print(char *name, int is_quiet)
{
    int rc = set_is_set(name);
    if (rc != -1 && !is_quiet)
        printf("%-15s %s\n", name, rc ? "on" : "off");
    return rc;
}

/* Cf. documentation in `shell.h`. */
int set_print_all(int is_set, int is_quiet, int is_command)
{
    size_t count = 0;
    for (size_t i = 0; i < NB_OPTIONS; ++i)
    {
        struct set current = set_list[i];
        if (is_set == -1 || is_set == current.value)
        {
            count += 1;
            if (is_quiet)
                continue;
            if (is_command)
                printf("set %s %s\n", current.value ? "-o" : "+o",
                                                 current.name);
            else
                printf("%-15s %s\n",
                                    current.name, current.value ? "on" : "off");
        }
    }
    return count != NB_OPTIONS;
}
