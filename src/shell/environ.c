/**
** \file src/shell/environ.c
**
** \brief Manage the history of the current shell in interactive mode.
**
** \author 42Schmittos.
*/

#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "shell.h"
#include "utils.h"

/** The number of implemented special variables */
#define NB_SPECIAL_VARIABLES sizeof(special_variables) \
                                                / sizeof(special_variables[0])

/**
** Creates a variable containing `$RANDOM`.
**
** \return `$RANDOM`.
*/
struct variable *variable_random(void)
{
    char *random_str = itoa(rand() % 32768);
    struct variable *var = variable_init("RANDOM", random_str);
    free(random_str);
    return var;
}

/**
** Creates a variable containing `$?`.
**
** \return `$?`.
*/
struct variable *variable_last_rc(void)
{
    char *last_rc = itoa(g_shell->last_rc);
    struct variable *var = variable_init("?", last_rc);
    free(last_rc);
    return var;
}

/** Represents a special variable with the function to create it. */
struct special_variable
{
    /** The name of the special variable. */
    char *name;
    /** The function to get the value of the variables. */
    struct variable *(*func)(void);
};

/** The list of implemented special variables. */
struct special_variable special_variables[] =
{
    { "RANDOM", variable_random },
    { "?", variable_last_rc },
};

/* Cf. documentation in `shell.h`. */
struct variable *variable_get(char *name)
{
    for (size_t i = 0; i < NB_SPECIAL_VARIABLES; ++i)
    {
        if (!strcmp(name, special_variables[i].name))
            return special_variables[i].func();
    }
    if (!g_shell)
        return NULL;
    return list_get(g_shell->variables, list_find(g_shell->variables, name));
}

/* Cf. documentation in `shell.h`. */
void variable_set(char *name, char *value, int is_exported)
{
    struct variable *var = variable_init(name, value);
    var->is_exported = is_exported;

    size_t i = list_find(g_shell->variables, name);
    size_t minus_one = -1;
    if (i != minus_one)
        variable_free(list_remove_at(g_shell->variables, i));

    list_push_back(g_shell->variables, var);
}

/* Cf. documentation in `shell.h`. */
void variable_unset(char *name)
{
    struct variable *var = list_remove_eq(g_shell->variables, name);
    if (var)
        variable_free(var);
}
