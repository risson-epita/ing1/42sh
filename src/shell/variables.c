/**
** \file src/shell/variables.c
**
** \brief Helper functions managing shell variables.
**
** \author 42Schmittos
*/

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "list.h"
#include "shell.h"
#include "utils.h"

/* Cf. documentation in `shell.h`. */
void variable_add_specials(int ind, char *argv[])
{
    variable_set("IFS", " \t\n", 0);
    char *n;
    variable_set("0", argv[0], 0);
    size_t i = 1;
    for (i = 1; argv[ind + i - 1]; ++i)
    {
        n = itoa(i);
        variable_set(n, argv[ind + i - 1], 0);
        free(n);
    }
    n = itoa(i);
    variable_set("#", n, 0);
    free(n);
    n = itoa(getpid());
    variable_set("$", n, 1);
    free(n);
    n = itoa(getuid());
    variable_set("UID", n, 1);
    free(n);
    variable_set("PS1", "\\s\\$ ", 0);
    variable_set("PS2", "> ", 0);
    char buffer_pwd[PATH_MAX + 1] = { 0 };
    if (getcwd(buffer_pwd, PATH_MAX))
    {
        variable_set("OLDPWD", buffer_pwd, 0);
        variable_set("PWD", buffer_pwd, 0);
    }
}

/* Cf. documentation in `variables.h`. */
struct variable *variable_init(char *name, char *value)
{
    struct variable *var = calloc(1, sizeof(struct variable));
    if (name)
        var->name = strdup(name);
    if (value)
        var->value = strdup(value);
    return var;
}

/* Cf. documentation in `variables.h`. */
struct variable *environ_to_struct(char *var)
{
    char *name = NULL;
    char *value = NULL;

    char *equal = strchr(var, '=');
    if (!equal)
    {
        name = strdup(var);
        value = NULL;
    }
    else if (*(equal + 1) == '\0')
    {
        name = strndup(var, equal - var);
        value = calloc(1, 1);
    }
    else
    {
        name = strndup(var, equal - var);
        value = strdup(equal + 1);
    }

    struct variable *variable = variable_init(name, value);
    free(name);
    free(value);

    return variable;
}

/* Cf. documentation in `variables.h`. */
int variable_cmp(void *lhs, void* rhs)
{
    struct variable *var = lhs;
    char *name = rhs;
    int rc = strcmp(var->name, name);
    if (rc < 0)
        return LIST_LOWER;
    if (rc > 0)
        return LIST_BIGGER;
    return LIST_EQUAL;
}

/* Cf. documentation in `variables.h`. */
void variable_free(void *p)
{
    if (!p)
        return;
    struct variable *var = p;
    free(var->name);
    free(var->value);
    free(var);
}
