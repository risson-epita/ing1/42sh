/**
** \file src/shell/shopt.c
**
** \brief Helper functions managing shell options for builtin `shopt`.
**
** \author 42Schmittos
*/

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** The number of supporteds options. */
#define NB_OPTIONS sizeof(shopt_list) / sizeof(shopt_list[0])

/** struct who store the name and the value of a element */
struct shopt
{
    /** The name of the element. */
    char *name;
    /** The value of the element. */
    int value;
};

/** The list of shopt options */
struct shopt shopt_list[] =
{
    { "ast_print", 0 },
    { "dotglob", 0 },
    { "expand_aliases", 0 },
    { "extglob", 0 },
    { "nocaseglob", 0 },
    { "nullglob", 0 },
    { "sourcepath", 1 },
    { "xpg_echo", 0 },
};

/* Cf. documentation in `shell.h`. */
int shopt_unset(char *name)
{
    for (size_t i = 0; i < NB_OPTIONS; ++i)
    {
        if (!strcmp(name, shopt_list[i].name))
        {
            shopt_list[i].value = 0;
            return 0;
        }
    }
    return 1;
}

/* Cf. documentation in `shell.h`. */
int shopt_set(char *name)
{
    for (size_t i = 0; i < NB_OPTIONS; ++i)
    {
        if (!strcmp(name, shopt_list[i].name))
        {
            shopt_list[i].value = 1;
            return 0;
        }
    }
    return 1;
}

/* Cf. documentation in `shell.h`. */
int shopt_is_set(char *name)
{
    for (size_t i = 0; i < NB_OPTIONS; ++i)
    {
        if (!strcmp(name, shopt_list[i].name))
        {
            return shopt_list[i].value;
        }
    }
    return -1;
}

/* Cf. documentation in `shell.h`. */
int shopt_print(char *name, int is_quiet)
{
    int rc = shopt_is_set(name);
    if (rc != -1 && !is_quiet)
        printf("%-15s %s\n", name, rc ? "on" : "off");
    return rc;
}

/* Cf. documentation in `shell.h`. */
int shopt_print_all(int is_set, int is_quiet, int is_command)
{
    size_t count = 0;
    for (size_t i = 0; i < NB_OPTIONS; ++i)
    {
        struct shopt current = shopt_list[i];
        if (is_set == -1 || is_set == current.value)
        {
            count += 1;
            if (is_quiet)
                continue;
            if (is_command)
                printf("shopt %s %s\n",
                                    current.value ? "-s" : "-u", current.name);
            else
                printf("%-15s %s\n",
                                    current.name, current.value ? "on" : "off");
        }
    }
    return count != NB_OPTIONS;
}
