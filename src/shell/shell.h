/**
** \file src/shell/shell.h
**
** \brief Stores all needed information throughout the shell execution.
**
** \author 42Schmittos
*/

#ifndef SHELL_H
#define SHELL_H

#include <dirent.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>

/** Differents input modes */
enum mode
{
    /** From a file */
    M_FILE,
    /** With the `-c` */
    M_CMDSTR,
    /** From stdin with `|` */
    M_STDIN,
    /** From the interactive mode */
    M_INTER
};

/** Stores all needed information throughout the shell execution. */
struct shell
{
    /** List of all exported variables. */
    struct list *variables;
    /** List of all launched processes. */
    struct list *processes;
    /** List of all functions. */
    struct list *functions;
    /** List of all aliases. */
    struct list *aliases;
    /** The default handler for signals. */
    struct sigaction default_sigaction;
    /** A list of opened `DIR *`. */
    struct list *opendirs;
    /** The number of loops we are in. */
    int loops;
    /** int indicates if we need to break or not and if yes how much times*/
    int is_break;
    /** int indicates if we need to break or not and if yes how much times*/
    int is_continue;
    /** the return code of the last executed command. */
    int last_rc;
    /** the arithmetic error code */
    int arithmetic_error;
    /** the input mode of the current shell. */
    enum mode mode;
    /** Where to resume execution after a `SIGINT`. */
    sigjmp_buf sigint_buf;
};
/** Store all needed information throughout the shell execution. */
extern struct shell *g_shell;
/**
** Initialize the global variable `g_shell`.
*/
void shell_init(enum mode mode, int ind, char *argv[], char *envp[]);
/**
** Free everything allocated in the global variable `g_shell`.
*/
void shell_free(void);

/** Represent an entry of the history. */
struct history
{
    /** Command ran. */
    char *command;
    /** Whether it was loaded from the history file. */
    int from_file;
};
/**
** Add a word to the history. If this word is `\n`, a new entry is created in
** the history list.
**
** \param word the word to add.
*/
void history_add_word(char *word);
/**
** Append the history list to `~/.42sh_history`.
*/
void history_write_to_file(void);

/**
** Set the shopt option of name `name`.
**
** \param name the name of the option.
**
** \return a int used as a return code.
*/
int shopt_set(char *name);
/**
** Unset the shopt option of name `name`.
**
** \param name the name of the option.
**
** \return a int used as a return code.
*/
int shopt_unset(char *name);
/**
** Check if an option of name `name is set`.
**
** \param name the name of the option.
**
** \return 1 if its set 0 otherwise.
*/
int shopt_is_set(char *name);
/**
** Print function for an option of shopt.
**
** \param name the name of the option to print.
** \param is_quiet if 1 disable the output.
**
** \return 0 if all went well, 1 otherwise
*/
int shopt_print(char *name, int is_quiet);
/**
** Print all options managed by `shopt`.
**
** \param is_set 1: print all set options, 0: print all unset options, -1: print
**               all options.
** \param is_quiet don't print anything, just report the result via the return
**                 code.
** \param is_command format output as commands that can be run.
**
** \return 1 if all options are set, 0 otherwise.
*/
int shopt_print_all(int is_set, int is_quiet, int is_command);
/* Cf. documentation in `parser.h`. */
/**
** Set an option with the name `name` of set.
**
** \param name the name of the option.
**
** \return 1 if its set 0 otherwise.
*/
int set_set(char *name);
/**
** Unset an option with the name `name` of set.
**
** \param name the name of the option
**
** \return 2 if its unset 0 otherwise
*/
int set_unset(char *name);
/**
** Check if an option is set or not.
**
** \param name the name of the option.
**
** \return 1 if its set, 0 otherwise.
*/
int set_is_set(char *name);
/**
** Print function for a set option.
**
** \param name the name of the option.
** \param is_quiet if 1 disable the output.
**
** \return a int used as an error code
*/
int set_print(char *name, int is_quiet);
/**
** Print all options managed by `set`.
**
** \param is_set 1: print all set options, 0: print all unset options, -1: print
**               all options.
** \param is_quiet don't print anything, just report the result via the return
**                 code.
** \param is_command format output as commands that can be run.
**
** \return 1 if all options are set, 0 otherwise.
*/
int set_print_all(int is_set, int is_quiet, int is_command);

/** Stores a process' information. */
struct process
{
    /** The process' PID. */
    pid_t pid;
    /** The signal to send to it if we free the list. `0` for no signal. */
    int signum;
};
/**
** Compare two processes by checking if their PID is the same.
**
** \param lhs the first process to compare.
** \param rhs the second process to compare.
**
** \return `LIST_[...]` depending on the result.
*/
int process_cmp(void *lhs, void *rhs);
/**
** Free a `process` structure, and kill it if its `signum` field is not `-1`.
**
** \param p the process structure to free.
*/
void process_free(void *p);
/**
** `SIGINT` handler.
**
** \param signum the signal sent to the program.
*/
void sigint_handler(int signum);

/** Represent a shell variable. */
struct variable
{
    /** The name of the variable. */
    char *name;
    /** The value of the variable. */
    char *value;
    /** Whether the variable must be transmitted to a simple command. */
    int is_exported;
};
/**
** Initialize a variable structure.
**
** \param name the name of the variable to initialize.
** \param value the value of the variable to initialize.
**
** \return the newly created variable.
*/
struct variable *variable_init(char *name, char *value);
/**
** Compare two variables.
**
** \param lhs the variable to compare.
** \param rhs the name to compare the variable to.
**
** \return `LIST_[...]` depending on the result.
*/
int variable_cmp(void *lhs, void* rhs);
/**
** Free a variable.
**
** \param p the variable to free.
*/
void variable_free(void *p);
/**
** Get a variable from its name.
**
** \param name the name of the variable to find.
**
** \return the variable, `NULL` if it does not exists.
*/
struct variable *variable_get(char *name);
/**
** Create a variable and add it to the global list of variables.
**
** \param name the name of the variable.
** \param value the value of the variable.
** \param is_exported whether the variable must be transmitted to a simple
**                    command.
*/
void variable_set(char *name, char *value, int is_exported);
/**
** Unset a variable.
**
** \param name the name of the variable to unset.
*/
void variable_unset(char *name);
/**
** Add variables from `argv` to `$0`, `$1`, `$2`, ...
**
** \param ind the index where the arguments start (`$1` is at `ind`).
** \param argv the array of arguments.
*/
void variable_add_specials(int ind, char *argv[]);
/**
** Convert an environment variable of the form `name=value` to a structure.
**
** \param var the variable to convert.
**
** \return the converted variable.
*/
struct variable *environ_to_struct(char *var);

/**
** Get the user home directory. If it cannot be found, it defaults to `/`.
**
** \return the user home directory.
*/
char *get_user_home(void);

/** Represent a shell alias. */
struct alias
{
    /** The name of the alias. */
    char *name;
    /** The value of the alias. */
    char *value;
};

/**
** Find an alias by its name.
**
** \param name the name of the alias to find.
**
** \return the alias if it exists, `NULL` otherwise.
*/
struct alias *alias_find(char *name);
/**
** Add a new alias.
**
** \param name the name of the alias to add.
** \param value the value of the alias to add.
*/
void alias_add(char *name, char *value);
/**
** Compare two aliases structure by their name.
**
** \param lhs the first alias to compare.
** \param rhs the second alias to compare.
**
** \return `LIST_[...]` depending on the result.
*/
int aliases_cmp(void *lhs, void *rhs);
/**
** Free a structure alias.
**
** \param element the alias to free.
*/
void aliases_free(void *element);

#endif /* ! SHELL_H */
