/**
** \file src/shell/history.c
**
** \brief Manage the history of the current shell in interactive mode.
**
** \author 42Schmittos.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <readline/history.h>

#include "shell.h"

/* Cf. documentation in `history.c`. */
void history_add_word(char *word)
{
    if (history_length == 0)
    {
        add_history(calloc(1, 1));
        if (!strcmp("\n", word))
            return;
    }

    HIST_ENTRY *entry = history_get(history_length);

    if (!strcmp("\n", word) && strlen(entry->line) > 0)
    {
        add_history(calloc(1, 1));
        return;
    }

    entry->line = realloc(entry->line,
                                    strlen(entry->line) + strlen(word) + 1);
    entry->line = strcat(entry->line, word);
}

/* Cf. documentation in `history.c`. */
void history_write_to_file(void)
{
    char *user_home = get_user_home();
    char *history_local = "/.42sh_history";
    char *history_filename = calloc(1,
                                strlen(user_home) + strlen(history_local) + 1);
    history_filename = strcat(history_filename, user_home);
    history_filename = strcat(history_filename, history_local);

    if (access(history_filename, W_OK) != -1)
        append_history(history_length, history_filename);
    else
        write_history(history_filename);

    free(history_filename);
    free(user_home);
}
