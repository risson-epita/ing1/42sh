/**
** \file src/shell/shell.c
**
** \brief Functions to manage global information needed throughout the shell.
**
** \author 42Schmittos
*/

#include <dirent.h>
#include <err.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

#include <readline/history.h>

#include "list.h"
#include "shell.h"
#include "utils.h"

/* Cf. documentation in `shell.h`. */
struct shell *g_shell;

int function_cmp(void *lhs, void* rhs);
void function_clear(void *p);

/**
** Convert a environment array to a doubly linked list.
**
** \param envp the environment array to convert.
**
** \return a list holding all the variables converted.
*/
struct list *envp_to_variables(char *envp[])
{
    struct list *variables = list_init(variable_cmp, variable_free);

    for (int i = 0; envp[i]; ++i)
    {
        struct variable *var = environ_to_struct(envp[i]);
        var->is_exported = 1;
        list_push_back(variables, var);
    }

    return variables;
}

void opendirs_free(void *element)
{
    if (!element)
        return;
    DIR *dir = element;
    closedir(dir);
}

int opendirs_cmp(void *lhs, void *rhs)
{
    if (lhs < rhs)
        return LIST_LOWER;
    if (lhs > rhs)
        return LIST_BIGGER;
    return LIST_EQUAL;
}

/* Cf. documentation in `shell.h`. */
void shell_init(enum mode mode, int ind, char *argv[], char *envp[])
{
    g_shell = calloc(1, sizeof(struct shell));

    g_shell->variables = envp_to_variables(envp);
    variable_add_specials(ind, argv);

    g_shell->processes = list_init(process_cmp, process_free);

    g_shell->functions = list_init(function_cmp, function_clear);

    g_shell->aliases = list_init(aliases_cmp, aliases_free);

    g_shell->opendirs = list_init(opendirs_cmp, opendirs_free);

    g_shell->loops = 0;
    g_shell->is_break = 0;
    g_shell->is_continue = 0;

    g_shell->last_rc = 0;

    g_shell->mode = mode;

    if (mode != M_INTER)
        return;

    using_history();

    shopt_set("expand_aliases");

    struct sigaction new_action;
    new_action.sa_handler = &sigint_handler;
    sigemptyset(&new_action.sa_mask);
    new_action.sa_flags = 0;
    if (sigaction(SIGINT, &new_action, &(g_shell->default_sigaction)) == -1)
        err(ERROR_CODE, "sigint_handler: error while catching signals");
}

/* Cf. documentation in `shell.h`. */
void shell_free(void)
{
    history_write_to_file();

    list_destroy(g_shell->variables);

    for (size_t i = 0; i < list_size(g_shell->processes); ++i)
    {
        struct process *process = list_get(g_shell->processes, i);
        process->signum = SIGTERM;
    }
    list_destroy(g_shell->processes);

    list_destroy(g_shell->functions);

    list_destroy(g_shell->aliases);

    list_destroy(g_shell->opendirs);

    free(g_shell);
}
