/**
** \file src/shell/processes.c
**
** \brief Handlers for processes launched by `42sh`.
**
** \author 42Schmittos
*/

#include <err.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "list.h"
#include "shell.h"
#include "utils.h"

/* Cf. documentation in `processes.h`. */
int process_cmp(void *lhs, void *rhs)
{
    struct process *left = lhs;
    struct process *right = rhs;
    if (left->pid < right->pid)
        return LIST_LOWER;
    if (left->pid > right->pid)
        return LIST_BIGGER;
    return LIST_EQUAL;
}

/* Cf. documentation in `processes.h`. */
void process_free(void *p)
{
    struct process *process = p;
    if (process->signum != 0)
    {
        int wstatus = 0;
        kill(process->pid, process->signum);
        pid_t w = waitpid(process->pid, &wstatus, WUNTRACED | WCONTINUED);
        if (w == -1)
            exit(ERROR_CODE);
    }
    free(process);
}

/* Cf. documentation in `processes.h`. */
void sigint_handler(int signum)
{
    if (signum == SIGINT)
    {
        for (size_t i = 0; i < list_size(g_shell->processes); ++i)
        {
            struct process *process = list_get(g_shell->processes, i);
            process->signum = SIGINT;
        }
        list_clear(g_shell->processes);

        list_clear(g_shell->opendirs);

        printf("\n");
        g_shell->last_rc = 130;
        siglongjmp(g_shell->sigint_buf, 1);
    }

    exit(signum);
}
