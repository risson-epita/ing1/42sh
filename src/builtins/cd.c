/**
** \file src/builtins/cd.c
**
** \brief Builtin commands.
**
** \author 42Schmittos
*/

#include <err.h>
#include <limits.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "list.h"
#include "shell.h"
#include "utils.h"

/** The default error return code of builtin `cd`. */
#define ERROR_CD 1

/**
** Check if a path is a directory.
**
** \param path the path to check
**
** \return whether `path` is a directory.
*/
int is_dir(char *path)
{
    struct stat buf;
    int rc = stat(path, &buf);
    if (rc == -1)
        return -1;
    return S_ISDIR(buf.st_mode);
}

/**
** Change current directory.
**
** \param path the directory to change to.
**
** \return the return code of the builtin `cd`.
*/
int cd_dir(char *path)
{
    char buffer_oldpwd[PATH_MAX + 1] = { 0 };
    char buffer_pwd[PATH_MAX + 1] = { 0 };
    if (!getcwd(buffer_oldpwd, PATH_MAX))
    {
        warn("cd: %s", path);
        return ERROR_CD;
    }
    int rc = chdir(path);
    if (rc == -1)
    {
        warn("cd: %s", path);
        return ERROR_CD;
    }
    if (!getcwd(buffer_pwd, PATH_MAX))
    {
        warn("cd: %s", path);
        return ERROR_CD;
    }
    variable_set("OLDPWD", buffer_oldpwd , 0);
    variable_set("PWD", buffer_pwd , 0);

    return 0;
}

/* Cf. documentation in `builtins.h`. */
int cd(int argc, char *argv[])
{
    if (argc > 2)
    {
        warnx("cd: too many arguments");
        return ERROR_CD;
    }
    if (argc == 1)
    {
        char *home = get_user_home();
        int rc = cd_dir(home);
        free(home);
        return rc;
    }
    if (strcmp("-", argv[1]) == 0)
    {
        if (!variable_get("OLDPWD"))
        {
            warnx("cd: OLDPWD not set");
            return ERROR_CD;
        }
        printf("%s\n", variable_get("OLDPWD")->value);
        return cd_dir(variable_get("OLDPWD")->value);
    }
    return cd_dir(argv[1]);
}
