/**
** \file src/builtins/builtins.h
**
** \brief Builtins commands.
**
** \author 42Schmittos
*/

#ifndef BUILTINS_H
#define BUILTINS_H

/** Represent a builtin. */
struct builtin
{
    /** Its name. */
    char *name;
    /** The function to execute when running the builtin. */
    int (*func)(int, char *[]);
};

/**
** Find the builtin by name.
**
** \param name the builtin's name
**
** \return the builtin searched, NULL if not found.
*/
struct builtin *get_builtin(char *name);

/**
** Builtin `history`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int history(int argc, char *argv[]);

/**
** Builtin `shopt`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int shopt(int argc, char *argv[]);

/**
** Builtin `set`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int set(int argc, char *argv[]);

/**
** Builtin `exit`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int builtin_exit(int argc, char *argv[]);

/**
** Builtin `cd`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int cd(int argc, char *argv[]);

/**
** Builtin `export`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int builtin_export(int argc, char *argv[]);

/**
** Builtin `source`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int source(int argc, char *argv[]);

/**
** Builtin `echo`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int echo(int argc, char *argv[]);

/**
** Builtin `break`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int builtin_break(int argc, char *argv[]);

/**
** Builtin `continue`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int builtin_continue(int argc, char *argv[]);

/**
** Builtin `alias`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int alias(int argc, char *argv[]);

/**
** Builtin `unalias`.
**
** \param argc the number of arguments.
** \param argv the list of arguments.
**
** \return the return code of the builtin.
*/
int unalias(int argc, char *argv[]);

#endif /* ! BUILTINS_H */
