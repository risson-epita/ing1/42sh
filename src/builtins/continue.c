/**
** \file src/builtins/continue.c
**
** \brief Builtin commands.
**
** \author 42Schmittos
*/

#include <err.h>
#include <stdlib.h>

#include "shell.h"

/* Cf. documentation in `builtins.h`. */
int builtin_continue(int argc, char *argv[])
{
    if (g_shell->loops == 0)
        return 0;
    int nb_continue = 0;
    if (argc > 2)
    {
        warnx("continue: too many arguments");
        g_shell->is_continue = -1;
        return 1;
    }
    if (argc == 1)
    {
        nb_continue = 1;
    }
    else
    {
        if (atoi(argv[1]) < 1)
        {
            warnx("continue: %s: loop count out of range", argv[1]);
            g_shell->is_continue = -1;
            return 1;
        }

        nb_continue = atoi(argv[1]);
    }

    g_shell->is_continue = nb_continue;

    return 0;
}
