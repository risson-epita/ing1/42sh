/**
** \file src/builtins/break.c
**
** \brief Builtin commands.
**
** \author 42Schmittos
*/

#include <err.h>
#include <stdlib.h>

#include "shell.h"

/* Cf. documentation in `builtins.h`. */
int builtin_break(int argc, char *argv[])
{
    if (g_shell->loops == 0)
        return 0;
    int nb_break = 0;
    if (argc > 2)
    {
        warnx("break: too many arguments");
        g_shell->is_break = -1;
        return 1;
    }
    if (argc == 1)
    {
        nb_break = 1;
    }
    else
    {
        if (atoi(argv[1]) < 1)
        {
            warnx("break: %s: loop count out of range", argv[1]);
            g_shell->is_break = -1;
            return 1;
        }

        nb_break = atoi(argv[1]);
    }
    g_shell->is_break = nb_break;

    return 0;
}
