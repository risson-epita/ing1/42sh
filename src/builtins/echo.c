/**
** \file src/builtins/echo.c
**
** \brief Handle the echo command.
**
** \author 42Schmittos
*/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "builtins.h"
#include "shell.h"

/** `args` the structure which help us to know if an option of echo is set */
struct args
{
    /** if it is set echo doesn't print a newline */
    int n;
    /** if it is set echo enable interpretation of \ */
    int e;
    /** if it is set echo disable interpretation of \ */
    int E;
    /** if no option is found this option is set */
    int quit;
    /** it's just an index */
    int index;
    /** useless to know how many options are set */
    int nb_options;
};

/**
** Parse the options of echo.
**
** \param arg the structure which set options
** \param c an int which stores the value of parsing arguments.
*/
void find_echo_options(struct args *arg, int c)
{
    switch (c)
    {
    case 'n':
        arg->n = 1;
        arg->nb_options++;
        break;
    case 'E':
        arg->E = 1;
        arg->e = 0;
        arg->nb_options++;
        break;
    case 'e':
        arg->e = 1;
        arg->E = 0;
        arg->nb_options++;
        break;
    case -1:
        break;
    default:
        arg->quit = 1;
        if (optind > 1)
            optind -= 1;
        break;
    }
}

/**
** Parse the options of echo.
**
** \param argc
** \param argv
**
** \return `arg` the args structure.
*/
struct args *echo_options(int argc, char *argv[])
{

    struct args* arg = calloc(1, sizeof(struct args));
    arg->e = shopt_is_set("xpg_echo") ? 1 : 0;
    optind = 0;
    opterr = 0;
    int c = 0;
    arg->nb_options = 1;

    while (c != -1 && !arg->quit)
    {
        c = getopt(argc, argv, "+neE");
        find_echo_options(arg, c);
    }

    arg->index = optind;
    return arg;

}

/**
** Parse the options of echo -e.
**
** \param str the string after echo command.
** \param j a counter.
*/
void print_backslash_continue(char *str, size_t j)
{
    switch(str[j])
    {
    case 'f':
        putchar('\f');
        break;
    case 'n':
        putchar('\n');
        break;
    case 'r':
        putchar('\r');
        break;
    case 't':
        putchar('\t');
        break;
    case 'v':
        putchar('\v');
        break;
    case '\\':
        putchar('\\');
        break;
    default:
        putchar(str[j - 1]);
        putchar(str[j]);
        break;
    }
}

/**
** Parse the options of echo -e.
**
** \param str the string after echo command.
** \param arg the structure args.
*/
int print_backslash(char *str, struct args *arg)
{
    for (size_t j = 0; j < strlen(str); j++)
    {
        if (str[j] != '\\')
        {
            putchar(str[j]);
        }
        else
        {
            j++;
            switch (str[j])
            {
            case 'a':
                putchar('\a');
                break;
            case 'b':
                putchar('\b');
                break;
            case 'c':
                free(arg);
                return 0;
            case 'E':
            case 'e':
                putchar(27);
                break;
            default:
                print_backslash_continue(str, j);
            }
        }
    }
    if (arg->n != 1)
        putchar('\n');
    free(str);
    return 1;
}

/* Cf. documentation in `builtins.h`. */
int echo(int argc, char *argv[])
{
    if (argc == 1)
    {
        putchar('\n');
        fflush(stdout);
        return 0;
    }

    struct args *arg = echo_options(argc, argv);
    if (arg->e)
    {
        int len = 0;
        for (int j = arg->index; j < argc; j++)
        {
            len = len + strlen(argv[j]) + 1;
        }
        char *str = calloc(sizeof(char), len);
        int k;
        for (k = arg->index; k < argc; k++)
        {
            str = strcat(str, argv[k]);
        }
        print_backslash(str, arg);
    }
    else
    {
        for (; arg->index < argc - 1; arg->index++)
            printf("%s ", argv[arg->index]);

        if (arg->n == 1 && strcmp(argv[1], "-n"))
            printf("%s\n", argv[argc - 1]);
        else if (arg->n == 0)
            printf("%s\n", argv[argc - 1]);
        else
            printf("%s", argv[argc - 1]);
    }
    fflush(stdout);
    return 0;
}
