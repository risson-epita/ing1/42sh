/**
** \file src/builtins/source.c
**
** \brief Builtin commands.
**
** \author 42Schmittos
*/

#include <dirent.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "list.h"
#include "shell.h"
#include "utils.h"

struct lexer;
struct lexer *prepare_from_file(char *filename);
int execute(struct lexer *lexer);

/* Cf. documentation in `builtins.h`. */
int source(int argc, char *argv[])
{
    if (argc < 2)
    {
        warnx("%s: filename argument required", argv[0]);
        return 2;
    }

    char *src = NULL;
    struct variable *path_var = variable_get("PATH");

    if (path_var && shopt_is_set("sourcepath"))
    {
        src = calloc(1,1);
        char *path = path_var->value;
        DIR *d;
        int found = 0;
        struct dirent *dir;
        char *curr;
        char *rest = path;
        int err = 0;
        while ((curr = strtok_r(rest, ":", &rest)))
        {
            d = opendir(curr);
            if (d)
                dir = readdir(d);
            else
                continue;
            while (dir && strcmp(dir->d_name, argv[1]) != 0)
            {
                dir = readdir(d);
            }

            if (dir && strcmp(dir->d_name, argv[1]) == 0)
            {
                src = append_str(src, path);
                src = append_char(src, '/');
                src = append_str(src, dir->d_name);
                err = 0;
                closedir(d);
                found = 1;
                break;
            }
            else if (!dir)
            {
                free(src);
                src = calloc(1,1);
                err = 1;
            }

            closedir(d);
            if (found)
                break;
        }

        if (err)
        {
            warnx("source: %s: file not found", argv[1]);
            return 1;
        }
    }

    if (!src)
        src = argv[1];

    struct lexer *lexer = prepare_from_file(src);
    if (lexer == NULL)
    {
        warnx("source: %s: file not found", src);
        return 1;
    }
    g_shell->mode = M_FILE;

    int rc  = execute(lexer);

    g_shell->mode = M_INTER;

    return rc;
}
