/**
** \file src/builtins/history.c
**
** \brief `history` builtin.
**
** \author 42Schmittos
*/

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <readline/history.h>

#include "shell.h"
#include "utils.h"

/** Arguments to the builtin `history`. */
struct history_arguments
{
    /** `-c`: clear all entries. */
    int clear;
    /** `-r`: load history file `~/.42sh_history`. */
    int read_from_file;
};

/**
** Read and add all commands from `~/.42sh_history`
*/
void history_read_from_file(void)
{
    char *user_home = get_user_home();
    char *history_local = "/.42sh_history";
    char *history_filename = calloc(1,
                                strlen(user_home) + strlen(history_local) + 1);
    history_filename = strcat(history_filename, user_home);
    history_filename = strcat(history_filename, history_local);

    read_history(history_filename);

    free(history_filename);
    free(user_home);
}

/**
** Print all entries in the history list.
*/
void history_print(void)
{
    for (int i = history_base; i < history_length - 1; ++i)
    {
        HIST_ENTRY *entry = history_get(i);
        if (!entry)
            continue;
        printf("%5d  %s\n", i, entry->line);
    }
}

/* Cf. documentation in `builtins.h`. */
int history(int argc, char **argv)
{
    struct history_arguments args = { 0, 0 };
    for (int i = 1; i < argc; ++i)
    {
        if (!strcmp("-c", argv[i]))
            args.clear = 1;
        else if (!strcmp("-r", argv[i]))
            args.read_from_file = 1;
        else
        {
            warnx("history: %s: invalid option", argv[i]);
            return ERROR_CODE;
        }
    }

    if (args.read_from_file)
        history_read_from_file();

    if (args.clear)
        clear_history();

    if (!args.read_from_file && !args.clear)
        history_print();

    return 0;
}
