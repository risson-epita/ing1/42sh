/**
** \file src/builtins/exit.c
**
** \brief Builtin commands.
**
** \author 42Schmittos
*/

#include <ctype.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "shell.h"
#include "utils.h"

/* Cf. documentation in `builtins.h`. */
int builtin_exit(int argc, char *argv[])
{
    char rc;

    if (argc < 2)
    {
        rc = 0;
    }
    else
    {
        size_t i = 0;
        if (argv[1][0] == '+' || argv[1][0] == '-')
            i++;
        for (; i < strlen(argv[1]); i++)
        {
            if (!isdigit(argv[1][i]))
            {
                warnx("exit: %s: numeric argument required", argv[1]);
                exit(2);
            }
        }
        rc = atoi(argv[1]);
    }
    if (g_shell->mode == M_INTER)
        fprintf(stderr, "exit\n");
    if (argc > 3)
    {
        warnx("exit: too many arguments");
        return 1;
    }

    shell_free();
    exit(rc);

    return rc;
}
