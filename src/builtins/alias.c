/**
** \file src/builtins/alias.c
**
** \brief Builtin commands.
**
** \author 42Schmittos
*/

#include <err.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "shell.h"

/**
** Helper function for alias.
**
** \param arg the new alias we want to create.
**
** \return the return code of the add operation.
*/
int alias_new(char *arg)
{
    char *equal = strchr(arg, '=');
    *equal = 0;
    char *name = strdup(arg);
    char *value = strdup(equal + 1);

    alias_add(name, value);
    free(name);
    free(value);

    return 0;
}

/**
** Helper function for alias.
**
** \param name the name of the alias we want to print.
**
** \return the return code of the print operation.
*/
int alias_print_one(char *name)
{
    struct alias *alias = alias_find(name);
    if (!alias)
    {
        warnx("alias: %s: not found", name);
        return 1;
    }
    printf("%s='%s'\n", alias->name, alias->value);
    return 0;
}

/**
** Helper function for alias.
**
** \return the return code of the print operation.
*/
int alias_print_all(void)
{
    list_sort(g_shell->aliases);
    for (size_t i = 0; i < list_size(g_shell->aliases); ++i)
    {
        struct alias *alias = list_get(g_shell->aliases, i);
        printf("%s='%s'\n", alias->name, alias->value);
    }
    return 0;
}

/* Cf. documentation in `builtins.h`. */
int alias(int argc, char *argv[])
{
    if (argc == 1)
        return alias_print_all();
    int count = 0;
    for (int i = 1; i < argc; ++i)
    {
        if (strchr(argv[i], '='))
            count += alias_new(argv[i]);
        else
            count += alias_print_one(argv[i]);
    }
    return count != 0 ? 1 : 0;
}

/**
** Parse unalias options.
**
** \param argc number of arguments.
** \param argv list of arguments.
**
** \return the index where non-options arguments start.
*/
int unalias_parse_options(int argc, char *argv[])
{
    optind = 0;
    opterr = 0;

    int c = 0;
    while (c != -1)
    {
        c = getopt(argc, argv, "a");
        switch(c)
        {
        case 'a':
            list_clear(g_shell->aliases);
            break;
        case -1:
            break;
        default:
            warnx("unalias -%c: invalid option", c);
            warnx("usage: unalias [-a] name [name ...]");
            return 2;
        }
    }
    return optind;
}

/* Cf. documentation in `builtins.h`. */
int unalias(int argc, char *argv[])
{
    if (argc == 1)
    {
        warnx("usage: unalias [-a] name [name ...]");
        return 2;
    }

    int rc = 0;
    int i = unalias_parse_options(argc, argv);

    for (; i < argc; ++i)
    {
        struct alias alias = { argv[i], NULL };
        struct alias *el = list_remove_eq(g_shell->aliases, &alias);
        if (!el)
        {
            warnx("unalias: %s: not found", argv[i]);
            rc = 1;
        }
        else
            aliases_free(el);
    }

    return rc;
}
