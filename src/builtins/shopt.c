/**
** \file src/builtins/shopt.c
**
** \brief shopt builtin.
**
** \author 42Schmittos
*/
#include <err.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>

#include "shell.h"
#include "utils.h"

/** Structure for shopt arguments */
struct shopt_arguments
{
    /** set flag `-s` */
    int set;
    /** unset flag `-u` */
    int unset;
    /** quiet flag `-q` */
    int quiet;
};

/**
** Option parser for shopt
**
** \param argc the number of arguments.
** \param argv the list of arguments.
** \param args the differents shopts arguments.
**
** \return an int used as an exit code
*/
int shopt_parse_options(int argc, char *argv[], struct shopt_arguments *args)
{
    optind = 0;
    opterr = 0;
    int c = 0;

    while (c != -1)
    {
        c = getopt(argc, argv, "suq");
        switch (c)
        {
        case 's':
            args->set = 1;
            break;
        case 'u':
            args->unset = 1;
            break;
        case 'q':
            args->quiet = 1;
            break;
        case -1:
            break;
        default:
            warnx("shopt: usage: shopt [-qsu] [optname ...]");
            return -1;
        }
    }

    return optind;
}

/**
** Printing function for shopt.
**
** \param args the differents arguments for shopt.
**
** \return an int used as an exit code.
*/
int shopt_print_all_exec(struct shopt_arguments args)
{
    if (args.set)
        return shopt_print_all(1, args.quiet, 0);
    else if (args.unset)
        return shopt_print_all(0, args.quiet, 0);
    else
        return shopt_print_all(-1, args.quiet, 0);
}

/* Cf. documentation in `builtins.h`. */
int shopt(int argc, char *argv[])
{
    int rc = 0;
    struct shopt_arguments args = { 0, 0, 0 };

    int i = shopt_parse_options(argc, argv, &args);
    if (i == -1)
        return ERROR_CODE;

    if (args.set && args.unset)
    {
        warnx("shopt: cannot set and unset shell options simultaneously");
        return -1;
    }

    if (i == argc)
        return shopt_print_all_exec(args);

    int count = 0;
    for (; i < argc; ++i)
    {
        if (args.set)
            rc = shopt_set(argv[i]);
        else if (args.unset)
            rc = shopt_unset(argv[i]);
        else
            rc = shopt_print(argv[i], args.quiet);

        if (rc == -1)
            warnx("shopt: %s: invalid shell option name", argv[i]);
        else
            count += rc;
    }

    return count == argc - optind ? 0 : ERROR_CODE;
}
