/**
** \file src/builtins/set.c
**
** \brief Builtin commands.
**
** \author 42Schmittos
*/

#include <stdio.h>
#include <string.h>

#include "shell.h"
#include "utils.h"

/** Structure for set arguments */
struct set_arguments
{
    /* set flag `-o` */
    int set;
    /* unset flag `+o` */
    int unset;
};

/* Cf. documentation in `builtins.h`. */
int set(int argc, char *argv[])
{
    if (argc == 2)
    {
        if (!strcmp(argv[1], "-o"))
            set_print_all(-1, 0, 0);
        if (!strcmp(argv[1], "+o"))
            set_print_all(-1, 0, 1);
    }
    else if (argc == 3)
    {
        if (!strcmp(argv[1], "-o"))
            set_set(argv[2]);
        if (!strcmp(argv[1], "+o"))
            set_unset(argv[2]);
    }
    return 0;
}
