/**
** \file src/builtins/export.c
**
** \brief Builtin commands.
**
** \author 42Schmittos
*/

#include <err.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "list.h"
#include "shell.h"
#include "utils.h"

/**
** Handle the case where export has no options.
**
** \param argc
** \param argv
**
** \return 0 if it succeed
*/
int export_no_option(int argc, char *argv[])
{
    for (int i = 1; i < argc; i++)
    {
        if (!strcmp(argv[i], "-p"))
            continue;

        struct variable *var = environ_to_struct(argv[i]);
        var->is_exported = 1;
        size_t existing = list_find(g_shell->variables, var->name);
        size_t minus_one = -1;
        if (!var->value && existing != minus_one)
            continue;
        if (existing != minus_one)
            variable_free(list_remove_at(g_shell->variables, existing));
        list_push_back(g_shell->variables, var);
    }

    return 0;
}

/**
** Handle the case where export has -p option set.
**
** \return 0 if it succeed
*/
int export_p(void)
{
    for (size_t i = 0; i < list_size(g_shell->variables); ++i)
    {
        struct variable *var = list_get(g_shell->variables, i);
        printf("export %s", var->name);
        if (var->value)
        {
            printf("=\"%s\"", var->value);
        }
        printf("\n");
    }

    return 0;
}

/**
** Handle the case where export has -n option set.
**
** \param argc
** \param argv
**
** \return 0 if it succeed
*/
int export_n(int argc, char *argv[])
{
    if (argc == 2)
        return export_p();
    for (int i = 1; i < argc; i++)
    {
        char *name = strtok(argv[i], "=");
        if (strcmp("-n", name) && strcmp("-p", name))
            variable_unset(name);
    }

    return 0;
}

/**
** Get options of the export builtin.
**
** \param argc export argc.
** \param argv export argv.
** \param p the option p.
** \param n the option n.
*/
void export_get_options(int argc, char *argv[], int *p, int *n)
{
    int c = 0;
    while (c != -1)
    {
        c = getopt(argc, argv, "pn");
        switch (c)
        {
        case 'p':
            *p = 1;
            break;
        case 'n':
            *n = 1;
            break;
        }
    }
}

/* Cf. documentation in `builtins.h`. */
int builtin_export(int argc, char *argv[])
{
    if (argc == 1)
        return export_p();

    int i = 1;

    while (argv[i][0] != '-' && argv[i + 1])
    {
        if (argv[i + 1][0] == '-')
        {
            warnx("export: '%s': not a valid identifier", argv[i + 1]);
            return 1;
        }
        i++;
    }
    optind = 0;
    opterr = 0;
    int p = 0;
    int n = 0;

    export_get_options(argc, argv, &p, &n);

    if (n == 1)
        return export_n(argc, argv);
    if (p == 1 && argc == 2)
        return export_p();

    return export_no_option(argc, argv);
}
