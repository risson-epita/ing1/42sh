/**
** \file src/builtins/builtins.c
**
** \brief Builtin commands.
**
** \author 42Schmittos
*/

#include <string.h>

#include "builtins.h"

/** Compute the number of builtins. */
#define NB_BUILTINS sizeof(builtins) / sizeof(builtins[0])

/** The list of available builtins. */
static struct builtin builtins[] =
{
    { "history", history },
    { "shopt", shopt },
    { "set", set },
    { "cd", cd },
    { "exit", builtin_exit },
    { "export", builtin_export },
    { "source", source },
    { ".", source },
    { "echo", echo },
    { "break", builtin_break },
    { "continue", builtin_continue },
    { "alias", alias },
    { "unalias", unalias },
};

/* Cf. documentation in `builtins.h`. */
struct builtin *get_builtin(char *name)
{
    for (size_t i = 0; i < NB_BUILTINS; ++i)
    {
        if (!strcmp(name, builtins[i].name))
            return builtins + i;
    }
    return NULL;
}
