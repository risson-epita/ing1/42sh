/**
** \file src/lexer/helpers.c
**
** \brief Helpers functions for the lexer.
**
** \author 42Schmittos
*/

#include <stdlib.h>
#include <string.h>

#include "lexer.h"
#include "list.h"
#include "utils.h"
#include "token.h"

/* Cf. documentation in `lexer.h`. */
char *lexer_fill(struct lexer *lexer)
{
    free(lexer->value);
    lexer->value = NULL;
    lexer->value = lexer->get_input(lexer->input);
    lexer->input->ps = PS2;
    return lexer->value;
}


/* Cf. documentation in `lexer.h`. */
int is_still_operator(char *s, char c)
{
    char *to_check;
    if (!s)
        to_check = calloc(1, 1);
    else
        to_check = strdup(s);
    to_check = append_char(to_check, c);

    enum token_type to_check_type = token_get_type(to_check);
    if (to_check_type != TOK_EOF && to_check_type != TOK_EOL
            && to_check_type != TOK_AND && to_check_type != TOK_OR
            && to_check_type != TOK_AMPERSAND && to_check_type != TOK_LPAREN
            && to_check_type != TOK_RPAREN && to_check_type != TOK_SEMICOLON
            && to_check_type != TOK_DSEMICOLON && to_check_type != TOK_PIPE
            && to_check_type != TOK_LESS && to_check_type != TOK_MORE
            && to_check_type != TOK_DLESS && to_check_type != TOK_DMORE
            && to_check_type != TOK_LESSAND && to_check_type != TOK_MOREAND
            && to_check_type != TOK_LESSMORE && to_check_type != TOK_MOREPIPE
            && to_check_type != TOK_DLESSDASH)
    {
        to_check_type = TOK_WORD;
    }

    free(to_check);
    return to_check_type != TOK_WORD && to_check_type != TOK_EOL;
}

/* Cf. documentation in `lexer.h`. */
struct lexer *lexer_create(struct input *input,
                           char *(*get_input)(struct input *input))
{
    struct lexer *lexer = calloc(1, sizeof(struct lexer));
    lexer->input = input;
    lexer->get_input = get_input;
    lexer->token_list = list_init(token_cmp, token_free);
    return lexer;
}

/* Cf. documentation in `lexer.h`. */
void lexer_free(struct lexer *lexer)
{
    if (lexer->input->fin)
        fclose(lexer->input->fin);
    free(lexer->input);
    free(lexer->value);
    list_destroy(lexer->token_list);
    free(lexer);
}

char *handle_quoted_backslash(struct lexer *lexer, char *c,
        struct token *token)
{
    if (*c == '\0')
    {
        return lexer_fill(lexer);
    }
    token->value = append_char(token->value, '\\');
    if (*c == '$' || *c == '"' || *c == '`' || *c == '\\')
    {
        token->value = append_char(token->value, *c);
        return c + 1;
    }
    else
    {
        return c;
    }
}
