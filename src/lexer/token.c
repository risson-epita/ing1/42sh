/**
** \file src/lexer/token.c
**
** \brief token utils and types.
**
** \author 42Schmittos
*/

#include <ctype.h>
#include <err.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "token.h"

/** Computes the number of tokens in the array `struct token types[];` */
#define NB_TOKENS sizeof(types) / sizeof(types[0])

/** List of all special tokens. */
static struct token types[] =
{
    { "", TOK_EOL },
    { "&&", TOK_AND },
    { "||", TOK_OR },
    { "if", TOK_IF },
    { "fi", TOK_FI },
    { "then", TOK_THEN },
    { "else", TOK_ELSE },
    { "elif", TOK_ELIF },
    { "while", TOK_WHILE },
    { "do", TOK_DO },
    { "done", TOK_DONE },
    { "in", TOK_IN },
    { "until", TOK_UNTIL },
    { "for", TOK_FOR },
    { "case", TOK_CASE },
    { "esac", TOK_ESAC },
    { "function", TOK_FUNCTION },
    { "&", TOK_AMPERSAND },
    { "(", TOK_LPAREN },
    { ")", TOK_RPAREN },
    { "{", TOK_LBRACK },
    { "}", TOK_RBRACK },
    { "!", TOK_EXCLAMATION },
    { ";", TOK_SEMICOLON },
    { ";;", TOK_DSEMICOLON },
    { "|", TOK_PIPE },
    { "<", TOK_LESS },
    { ">", TOK_MORE },
    { "<<", TOK_DLESS },
    { ">>", TOK_DMORE },
    { "<&", TOK_LESSAND },
    { ">&", TOK_MOREAND },
    { "<>", TOK_LESSMORE },
    { ">|", TOK_MOREPIPE },
    { "<<-", TOK_DLESSDASH },
    { "()", TOK_DPAREN }
};

/* Cf. documentation in `token.h`. */
int correct_variable_name_syntax(char *str)
{
    if (strcmp(str, "?") == 0)
        return 1;

    for (; *str; str += 1)
    {
        if (!isalnum(*str) && *str != '_')
            return 0;
    }

    return 1;
}

/**
** Check if the syntax of a word could make it an assignment word.
**
** \param str the word to check.
**
** \return whether a word is an assignment word.
*/
int correct_assignement_syntax(char *str)
{
    char *p = strchr(str, '=');
    *p = 0;

    if (str[0] >= '0' && str[0] <= '9')
        return 0;
    if (str[0] == '=')
        return 0;

    int rc = correct_variable_name_syntax(str);

    *p = '=';
    return rc;
}

/* Cf. documentation in `token.h`. */
struct token *token_create(enum token_type type, char *value)
{
    struct token *token = malloc(sizeof(struct token));
    token->type = type;
    token->value = value;
    return token;
}

/* Cf. documentation in `token.h`. */
enum token_type token_get_type(char *value)
{
    for (long unsigned int i = 0; i < NB_TOKENS; i++)
    {
        if (strcmp(value, types[i].value) == 0)
        {
            return types[i].type;
        }
    }
    for (size_t i = 0; i < strlen(value); ++i)
    {
        if (value[i] < '0' || '9' < value[i])
            return TOK_WORD;
    }
    return TOK_IONUMBER;
}

/* Cf. documentation in `token.h`. */
int is_redirection(struct token *token)
{
    enum token_type t = token->type;
    if (t == TOK_LESS || t == TOK_MORE || t == TOK_DLESS || t == TOK_DMORE
            || t == TOK_LESSAND || t == TOK_MOREAND || t == TOK_LESSMORE
            || t == TOK_MOREPIPE || t == TOK_DLESSDASH)
    {
        return 1;
    }
    return 0;
}

/* Cf. documentation in `token.h`. */
void token_set_type(struct token *token)
{
    if (token->type != TOK_QUOTING)
        token->type = token_get_type(token->value);
    else
        token->type = TOK_WORD;

    if (strchr(token->value, '=') && correct_assignement_syntax(token->value))
        token->type = TOK_ASSIGNMENT;
}

/* Cf. documentation in `token.h`. */
int token_cmp(void *lhs, void *rhs)
{
    struct token *left = lhs;
    struct token *right = rhs;
    if (left->type < right->type)
        return LIST_LOWER;
    if (left->type > right->type)
        return LIST_BIGGER;
    return LIST_EQUAL;
}

/* Cf. documentation in `token.h`. */
void token_free(void *p)
{
    if (!p)
        return;
    struct token *token = p;
    free(token->value);
    free(token);
}
