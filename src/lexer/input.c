/**
** \file src/lexer/input.c
**
** \brief Functions to be called by the lexer to get input.
**
** \author 42Schmittos
*/

#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <readline/readline.h>

#include "lexer.h"
#include "shell.h"
#include "utils.h"

/* Cf. documentation in `input.h`. */
char *get_input_from_stream(struct input *input)
{
    char *line = NULL;
    size_t len = 0;
    errno = 0;

    ssize_t nread = getline(&line, &len, input->fin);
    fflush(input->fin);

    if (nread == -1)
    {
        if (errno)
            err(ERROR_CODE, "Error while reading file");
        free(line);
        return NULL;
    }

    if (line && *line && line[strlen(line) - 1] == '\n')
        line[strlen(line) - 1] = '\0';

    return line;
}

/* Cf. documentation in `input.h`. */
int is_interactive(void)
{
    int tty = rl_instream ? fileno(rl_instream) : fileno(stdin);
    return isatty(tty);
}

/**
** Prepare a tty if it's interactive.
**
** \param meta_flag `meta_flag` parameter to `rl_prep_terminal`.
*/
static void prep_terminal(int meta_flag)
{
    if (is_interactive())
        rl_prep_terminal(meta_flag);
}

/* Cf. documentation in `input.h`. */
char *get_input_from_stdin(struct input *input)
{
    char *prompt = get_prompt(input);

    rl_prep_term_function = prep_terminal;

    if (!is_interactive())
        prompt = NULL;

    char *line = readline(prompt);

    free(prompt);

    if (line && *line != EOF && strlen(line) > 0 && is_interactive())
    {
        switch(input->ps)
        {
        case PS1:
            history_add_word("\n");
            history_add_word(line);
            break;
        case PS2:
            history_add_word("; ");
            history_add_word(line);
            break;
        }
    }
    else if ((!line || *line == EOF) && is_interactive())
        history_add_word("\n");

    return line;
}
