/**
** \file src/lexer/token.h
**
** \brief Header file for the token utils.
**
** \author 42Schmittos
*/

#ifndef TOKEN_H
#define TOKEN_H

/** All tokens that can exist. */
enum token_type
{
    /** Represents `NULL`. */
    TOK_NONE,
    /** Represents the end of a line. */
    TOK_EOL,
    /** Represents a word. */
    TOK_WORD,
    /** Represents `EOF`. */
    TOK_EOF,
    /** Represents `&&`. */
    TOK_AND,
    /** Represents `if`. */
    TOK_IF,
    /** Represents `then`. */
    TOK_THEN,
    /** Represents `else`. */
    TOK_ELSE,
    /** Represents 'elif'. */
    TOK_ELIF,
    /** Represents `fi`. */
    TOK_FI,
    /** Represents `while`. */
    TOK_WHILE,
    /** Represents `do`. */
    TOK_DO,
    /** Represents `done`. */
    TOK_DONE,
    /** Represents `in`. */
    TOK_IN,
    /** Represents `until`. */
    TOK_UNTIL,
    /** Represents `for`. */
    TOK_FOR,
    /** Represents `case`. */
    TOK_CASE,
    /** Represents `esac`. */
    TOK_ESAC,
    /** Represents `function`. */
    TOK_FUNCTION,
    /** Represents `||`. */
    TOK_OR,
    /** Represents '&'. */
    TOK_AMPERSAND,
    /** Represents `(`. */
    TOK_LPAREN,
    /** Represents `)`. */
    TOK_RPAREN,
    /** Represents `{`. */
    TOK_LBRACK,
    /** Represents `}`. */
    TOK_RBRACK,
    /** Represents `!`. */
    TOK_EXCLAMATION,
    /** Represents `;`. */
    TOK_SEMICOLON,
    /** Represents `;;`. */
    TOK_DSEMICOLON,
    /** Represents `|`. */
    TOK_PIPE,
    /** Represents `<`. */
    TOK_LESS,
    /** Represents `>`. */
    TOK_MORE,
    /** Represents `<<`. */
    TOK_DLESS,
    /** Represents `>>`. */
    TOK_DMORE,
    /** Represents '<&'. */
    TOK_LESSAND,
    /** Represents '>&'. */
    TOK_MOREAND,
    /** Represents '<>'. */
    TOK_LESSMORE,
    /** Represents '>|'. */
    TOK_MOREPIPE,
    /** Represents '<<-'. */
    TOK_DLESSDASH,
    /** Represents '()'. */
    TOK_DPAREN,
    /** Represents a file descriptor. */
    TOK_IONUMBER,
    /** Represents an assignment word. */
    TOK_ASSIGNMENT,
    /** Represent a quoted token. */
    TOK_QUOTING,
    /** Represent a comment token. */
    TOK_COMMENT,
};

/** Stores a token with its type and its value. */
struct token
{
    /** The value of the token. */
    char *value;
    /** The type of the token. */
    enum token_type type;
};

/**
** Creates a token.
**
** \param type the type of the token to create.
** \param value the value of the token to create.
**
** \return the newly created token.
*/
struct token *token_create(enum token_type type, char *value);
/**
** Get the type of a word.
**
** \param value the word to get the type of.
**
** \return the type of the word.
*/
enum token_type token_get_type(char *value);
/**
** Set the type of a token.
**
** \param token the token to set the type of.
*/
void token_set_type(struct token *token);
/**
** Check if a token represents a redirection.
**
** \param token the token to check.
**
** \return whether a token represents a redirection.
*/
int is_redirection(struct token *token);
/**
** Compare two tokens.
**
** \param lhs the first token to compare.
** \param rhs the second token to compare.
**
** \return `LIST_[...]` depending on the result.
*/
int token_cmp(void *lhs, void *rhs);
/**
** Free the value of the token and the token.
**
** \param p the token to destroy.
*/
void token_free(void *p);
/**
** Check if the given string is a correct name.
**
** \param str the value to check.
**
** \return whether the string is correct.
*/
int correct_variable_name_syntax(char *str);

#endif /* ! TOKEN_H */
