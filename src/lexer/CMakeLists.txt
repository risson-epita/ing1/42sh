add_library(lexer STATIC
        accessors.c
        helpers.c
        input.c
        lexer.c
        lexer.h
        ps.c
        token.c
        token.h
)

set_target_properties(lexer PROPERTIES
        C_STANDARD 99
        C_STANDARD_REQUIRED ON
        C_EXTENSIONS OFF
)

target_include_directories(lexer PUBLIC
        .
)

target_link_libraries(lexer PRIVATE
        ${READLINE_LIB}
        common_options
        list
        shell
        utils
)
