/**
** \file src/lexer/lexer.h
**
** \brief Header file for the lexer modifiers and utils.
**
** \author 42Schmittos
*/

#ifndef LEXER_H
#define LEXER_H

#include <ctype.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "shell.h"
#include "token.h"

/** List of prompts. */
enum ps
{
    /** Refers to `$PS1` */
    PS1,
    /** Refers to `$PS2` */
    PS2
};

/** Structure to store necessary information to get input for the lexer */
struct input
{
    /** Input file to read the data from. */
    FILE *fin;
    /** Which PS should be displayed */
    enum ps ps;
    /** The mode of input. */
    enum mode mode;
};

/**
** Check if a character is part of the IFS.
**
** \param c the character to check.
**
** \return whether the character is part of the IFS.
*/
static inline int is_ifs(char c)
{
    if (!c)
        return 0;
    struct variable *ifs = variable_get("IFS");
    if (ifs)
    {
        if (strchr(ifs->value, c))
            return 1;
        return 0;
    }
    return isblank(c) || c == '\n';
}

/**
** Get the text to display as promt.
**
** \param input an element of type `struct input`, used to get the PSx.
**
** \return the text to display as prompt.
*/
char *get_prompt(struct input *input);

/**
** Get input from a `FILE *` stream.
** \param input the structure holding the `FILE *` structure.
**
** \return a string fetched from `stdin`.
*/
char *get_input_from_stream(struct input *input);
/**
** Get input from `stdin`, whether it redirected to 42sh or in interactive mode.
**
** \param input placeholder, unused.
**
** \return a string fetched from `stdin`.
*/
char *get_input_from_stdin(struct input *input);
/**
** Check if a tty is interactive.
**
** \return whether a tty is interactive.
*/
int is_interactive(void);

/** Represent a lexer */
struct lexer
{
    /** Argument for the function `get_input`. */
    struct input *input;
    /** Pointer to the function to use to refill the field `value`. */
    char *(*get_input)(struct input *input);
    /** A list of lexed tokens. */
    struct list *token_list;
    /** The string that the lexer got from user input. */
    char *value;
};

/**
** Create a lexer from an input source.
**
** \param input a structure containing all the necessary information to get
**              an input.
** \param get_input the function to be used by the lexer to get input.
**
** \return the created lexer.
*/
struct lexer *lexer_create(struct input *input,
                           char *(*get_input)(struct input *input));
/**
** Re-fill the lexer's value field with user input.
**
** \param lexer the lexer
**
** \return the input from the user.
*/
char *lexer_fill(struct lexer *lexer);
/**
** Start lexing the user input.
**
** \param lexer the lexer.
** \param error the error indicator.
*/
void lexer_start(struct lexer *lexer, int *error);
/**
** Get the next token without consuming it.
**
** \param lexer the `lexer` structure we want to peek.
** \param error the error indicator.
**
** \return the token that is in head of the token list.
*/
struct token *lexer_peek(struct lexer *lexer, int *error);
/**
** Get the second token from the token list without consuming it.
**
** \param lexer the lexer.
**
** \return the second token from the token list.
*/
struct token *lexer_peek_follow(struct lexer *lexer);
/**
** Get the next token and consume it.
**
** \param lexer the `lexer` structure we want to pop.
**
** \return the token who was in head of the token list.
*/
struct token *lexer_pop(struct lexer *lexer);
/**
** Get the next token and consume it.
** The token is always marked as a `TOK_WORD`.
**
** \param lexer the `lexer` structure we want to pop.
**
** \return the token who was in head of the token list.
*/
struct token *lexer_pop_command(struct lexer *lexer);
/**
** Get the next token without consuming it.
** The token is always marked as a `TOK_WORD`.
**
** \param lexer the `lexer` structure we want to pop.
**
** \return the token who was in head of the token list.
*/
struct token *lexer_peek_command(struct lexer *lexer);
/**
** Empty the list of lexed tokens and clear the user input.
**
** \param lexer the lexer.
*/
void lexer_reset(struct lexer *lexer);
/**
** Free the lexer and the list of tokens.
**
** \param lexer the `lexer` structure we want to free.
*/
void lexer_free(struct lexer *lexer);

/**
** Check if a string is still an operator after appending a character to it.
**
** \param s the string to check.
** \param c the character to append.
**
** \return whether a string is still an operator after appending a character to
**          it.
*/
int is_still_operator(char *s, char c);

/**
** Check if the value of a token match an alias and insert the
** corresponding tokens in the token list at it's place.
**
** \param lexer the current lexer containing the token list.
** \param token the token to check
**
** \return whether the token matches an alias or not.
*/
int handle_alias(struct lexer *lexer, struct token *token);

char *handle_quoted_backslash(struct lexer *lexer, char *c,
    struct token *token);

#endif /* ! LEXER_H */
