/**
** \file src/lexer/ps.c
**
** \brief Utilities to get the prompt to display.
**
** \author 42Schmittos
*/

#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>

#include "lexer.h"
#include "utils.h"

/** The max size for the date buffer. */
#define BUFFER_MAX_SIZE 1024

/**
** Format the prompt the date.
**
** \param str the resulting prompt.
** \param ps the input PS.
** \param i the index in `ps`.
*/
void format_case_date(char **str, char *ps, size_t *i)
{
    time_t t = time(NULL);
    struct tm *date = localtime(&t);
    char buffer[BUFFER_MAX_SIZE] = { 0 };
    char *tmp = calloc(1, 1);

    switch(ps[*i])
    {
    case 'D':
        ++(*i);
        if (ps[*i] != '{')
        {
            *str = append_char(*str, '\\');
            *str = append_char(*str, 'D');
        }
        else
        {
            for (++(*i); ps[*i] != '}'; ++(*i))
                tmp = append_char(tmp, ps[*i]);
            ++(*i);

            strftime(buffer, BUFFER_MAX_SIZE - 1, tmp, date);
            *str = realloc(*str, strlen(*str) + strlen(buffer) + 1);
            *str = strcat(*str, buffer);
        }
        break;
    case 'd':
        ++(*i);
        strftime(buffer, BUFFER_MAX_SIZE - 1, "%a %b %d", date);
        *str = realloc(*str, strlen(*str) + strlen(buffer) + 1);
        *str = strcat(*str, buffer);
        break;
    }
    free(tmp);
}

/**
** Format the prompt with the shell name.
**
** \param str the resulting prompt.
** \param ps the input PS.
** \param i the index in `ps`.
*/
void format_case_shell(char **str, char *ps, size_t *i)
{
    switch (ps[*i])
    {
    case 's':
        ++(*i);
        char *name = basename(variable_get("0")->value);
        *str = realloc(*str, strlen(*str) + strlen(name) + 1);
        *str = strcat(*str, name);
        break;
    default:
        format_case_date(str, ps, i);
    }
}

/**
** Format the prompt with the username.
**
** \param str the resulting prompt.
** \param ps the input PS.
** \param i the index in `ps`.
*/
void format_case_user(char **str, char *ps, size_t *i)
{
    char *login;
    uid_t stk = getuid();
    switch (ps[*i])
    {
    case 'u':
        ++(*i);
        login = getlogin();
        *str = realloc(*str, strlen(*str) + strlen(login) + 1);
        *str = strcat(*str, login);
        break;
    case '$':
        ++(*i);
        *str = append_char(*str, stk == 0 ? '#' : '$');
        break;
    default:
        format_case_shell(str, ps, i);
    }
}

/**
** Format the prompt with a truncated path.
**
** \param str the resulting prompt.
** \param ps the input PS.
** \param i the index in `ps`.
*/
void format_case_short_path(char **str, char *ps, size_t *i)
{
    struct variable *var_pwd = variable_get("PWD");
    char *home = get_user_home();
    char *pwd = "";
    if (var_pwd)
        pwd = var_pwd->value;
    char *base = basename(pwd);

    switch (ps[*i])
    {
    case 'W':
        ++(*i);
        if (strcmp(pwd, "/") == 0)
            *str = append_char(*str, '/');
        else if (strcmp(pwd, home) == 0)
            *str = append_char(*str, '~');
        else
        {
            *str = realloc(*str, strlen(*str) + strlen(base) + 1);
            *str = strcat(*str, base);
        }
        break;
    default:
        format_case_user(str, ps, i);
    }
    free(home);
}

/**
** Format the prompt with a path.
**
** \param str the resulting prompt.
** \param ps the input PS.
** \param i the index in `ps`.
*/
void format_case_long_path(char **str, char *ps, size_t *i)
{
    struct variable *var_pwd = variable_get("PWD");
    char *pwd = "";
    if (var_pwd)
        pwd = var_pwd->value;

    switch (ps[*i])
    {
    case 'w':
        ++(*i);
        if (strcmp(pwd, "/") == 0)
            *str = append_char(*str, '/');
        else
        {
            *str = realloc(*str, strlen(*str) + strlen(pwd) + 1);
            *str = strcat(*str, pwd);
        }
        break;
    default:
        format_case_short_path(str, ps, i);
    }
}

/**
** Format the prompt with the hostname.
**
** \param str the resulting prompt.
** \param ps the input PS.
** \param i the index in `ps`.
*/
void format_case_hostname(char **str, char *ps, size_t *i)
{
    char buffer[BUFFER_MAX_SIZE] = { 0 };
    gethostname(buffer, BUFFER_MAX_SIZE - 1);
    char *point;
    switch (ps[*i])
    {
    case 'h':
        ++(*i);
        point = strchr(buffer, '.');
        if (point)
            *point = '\0';
        *str = realloc(*str, strlen(*str) + strlen(buffer) + 1);
        *str = strcat(*str, buffer);
       break;
    case 'H':
        ++(*i);
        *str = realloc(*str, strlen(*str) + strlen(buffer) + 1);
        *str = strcat(*str, buffer);
        break;
    default:
        format_case_long_path(str, ps, i);
    }
}

/**
** Format the prompt with special characters.
**
** \param str the resulting prompt.
** \param ps the input PS.
** \param i the index in `ps`.
*/
void format_case_char(char **str, char *ps, size_t *i)
{
    switch (ps[*i])
    {
    case '\\':
        ++(*i);
        *str = append_char(*str, '\\');
        break;
    case 'a':
        ++(*i);
        *str = append_char(*str, '\a');
        break;
    case 'e':
        ++(*i);
        *str = append_char(*str, 27);
        break;
    case 'n':
        ++(*i);
        *str = append_char(*str, '\n');
        break;
    case 'r':
        ++(*i);
        *str = append_char(*str, '\r');
        break;
    default:
        format_case_hostname(str, ps, i);
    }
}

/**
** Format the prompt with an octal representation of a character.
**
** \param str the resulting prompt.
** \param ps the input PS.
** \param i the index in `ps`.
*/
int format_case_octal(char **str, char *ps, size_t *i)
{
    if (ps[*i + 1] <= '9' && ps [*i + 1] >= '0')
    {
        ++(*i);
        char tmp[4] = { 0 };
        tmp[0] = ps[(*i)++];
        tmp[1] = ps[(*i)++];
        tmp[2] = ps[(*i)++];
        int oct = atoi(tmp);
        int res = 0;
        int base = 1;
        int temp = oct;
        while (temp)
        {
            int last_digit = temp % 10;
            temp = temp / 10;
            res += last_digit * base;
            base = base * 8;
        }
        oct = res;
        *str = append_char(*str, oct);
        return 1;
    }
    return 0;
}

/**
** Build a formatted prompt with a PS variables.
**
** \param ps the PS variable to build the prompt from.
**
** \return the resulting prompt.
*/
char *format_ps(char *ps)
{
    char *str = calloc(1, sizeof(char));

    size_t i = 0;
    while (ps[i])
    {
        if (ps[i] != '\\')
        {
            str = append_char(str, ps[i]);
            ++i;
        }
        else
        {
            if (format_case_octal(&str, ps, &i))
                continue;
            ++i;
            format_case_char(&str, ps, &i);
        }
    }
    return str;
}

/* Cf. documentation in `get_ps.h`. */
char *get_prompt(struct input *input)
{
    struct variable *var = NULL;
    switch(input->ps)
    {
    case PS1:
        var = variable_get("PS1");
        break;
    case PS2:
        var = variable_get("PS2");
        break;
    }
    if (!var)
        return NULL;
    char *ps = format_ps(var->value);
    return ps;
}
