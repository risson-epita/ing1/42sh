/**
** \file src/lexer/accessors.c
**
** \brief Functions to be called by the lexer to access to tokens.
**
** \author 42Schmittos
*/

#include <stdlib.h>
#include <string.h>

#include "lexer.h"
#include "list.h"
#include "token.h"

/* Cf. documentation in `lexer.h`. */
struct token *lexer_pop(struct lexer *lexer)
{
    int error = 0;
    if (list_size(lexer->token_list) == 0)
        lexer_start(lexer, &error);

    return list_remove_at(lexer->token_list, 0);
}

/* Cf. documentation in `lexer.h`. */
struct token *lexer_peek(struct lexer *lexer, int *error)
{
    if (list_size(lexer->token_list) == 0)
        lexer_start(lexer, error);

    return list_get(lexer->token_list, 0);
}

/* Cf. documentation in `lexer.h`. */
struct token *lexer_pop_command(struct lexer *lexer)
{
    int error = 0;
    if (list_size(lexer->token_list) == 0)
        lexer_start(lexer, &error);

    struct token *token = list_remove_at(lexer->token_list, 0);
    token->type = TOK_WORD;

    return token;
}

/* Cf. documentation in `lexer.h`. */
struct token *lexer_peek_command(struct lexer *lexer)
{
    int error = 0;
    if (list_size(lexer->token_list) == 0)
        lexer_start(lexer, &error);

    struct token *token = list_get(lexer->token_list, 0);

     if (token->type == TOK_IF || token->type == TOK_FI
            || token->type == TOK_THEN || token->type == TOK_ELSE
            || token->type == TOK_ELIF || token->type == TOK_WHILE
            || token->type == TOK_DO || token->type == TOK_DONE
            || token->type == TOK_IN || token->type == TOK_UNTIL
            || token->type == TOK_FOR || token->type == TOK_CASE
            || token->type == TOK_ESAC || token->type == TOK_FUNCTION
            || token->type == TOK_LBRACK || token->type == TOK_RBRACK
            || token->type == TOK_ASSIGNMENT)
     {
        token->type = TOK_WORD;
     }

    return token;
}

/* Cf. documentation in `lexer.h`. */
struct token *lexer_peek_follow(struct lexer *lexer)
{
    return list_get(lexer->token_list, 1);
}

/* Cf. documentation in `lexer.h`. */
void lexer_reset(struct lexer *lexer)
{
    list_clear(lexer->token_list);
}
