/**
** \file src/lexer/lexer.c
**
** \brief The biggest part of the lexer.
**
** \author 42Schmittos
*/

#include <ctype.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "lexer.h"
#include "list.h"
#include "shell.h"
#include "token.h"
#include "utils.h"

/** Number of nested parenthesis. */
static size_t nested = 0;

char *lexer_rec(struct lexer *lexer, char *c, struct token *token,
                                                            char quoting_char);

/**
** Lex quoting.
**
** \param lexer the lexer.
** \param c the character we are lexing.
** \param token the token we are filling.
** \param quoting_char whether we are in a quoting.
**
** \return the character where we stopped lexing.
*/
char *handle_expansion(struct lexer *lexer, char *c, struct token *token,
                                                            char quoting_char)
{
    while (c && (nested != 0 || *c != quoting_char))
    {
        if (*c == '\0')
        {
            token->value = append_char(token->value, '\n');
            c = lexer_fill(lexer);
        }
        else
        {
            c = lexer_rec(lexer, c, token, quoting_char);
            if ((*c == ')' || *c == '`') && nested == 0)
                break;
            token->value = append_char(token->value, *c);
            c += 1;
        }
    }
    if (c)
    {
        token->value = append_char(token->value, quoting_char);
        return c + 1;
    }
    return c;
}

/**
** Lex a dollar.
**
** \param lexer the lexer.
** \param c the character we are lexing.
** \param token the token we are filling.
** \param quoting_char whether we are inside quotes.
**
** \return the character where we stopped lexing.
*/
char *handle_dollar(struct lexer *lexer, char *c, struct token *token,
        char quoting_char)
{
    if (*c == '\0')
        return c;
    token->value = append_char(token->value, *c);

    if (*(c + 1) == '(')
    {
        nested += 1;
        c += 1;
        token->value = append_char(token->value, *c);
        return handle_expansion(lexer, c + 1, token, ')');
    }

    if (*(c + 1) == '{')
    {
        c += 1;
        token->value = append_char(token->value, *c);
        return handle_expansion(lexer, c + 1, token, '}');
    }

    if (*c == '`')
        return handle_expansion(lexer, c + 1, token, '`');

    if (quoting_char == '"' || quoting_char == '\'')
        return c + 1;

    return lexer_rec(lexer, c + 1, token, quoting_char);
}

char *handle_quoting(struct lexer *lexer, char *c, struct token *token,
                                                            char quoting_char)
{
    if (quoting_char == '\\')
    {
        if (*c == 0)
        {
            return lexer_fill(lexer);
        }
        else
        {
            token->value = append_char(token->value, '\\');
            token->value = append_char(token->value, *c);
            return c + 1;
        }
    }

    size_t nested_quotes = 1;
    while (c && nested_quotes % 2 != 0)
    {
        if (*c == 0)
        {
            token->value = append_char(token->value, '\n');
            c = lexer_fill(lexer);
            continue;
        }
        if (*c == '\\')
        {
            c = handle_quoted_backslash(lexer, c + 1, token);
            continue;
        }
        if (*c == quoting_char)
            nested_quotes += 1;

        if (*c == '$')
        {
            c = handle_dollar(lexer, c, token, quoting_char);
        }
        else if (*c == '`')
            c = handle_dollar(lexer, c, token, *c);
        else
        {
            token->value = append_char(token->value, *c++);
        }
    }

    return c;
}

/**
** Second function of recursive lexing.
**
** \param lexer the lexer.
** \param c the character we are lexing.
** \param token the token we are filling.
**
** \return the character where we stopped lexing.
*/
char *lexer_rec2(struct lexer *lexer, char *c, struct token *token)
{
    if (*c && is_still_operator(NULL, *c))
    {
        if (token->type != TOK_NONE)
            return c;

        token->value = append_char(token->value, *c);
        return lexer_rec(lexer, c + 1, token, 0);
    }

    if (*c == '\0')
        return c;

    if (is_ifs(*c))
    {
        while (is_ifs(*c))
            ++c;

        if (strlen(token->value) == 0)
            return lexer_rec(lexer, c, token, 0);

        return c;
    }

    if (token->type == TOK_WORD)
    {
        token->value = append_char(token->value, *c);
        return lexer_rec(lexer, c + 1, token, 0);
    }

    if (*c == '#')
    {
        token->type = TOK_COMMENT;
        while (*c)
            ++c;
        return c;
    }

    token->type = TOK_WORD;
    return lexer_rec(lexer, c, token, 0);
}

/**
** First function of recursive lexing.
**
** \param lexer the lexer.
** \param c the character we are lexing.
** \param token the token we are filling.
** \param quoting_char whether we are in a quoting.
**
** \return the character where we stopped lexing.
*/
char *lexer_rec(struct lexer *lexer, char *c, struct token *token,
        char quoting_char)
{
    if (!c)
    {
        if (strlen(token->value) == 0)
            token->type = TOK_EOF;
        return c;
    }

    nested += quoting_char == ')' && *c == '(' ? 1 : 0;
    nested -= quoting_char == ')' && *c == ')' ? 1 : 0;

    if (*c && token->type != TOK_QUOTING && is_still_operator(token->value, *c))
    {
        token->value = append_char(token->value, *c);
        return lexer_rec(lexer, c + 1, token, 0);
    }

    if (*c && token->type != TOK_QUOTING && is_still_operator(token->value, 0))
        return c;

    if (*c && token->type != TOK_QUOTING && (*c == '\\' || *c == '\''
                || *c == '"'))
    {
        token->type = TOK_QUOTING;
        if (*c != '\\')
            token->value = append_char(token->value, *c);
        c = handle_quoting(lexer, c + 1, token, *c);
        return lexer_rec(lexer, c, token, quoting_char);
    }

    if (*c && (quoting_char == ')' || quoting_char == '}'
                || quoting_char == '`'))
        return c;

    if (*c == '$' || *c == '`')
    {
        return handle_dollar(lexer, c, token, *c);
    }

    return lexer_rec2(lexer, c, token);
}

/**
** Lex an here-document.
**
** \param lexer the lexer.
** \param delim the token to fill.
*/
void lexer_heredoc(struct lexer *lexer, struct token *delim)
{
    char *word = calloc(2, 1);
    word[0] = '"';
    char *line = lexer_fill(lexer);

    while (line && strcmp(line, delim->value) != 0)
    {
        word = realloc(word, strlen(word) + strlen(line) + 2);
        word = strcat(word, line);
        size_t len = strlen(word);
        word[len] = '\n';
        word[len + 1] = 0;
        line = lexer_fill(lexer);
    }

    if (!line)
        warnx("warning: here-document delimited by end-of-file (wanted `%s'')",
                delim->value);

    size_t len = strlen(word);
    word = realloc(word, len + 2);
    word[len] = '"';
    word[len + 1] = '\0';

    free(delim->value);
    delim->value = word;
}

/**
** Lex redirections and IO numbers.
**
** \param lexer the lexer.
*/
void lexer_redirections(struct lexer *lexer)
{
    for (size_t i = 0; i < list_size(lexer->token_list); i++)
    {
        struct token *token = list_get(lexer->token_list, i);
        if (token->type == TOK_DLESS || token->type == TOK_DLESSDASH)
        {
            struct token *delim = list_get(lexer->token_list, i + 1);
            if (is_still_operator(delim->value, 0))
                return;
            lexer_heredoc(lexer, delim);
        }
        if (token->type == TOK_IONUMBER)
        {
            struct token *next = list_get(lexer->token_list, i + 1);
            if (!next || !is_redirection(next))
                token->type = TOK_WORD;
        }
    }
}

/**
** Find the last alias in the list of shell aliases.
**
** \param value the name of the alias to find.
**
** \return the alias if found, `NULL` otherwise.
*/
static struct alias *get_last_alias(char *value)
{
    struct alias *alias = NULL;
    struct alias *next = NULL;
    char *original_value = value;

    while ((next = alias_find(value)))
    {
        alias = next;
        if (strcmp(alias->value, original_value) == 0)
            break;
        value = alias->value;
    }

    return alias;
}

/* Cf. documentation in `lexer.h`. */
int handle_alias(struct lexer *lexer, struct token *token)
{
    if (!shopt_is_set("expand_aliases") || token->type != TOK_WORD)
        return 0;

    size_t len = list_size(lexer->token_list);

    struct token *prev_token = list_get(lexer->token_list, len - 1);
    if ((len > 0 && (prev_token->type == TOK_WORD || is_redirection(prev_token)
                || prev_token->type == TOK_FOR)) || token->type != TOK_WORD)
            return 0;

    struct alias *alias = get_last_alias(token->value);
    if (!alias)
        return 0;

    char *start_next_token = alias->value;
    do {
        struct token *token_new = calloc(1, sizeof(struct token));
        token_new->value = calloc(1, 1);

        start_next_token = lexer_rec(lexer, start_next_token, token_new, 0);
        if (start_next_token && token_new->type != TOK_COMMENT)
            token_set_type(token_new);

        if (token_new->type != TOK_NONE && token_new->type != TOK_COMMENT)
            list_push_back(lexer->token_list, token_new);
        else
            token_free(token_new);
        nested = 0;
    } while (start_next_token && *start_next_token);

    return 1;
}

static void lexer_set_error(struct lexer *lexer, struct token *token,
        int *error)
{
    *error = 2;
    if (lexer->input->mode == M_INTER)
        fprintf(stdout, "\n");
    warnx("syntax error: unexpected end of file");
    token->type = TOK_EOL;
}

/* Cf. documentation in `lexer.h`. */
void lexer_start(struct lexer *lexer, int *error)
{
    char *start_of_next_token = lexer_fill(lexer);

    do {
        struct token *token_new = calloc(1, sizeof(struct token));
        token_new->value = calloc(1, 1);

        start_of_next_token = lexer_rec(lexer, start_of_next_token, token_new,
                                                                            0);
        if (!start_of_next_token && strlen(token_new->value))
        {
            lexer_set_error(lexer, token_new, error);
        }
        else if (start_of_next_token && token_new->type != TOK_COMMENT)
        {
            token_set_type(token_new);
        }

        if (token_new->type != TOK_NONE && token_new->type != TOK_COMMENT
                && !handle_alias(lexer, token_new))
            list_push_back(lexer->token_list, token_new);
        else
            token_free(token_new);

        nested = 0;
    } while (start_of_next_token && *start_of_next_token);

    if (start_of_next_token || error)
    {
        struct token *token_eol = token_create(TOK_EOL, NULL);
        list_push_back(lexer->token_list, token_eol);
    }

    free(lexer->value);
    lexer->value = NULL;

    lexer_redirections(lexer);
}
