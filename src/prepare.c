/**
** \file src/prepare.c
*/

#include <err.h>
#include <errno.h>
#include <setjmp.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "exec.h"
#include "lexer.h"
#include "parser.h"
#include "prepare.h"
#include "shell.h"
#include "utils.h"

/* Cf. documentation in `prepare.h`. */
struct lexer *prepare_from_command_string(char *command)
{
    if (strlen(command) == 0)
        exit(0);
    struct input *input = calloc(1, sizeof(struct input));
    input->mode = M_CMDSTR;
    input->fin = fmemopen(command, strlen(command), "r");
    if (!input->fin)
        err(ERROR_CODE, NULL);
    return lexer_create(input, get_input_from_stream);
}

/* Cf. documentation in `prepare.h`. */
struct lexer *prepare_from_file(char *filename)
{
    struct input *input = calloc(1, sizeof(struct input));
    input->mode = M_FILE;
    input->fin = fopen(filename, "r");
    if (!input->fin)
        return NULL;
    return lexer_create(input, get_input_from_stream);
}

/* Cf. documentation in `prepare.h`. */
struct lexer *prepare_from_stdin(void)
{
    struct input *input = calloc(1, sizeof(struct input));
    input->mode = M_STDIN;
    if (is_interactive())
        input->mode = M_INTER;
    return lexer_create(input, get_input_from_stdin);
}

/* Cf. documentation in `prepare.h`. */
int load_resources(void)
{
    struct lexer *lexer;
    char *rc_global = "/etc/42shrc";
    int rc = 0;

    if (access(rc_global, R_OK) == 0)
    {
        lexer = prepare_from_file(rc_global);
        rc = execute(lexer);
    }

    if (rc)
        return rc;

    char *user_home = get_user_home();
    char *rc_local = "/.42shrc";
    char *rc_user = calloc(1, strlen(user_home) + strlen(rc_local) + 1);
    rc_user = strcat(rc_user, user_home);
    rc_user = strcat(rc_user, rc_local);

    if (access(rc_user, R_OK) == 0)
    {
        lexer = prepare_from_file(rc_user);
        rc = execute(lexer);
    }
    free(rc_user);
    free(user_home);

    return rc;
}

/* Cf. documentation in `prepare.h`. */
int execute(struct lexer *lexer)
{
    if (!lexer)
        return ERROR_CODE;

    while(sigsetjmp(g_shell->sigint_buf, 1) != 0)
        ;

    int rc = 0;
    struct ast_node *ast = NULL;

    do {
        if (ast)
            ast->free(ast);
        lexer->input->ps = PS1;
        lexer_reset(lexer);
        int error = 0;

        ast = parse(lexer, &error);

        if (ast)
        {
            if (shopt_is_set("ast_print"))
                ast_print(ast);

            rc = error ? ERROR_CODE : exec(ast);
            g_shell->last_rc = rc;
        }
    } while(ast);

    if (lexer->input->mode == M_INTER)
        printf("\n");

    if (rc == ERROR_CODE && lexer->input->mode == M_CMDSTR)
        rc = 1;

    lexer_free(lexer);

    return rc;
}
