/**
** \file src/list/ops.c
**
** \brief Helper functions to execute operations on a list structure.
**
** \author 42Schmittos
*/

#include <stddef.h>
#include <stdlib.h>

#include "list.h"

/* Cf. documentation in `list.h`. */
void list_map(struct list *list, void *(*func)(void *))
{
    if (!list)
        return;
    struct list_item *node = list->head;
    while (node)
    {
        node->data = func(node->data);
        node = node->next;
    }
}

/* Cf. documentation in `list.h`. */
void list_reverse(struct list *list)
{
    if (!list)
        return;
    struct list_item *node = list->head;
    struct list_item *tmp;
    while (node)
    {
        tmp = node->next;
        node->next = node->prev;
        node->prev = tmp;
        node = node->prev;
    }
    tmp = list->head;
    list->head = list->tail;
    list->tail = tmp;
}

/**
** Swap the data of two items in the list.
**
** \param node1 the first item.
** \param node2 the second item.
*/
void list_item_swap_data(struct list_item *node1, struct list_item *node2)
{
    void *tmp = node1->data;
    node1->data = node2->data;
    node2->data = tmp;
}

/**
** Partition a list.
**
** \param list the list to partition.
** \param b the beginning index.
** \param e the ending index.
** \param p the index of the pivot.
**
** \return the new index of the pivot.
*/
size_t partition(struct list *list, size_t b, size_t e, size_t p)
{
    void *pivot = list_get(list, p);
    size_t i = b - 1;
    size_t j = e;

    while (1)
    {
        do {
            ++i;
        } while (list->cmp(list_get(list, i), pivot) == LIST_LOWER);

        do {
            --j;
        } while (list->cmp(list_get(list, j), pivot) == LIST_BIGGER);

        if (j <= i)
            return i + (i == b ? 1 : 0);

        list_item_swap_data(list_get_node(list, i), list_get_node(list, j));
    }
}

/**
** Find a pivot using the median technique.
**
** \param list the list.
** \param l the beginning index.
** \param r the ending index.
**
** \return the index of the pivot.
*/
size_t pivot_median(struct list *list, size_t l, size_t r)
{
    size_t left_i = l;
    size_t middle_i= l + (r - l) / 2;
    size_t right_i = r - 1;
    void *left = list_get(list, left_i);
    void *middle = list_get(list, middle_i);
    void *right = list_get(list, right_i);

    int left_right = list->cmp(left, right);
    int left_middle = list->cmp(left, middle);
    int middle_right = list->cmp(middle, right);

    if (left_middle != 1)
    {
        if (middle_right != 1)
            return middle_i;
        if (left_right != 1)
            return right_i;
        return left_i;
    }
    else
    {
        if (middle_right == 1)
            return middle_i;
        if (left_right != 1)
            return left_i;
        return right_i;
    }
}

/**
** Sort a list using the quick sort algorithm.
**
** \param list the list to sort.
** \param b the starting index.
** \param e the ending index.
*/
void list_quick_sort_rec(struct list *list, size_t b, size_t e)
{
    if ((e - b) > 1)
    {
        size_t pivot_i = pivot_median(list, b, e);
        size_t p = partition(list, b, e, pivot_i);
        list_quick_sort_rec(list, b, p);
        list_quick_sort_rec(list, p, e);
    }
}

/* Cf. documentation in `list.h`. */
void list_sort(struct list *list)
{
    if (!list)
        return;
    size_t size = list_size(list);
    if (size == 0 || size == 1)
        return;
    list_quick_sort_rec(list, 0, size);
}

/* Cf. documentation in `list.h`. */
void **list_to_array(struct list *list)
{
    if (!list)
        return NULL;
    void **array = calloc(list_size(list) + 1, sizeof(void *));

    struct list_item *node = list->head;
    size_t i = 0;
    while (node)
    {
        array[i] = node->data;
        node = node->next;
        ++i;
    }

    return array;
}
