/**
** \file src/list/finders.c
**
** \brief helper functions to find elements in a list structure.
**
** \author 42Schmittos
*/

#include <stdlib.h>

#include "list.h"

/* Cf. documentation in `list.h`. */
struct list_item *list_get_node(struct list *list, size_t index)
{
    if (!list || index >= list->size)
        return NULL;
    struct list_item *node = list->head;
    for (size_t i = 0; i < index; ++i)
        node = node->next;
    return node;
}

/* Cf. documentation in `list.h`. */
void *list_get(struct list *list, size_t index)
{
    struct list_item *node = list_get_node(list, index);
    if (!node)
        return NULL;
    return node->data;
}

/* Cf. documentation in `list.h`. */
size_t list_find(const struct list *list, void *element)
{
    if (!list)
        return LIST_FAIL;
    struct list_item *node = list->head;
    for (size_t i = 0; i < list->size; ++i)
    {
        if (list->cmp(node->data, element) == LIST_EQUAL)
            return i;
        node = node->next;
    }
    return LIST_FAIL;
}
