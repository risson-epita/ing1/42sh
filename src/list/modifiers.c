/**
** \file src/list/modifiers.c
**
** \brief Modifier functions to manage a list structure.
**
** \author 42Schmittos
*/

#include <stdlib.h>

#include "list.h"

/* Cf. documentation in `list.h`. */
int list_push_front(struct list *list, void *element)
{
    struct list_item *new = malloc(sizeof(struct list_item));
    if (!new)
        return LIST_FAIL;
    new->data = element;
    if (!list->head)
    {
        list->head = new;
        list->tail = new;
        new->prev = NULL;
        new->next = NULL;
    }
    else
    {
        new->next = list->head;
        list->head->prev = new;
        list->head = new;
        new->prev = NULL;
    }
    ++(list->size);
    return LIST_SUCCESS;
}

/* Cf. documentation in `list.h`. */
int list_push_back(struct list *list, void *element)
{
    struct list_item *new = malloc(sizeof(struct list_item));
    if (!new)
        return LIST_FAIL;
    new->data = element;
    if (!list->tail)
    {
        list->head = new;
        list->tail = new;
        new->prev = NULL;
        new->next = NULL;
    }
    else
    {
        new->prev = list->tail;
        list->tail->next = new;
        list->tail = new;
        new->next = NULL;
    }
    ++(list->size);
    return LIST_SUCCESS;
}

/* Cf. documentation in `list.h`. */
int list_insert_at(struct list *list, void *element, size_t index)
{
    if (!list || index > list->size)
        return LIST_FAIL;
    if (index == 0)
        return list_push_front(list, element);
    if (index == list->size)
        return list_push_back(list, element);

    struct list_item *new = malloc(sizeof(struct list_item));
    if (!new)
        return LIST_FAIL;
    struct list_item *old = list_get_node(list, index);
    if (!old)
    {
        free(new);
        return LIST_FAIL;
    }

    new->data = element;

    new->prev = old->prev;
    new->prev->next = new;
    old->prev = new;
    new->next = old;

    ++(list->size);
    return LIST_SUCCESS;
}

/* Cf. documentation in `list.h`. */
void *list_remove_at(struct list *list, size_t index)
{
    if (!list || index >= list->size || list->size == 0)
        return NULL;
    void *data;
    struct list_item *to_delete = list_get_node(list, index);
    if (!to_delete)
        return NULL;
    if (list->size == 1)
    {
        list->head = NULL;
        list->tail = NULL;
    }
    else if (index == 0)
    {
        list->head = to_delete->next;
        list->head->prev = NULL;
    }
    else if (index == list->size - 1)
    {
        list->tail = to_delete->prev;
        list->tail->next = NULL;
    }
    else
    {
        to_delete->prev->next = to_delete->next;
        to_delete->next->prev = to_delete->prev;
    }
    data = to_delete->data;
    free(to_delete);
    --(list->size);
    return data;
}

/* Cf. documentation in `list.h`. */
void *list_remove_eq(struct list *list, void *element)
{
    if (!list)
        return 0;
    struct list_item *node = list->head;
    size_t count = 0;
    while (node)
    {
        if (list->cmp(node->data, element) == LIST_EQUAL)
            return list_remove_at(list, count);
        ++count;
        node = node->next;
    }
    return NULL;
}
