/**
** \file src/list/helpers.c
**
** \brief Helper functions to manage a list structure.
**
** \author 42Schmittos
*/

#include <stdlib.h>

#include "list.h"

/* Cf. documentation in `list.h`. */
struct list *list_init(int (*cmp)(void *, void *), void (*clear)(void *))
{
    struct list *list = malloc(sizeof(struct list));
    if (!list)
        return NULL;
    list->size = 0;
    list->head = NULL;
    list->tail = NULL;
    list->cmp = cmp;
    list->clear = clear;
    return list;
}

/**
** Recalculate the size of a list.
**
** To be used if you don't want to keep track of the size of a list in list
** functions.
** If everything goes well, it shouldn't be used at all.
**
** \param list the list of which the size has to be recalculated.
*/
void list_recalculate_size(struct list *list)
{
    if (!list)
        return;
    if (!list->head)
        list->size = 0;
    size_t size = 0;
    struct list_item *node = list->head;
    while (node)
    {
        node = node->next;
        ++size;
    }
    list->size = size;
}

/* Cf. documentation in `list.h`. */
size_t list_size(const struct list *list)
{
    if (!list)
        return -1;
    return list->size;
}

/* Cf. documentation in `list.h`. */
void list_clear(struct list *list)
{
    if (!list)
        return;
    struct list_item *node = list->head;
    while (node)
    {
        struct list_item *tmp = node;
        node = node->next;
        list->clear(tmp->data);
        free(tmp);
    }
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
}

/* Cf. documentation in `list.h`. */
void list_destroy(struct list *list)
{
    list_clear(list);
    free(list);
}
