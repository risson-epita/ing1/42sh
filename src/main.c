/**
** \file src/main.c
**
** \brief Start of the program 42sh. Handling of arguments.
**
** \author 42Schmittos
*/

#include <err.h>
#include <string.h>
#include <unistd.h>

#include "lexer.h"
#include "prepare.h"
#include "shell.h"
#include "utils.h"

/** Structure to store the results of the arguments passed to 42sh. */
struct arguments
{
    /** Flag for `--norc` argument to 42sh. */
    int norc;
    /** Flag for `--ast-print` argument to 42sh. */
    int ast_print;
    /** Flag for `-c` argument to 42sh. */
    int command_string;
};

/**
** Parse GNU long options.
**
** Here's the list of the options that are recognized:
** - `--norc`
** - `--ast-print`
**
** \param args structure where we will store the arguments.
** \param i `argv` index that we update as we go through the list.
** \param argc `main` argument `argc`.
** \param argv `main` argument `argv`.
*/
void parse_long_options(struct arguments *args, int *i, int argc, char *argv[])
{
    while (*i < argc)
    {
        if (!strcmp("--norc", argv[*i]))
            args->norc = 1;
        else if (!strcmp("--ast-print", argv[*i]))
            args->ast_print = 1;
        else
            break;
        ++(*i);
    }
}

/**
** Parse short options.
**
** - `+O <option>`
** - `-O <option>`
** - `+o <option>`
** - `-o <option>`
** - `-c <command>`
**
** \param args structure where we will store the arguments.
** \param i `argv` index that we update as we go through the list.
** \param argc `main` argument `argc`.
** \param argv `main` argument `argv`.
*/
void parse_short_options(struct arguments *args, int *i, int argc, char *argv[])
{
    for (; *i < argc; ++(*i))
    {
        if (!strcmp("+O", argv[*i]))
        {
            if (++(*i) == argc)
                shopt_print_all(-1, 0, 1);
            else if (shopt_unset(argv[*i]) == 1)
                errx(ERROR_CODE, "%s: invalid shell option name", argv[*i]);
        }
        else if (!strcmp("-O", argv[*i]))
        {
            if (++(*i) == argc)
                shopt_print_all(-1, 0, 0);
            else if (shopt_set(argv[*i]) == 1)
                errx(ERROR_CODE, "%s: invalid shell option name", argv[*i]);
        }
        else if (!strcmp("+o", argv[*i]))
        {
            if (++(*i) == argc)
                set_print_all(-1, 0, 1);
            else if (set_unset(argv[*i]) == 1)
                errx(ERROR_CODE, "%s: invalid shell option name", argv[*i]);
        }
        else if (!strcmp("-o", argv[*i]))
        {
            if (++(*i) == argc)
                set_print_all(-1, 0, 0);
            else if (set_set(argv[*i]) == 1)
                errx(ERROR_CODE, "%s: invalid shell option name", argv[*i]);
        }
        else if (!strcmp("-c", argv[*i]))
            args->command_string = 1;
        else
            break;
    }
}

/**
** Entry point for 42sh.
**
** \param argc number of argument passed to the program.
** \param argv list of arguments passed to the program.
** \param envp list of environment variables passed to the program.
**
** \return the exit code of the program.
*/
int main(int argc, char *argv[], char *envp[])
{
    int rc = 0;

    struct arguments args = { 0, 0, 0 };
    int i = 1;

    parse_long_options(&args, &i, argc, argv);
    parse_short_options(&args, &i, argc, argv);

    struct lexer *lexer;

    if (args.command_string) // Using -c
    {
        if (i == argc)
            errx(ERROR_CODE, "-c requires an argument.");
        lexer = prepare_from_command_string(argv[i++]);
    }
    else if (i < argc) // Reading commands from file
        lexer = prepare_from_file(argv[i++]);
    else // Reading commands from stdin (interactive or not)
        lexer = prepare_from_stdin();

    if (!lexer)
        err(ERROR_CODE, "Problem while parsing file");

    shell_init(lexer->input->mode, i, argv, envp);

    if (!args.norc && load_resources())
        return ERROR_CODE;

    if (args.ast_print)
        shopt_set("ast_print");

    rc = execute(lexer);

    shell_free();

    return rc;
}
