#ifndef PREPARE_H
#define PREPARE_H

/**
** Prepare a lexer that gets its input from the command argument passed with
** `-c`.
**
** \param command the command given with `-c`.
**
** \return a lexer ready to receive input.
*/
struct lexer *prepare_from_command_string(char *command);
/**
** Prepare a lexer that gets its input from a file passed in argument.
**
** \param filename to name of the file.
**
** \return a lexer ready to receive input.
*/
struct lexer *prepare_from_file(char *filename);
/**
** Prepare a lexer that gets its input from the standard input, interactive or
** not.
**
** \return a lexer ready to receive input.
*/
struct lexer *prepare_from_stdin(void);
/**
** Load resource files from `/etc/42shrc` and then `~/.42shrc`.
**
** \return the return code of the last command executed in those files.
*/
int load_resources(void);
/**
** Execute the shell from the tokens given by the lexer.
**
** \param lexer the lexer from which we get input.
**
** \return the return code of the last command executed.
*/
int execute(struct lexer *lexer);

#endif /* ! PREPARE_H */
