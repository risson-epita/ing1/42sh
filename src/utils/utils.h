/**
** \file src/utils/utils.h
**
** \brief Utilities used throughout the project
**
** \author 42Schmittos
*/

#ifndef UTILS_H
#define UTILS_H

#include <ctype.h>

/** A macro representing the default error code */
#define ERROR_CODE 2

/**
** Utility to implicitly cast `*p` to a `(void *)`.
**
** \param p a pointer.
**
** \return `p` as a `(void *)`.
*/
static inline void *cast_void(void *p)
{
    return p;
}

/**
** Utility to implicitly cast a `void *` to a `char *`.
**
** \param p the pointer to cast.
**
** \return the pointer cast to a `char *`.
*/
static inline char *cast_char(void *p)
{
    char *ptr = cast_void(p);
    return ptr;
}

/**
** Utility to implicitly cast a `void **` to a `char **`.
**
** \param p the pointer to cast.
**
** \return the pointer cast to a `char **`.
*/
static inline char **cast_double_char(void **p)
{
    char **ptr = cast_void(p);
    return ptr;
}

/**
** Convert a number to its string representation.
** Only works with positive numbers.
**
** \param n the number to convert.
**
** \return the string representation of the number.
*/
char *itoa(long n);

/**
** Appends a string to another string.
**
** \param src the string to append to.
** \param dst the string to be append.
**
** \return the resulting string.
*/
char *append_str(char* src, char *dst);
/**
** Append a character to a string by realloc'ing it.
**
** \param s the string to append to.
** \param c the character to append.
**
** \return the string with the appended character.
*/
char *append_char(char *s, char c);

#endif /* ! UTILS_H */
