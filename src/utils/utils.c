/**
** \file src/utils/utils.c
**
** \brief Utilities used throughout the project
**
** \author 42Schmittos
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

/** Get the absolute value of a number */
#define ABS(x) (x < 0 ? -x : x)

/**
** Count the number of digits in a number.
**
** \param n the number to count the digits of.
**
** \return the number of digits in the number.
*/
size_t number_digits(long n)
{
    size_t result = 1;
    if (n < 0)
        result += 1;

    n = ABS(n);

    while (n > 10)
    {
        ++result;
        n /= 10;
    }

    return result;
}

/**
** Swap two characters.
**
** \param x pointer to the first character.
** \param y pointer to the second character.
*/
static inline void swap(char *x, char *y)
{
    char t = *x;
    *x = *y;
    *y = t;
}

/**
** Reverse part of a string.
**
** \param buffer the string to reverse.
** \param i where to start.
** \param j where to end.
**
** \return a pointer to the string.
*/
static char* reverse(char *buffer, int i, int j)
{
    while (i < j)
        swap(&buffer[i++], &buffer[j--]);

    return buffer;
}

/* Cf. documentation in `utils.h`. */
char* itoa(long value)
{
    int base = 10;
    char *buffer = calloc(number_digits(value) + 2, sizeof(char));
    long n = ABS(value);

    long i = 0;
    while (n)
    {
        long r = n % base;

        if (r >= 10)
            buffer[i++] = 65 + (r - 10);
        else
            buffer[i++] = 48 + r;
        n = n / base;
    }
    if (i == 0)
        buffer[i++] = '0';

    if (value < 0 && base == 10)
        buffer[i++] = '-';

    buffer[i] = '\0';
    return reverse(buffer, 0, i - 1);
}

/* Cf. documentation in `utils.h`. */
char *append_str(char* src, char *dst)
{
    char *new_str = calloc(strlen(src) + strlen(dst) + 1, sizeof(char));
    sprintf(new_str, "%s%s", src, dst);

    free(src);

    return new_str;
}
/* Cf. documentation in `utils.h` */
char *append_char(char *s, char c)
{
    s = realloc(s, strlen(s) + 2);
    size_t len = strlen(s);
    s[len] = c;
    s[len + 1] = 0;
    return s;
}
